<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitrixEditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitrix_editions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('bitrix_editions')->insert([
            ['name' => 'Старт'],
            ['name' => 'Стандарт'],
            ['name' => 'Малый бизнес'],
            ['name' => 'Эксперт'],
            ['name' => 'Бизнес'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitrix_editions');
    }
}
