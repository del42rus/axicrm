<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('number')->unique();
            $table->string('scanned_copy');
            $table->string('site')->unique()->nullable();
            $table->date('start_date');
            $table->date('finish_date');
            $table->float('budget');
            $table->integer('status')->default(1);
            $table->integer('stage_id')->unsigned()->nullable();
            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('salesman_id')->unsigned()->nullable();

            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('salesman_id')
                ->references('id')->on('users');

            $table->foreign('stage_id')
                ->references('id')->on('app_contract_stages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_contracts');
    }
}
