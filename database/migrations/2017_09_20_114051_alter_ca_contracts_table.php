<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCaContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->renameColumn('status', 'active')->default(1)->change();

            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('salesman_id')->unsigned()->nullable();
            $table->integer('responsible_person_id')->unsigned()->nullable();

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('salesman_id')
                ->references('id')->on('users');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->renameColumn('active', 'status')->default(0)->change();

            $table->dropForeign('ca_contracts_marketer_id_foreign');
            $table->dropForeign('ca_contracts_salesman_id_foreign');
            $table->dropForeign('ca_contracts_responsible_person_id_foreign');

            $table->dropColumn('marketer_id');
            $table->dropColumn('salesman_id');
            $table->dropColumn('responsible_person_id');
        });
    }
}
