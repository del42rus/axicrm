<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarketerToContractRegistryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_registry', function (Blueprint $table) {
            $table->integer('marketer_id')->nullable()->after('folder_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_registry', function (Blueprint $table) {
            $table->dropColumn('marketer_id');
        });
    }
}
