<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedPaymentMethodContractTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('amount');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('budget');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('budget');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('budget');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('budget');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('amount');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->integer('payment_method_id')->unsigned()->nullable()->after('finish_date');

            $table->foreign('payment_method_id')
                ->references('id')->on('payment_methods')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->dropForeign('adm_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->dropForeign('app_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->dropForeign('ca_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->dropForeign('ci_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->dropForeign('seo_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->dropForeign('smm_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->dropForeign('hosting_contracts_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });
    }
}
