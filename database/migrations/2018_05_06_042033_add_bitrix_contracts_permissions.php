<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBitrixContractsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'create-bitrix-contracts',
                'display_name' => 'Создавать договоры',
                'description' => 'Создавать договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'read-bitrix-contracts',
                'display_name' => 'Просматривать договоры',
                'description' => 'Просматривать договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'update-bitrix-contracts',
                'display_name' => 'Редактировать договоры',
                'description' => 'Редактировать договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'delete-bitrix-contracts',
                'display_name' => 'Удалять договоры',
                'description' => 'Удалять договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'activate-bitrix-contracts',
                'display_name' => 'Возобновлять договоры',
                'description' => 'Возобновлять договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'decline-bitrix-contracts',
                'display_name' => 'Отклонять договоры',
                'description' => 'Отклонять договоры',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'soft-delete-bitrix-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => 'CMS:1С-Битрикс'
            ],
            [
                'name' => 'restore-bitrix-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => 'CMS:1С-Битрикс'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
