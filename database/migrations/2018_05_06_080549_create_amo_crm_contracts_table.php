<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmoCrmContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amo_crm_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amo_crm_edition_id')->unsigned();
            $table->string('scanned_copy');
            $table->string('site')->nullable();
            $table->date('start_date');
            $table->date('finish_date');
            $table->float('cost');
            $table->float('renewal_cost');
            $table->integer('user_count');
            $table->string('admin_login');
            $table->string('admin_password');
            $table->string('cancellation_letter')->nullable();
            $table->string('cancellation_agreement')->nullable();
            $table->date('cancellation_date')->nullable()->nullable();
            $table->integer('active')->default(1);
            $table->integer('salesman_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('amo_crm_edition_id')
                ->references('id')->on('amo_crm_editions');

            $table->foreign('salesman_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amo_crm_contracts');
    }
}
