<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('number')->unique();
            $table->string('scanned_copy');
            $table->string('site')->unique()->nullable();
            $table->date('start_date');
            $table->float('amount');
            $table->string('metrika_link')->nullable();
            $table->integer('active')->default(1);
            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('salesman_id')->unsigned()->nullable();
            $table->integer('responsible_person_id')->unsigned()->nullable();

            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('salesman_id')
                ->references('id')->on('users');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_contracts');
    }
}
