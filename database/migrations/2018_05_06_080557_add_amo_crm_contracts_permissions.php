<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmoCrmContractsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'create-amo-crm-contracts',
                'display_name' => 'Создавать договоры',
                'description' => 'Создавать договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'read-amo-crm-contracts',
                'display_name' => 'Просматривать договоры',
                'description' => 'Просматривать договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'update-amo-crm-contracts',
                'display_name' => 'Редактировать договоры',
                'description' => 'Редактировать договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'delete-amo-crm-contracts',
                'display_name' => 'Удалять договоры',
                'description' => 'Удалять договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'activate-amo-crm-contracts',
                'display_name' => 'Возобновлять договоры',
                'description' => 'Возобновлять договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'decline-amo-crm-contracts',
                'display_name' => 'Отклонять договоры',
                'description' => 'Отклонять договоры',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'soft-delete-amo-crm-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => 'CRM:Amo.CRM'
            ],
            [
                'name' => 'restore-amo-crm-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => 'CRM:Amo.CRM'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
