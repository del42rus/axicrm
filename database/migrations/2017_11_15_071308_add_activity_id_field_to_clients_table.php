<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivityIdFieldToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('kind_of_activity');

            $table->integer('activity_id')->unsigned()->nullable()->after('id');

            $table->foreign('activity_id')
                ->references('id')->on('activities')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_activity_id_foreign');
            $table->dropColumn('activity_id');

            $table->string('kind_of_activity')->nullable();
        });
    }
}
