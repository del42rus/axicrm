<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteAndRestorePermissionsClientsAndContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'soft-delete-contacts',
                'display_name' => 'Удалять контакты в архив',
                'description' => 'Удалять контакты в архив',
                'group' => 'Контакты'
            ],
            [
                'name' => 'restore-contacts',
                'display_name' => 'Восстанавливать контакты из архива',
                'description' => 'Восстанавливать контакты из архива',
                'group' => 'Контакты'
            ],
            [
                'name' => 'soft-delete-clients',
                'display_name' => 'Удалять клиентов в архив',
                'description' => 'Удалять клиентов в архив',
                'group' => 'Клиенты'
            ],
            [
                'name' => 'restore-clients',
                'display_name' => 'Восстанавливать клиентов из архива',
                'description' => 'Восстанавливать клиентов из архива',
                'group' => 'Клиенты'
            ],
            [
                'name' => 'soft-delete-suppliers',
                'display_name' => 'Удалять поставщиков в архив',
                'description' => 'Удалять поставщиков в архив',
                'group' => 'Поставщики'
            ],
            [
                'name' => 'restore-suppliers',
                'display_name' => 'Восстанавливать поставщиков из архива',
                'description' => 'Восстанавливать поставщиков из архива',
                'group' => 'Поставщики'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
