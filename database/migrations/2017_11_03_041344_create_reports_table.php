<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contract_id')->unsigned();
            $table->string('contract_type');
            $table->string('report_file')->nullable();
            $table->string('act_file')->nullable();
            $table->decimal('act_total')->nullable();
            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('responsible_person_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
