<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNameColumnPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('permissions')
            ->where('name', 'create-registry-contracts')
            ->update(['name' => 'create-contracts']);

        DB::table('permissions')
            ->where('name', 'update-registry-contracts')
            ->update(['name' => 'update-contracts']);

        DB::table('permissions')
            ->where('name', 'read-registry-contracts')
            ->update(['name' => 'read-contracts']);

        DB::table('permissions')
            ->where('name', 'delete-registry-contracts')
            ->update(['name' => 'delete-contracts']);

        DB::table('permissions')
            ->where('name', 'edit-folder-registry-contracts')
            ->update(['name' => 'edit-folder-contracts']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
