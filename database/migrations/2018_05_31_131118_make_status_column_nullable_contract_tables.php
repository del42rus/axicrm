<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeStatusColumnNullableContractTables extends Migration
{
    private $tables = [
        'adm_contracts', 'app_contracts', 'ci_contracts', 'ca_contracts', 'smm_contracts', 'seo_contracts',
        'hosting_contracts', 'additional_contracts', 'additional_digital_contracts', 'bitrix_contracts', 'bitrix_crm_contracts', 'amo_crm_contracts'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->integer('status')->nullable()->default(null)->change();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->integer('status')->nullable(false)->default(1)->change();
            });
        }
    }
}
