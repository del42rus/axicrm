<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesContractsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->softDeletes()->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
