<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractIdAllContractTables extends Migration
{
    private $tables = [
        'app' => 'app_contracts',
        'ca' => 'ca_contracts',
        'ci' => 'ci_contracts',
        'smm' => 'smm_contracts',
        'seo' => 'seo_contracts',
        'additional' => 'additional_contracts',
        'additional-digital' => 'additional_digital_contracts',
        'hosting' => 'hosting_contracts',
        'bitrix' => 'bitrix_contracts',
        'bitrix-crm' => 'bitrix_crm_contracts',
        'amo-crm' => 'amo_crm_contracts'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $key => $table) {
            $contracts = DB::table('contracts')->where('contract_type', $key)->get();

            foreach ($contracts as $contract) {
                DB::table($table)
                    ->where('id', $contract->contract_id)
                    ->update(['contract_id' => $contract->id]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('contract_id');
            });

            Schema::table($table, function (Blueprint $table) {
                $table->integer('contract_id');
            });
        }
    }
}
