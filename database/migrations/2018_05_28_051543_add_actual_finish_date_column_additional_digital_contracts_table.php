<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualFinishDateColumnAdditionalDigitalContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_digital_contracts', function (Blueprint $table) {
            $table->date('actual_finish_date')->after('finish_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_digital_contracts', function (Blueprint $table) {
            $table->dropColumn('actual_finish_date');
        });
    }
}
