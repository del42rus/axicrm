<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnershipTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ownership_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        DB::table('ownership_types')->insert([
                ['name' => 'ИП'],
                ['name' => 'ООО'],
                ['name' => 'ОАО'],
                ['name' => 'ЗАО'],
                ['name' => 'Физ.лицо']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ownership_types');
    }
}
