<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancellationColumnsToAdditionalDigitalContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_digital_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
