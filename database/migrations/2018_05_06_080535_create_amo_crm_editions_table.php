<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmoCrmEditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amo_crm_editions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('amo_crm_editions')->insert([
            ['name' => 'Микро-Бизнес'],
            ['name' => 'Старт-Ап'],
            ['name' => 'Базовый'],
            ['name' => 'Расширенный'],
            ['name' => 'Профессиональный'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amo_crm_editions');
    }
}
