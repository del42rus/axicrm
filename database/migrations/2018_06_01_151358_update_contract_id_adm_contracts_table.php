<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateContractIdAdmContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $contracts = DB::table('contracts')->where('contract_type', 'adm')->get();

        foreach ($contracts as $contract) {
            DB::table('adm_contracts')
                ->where('id', $contract->contract_id)
                ->update(['contract_id' => $contract->id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->dropColumn('contract_id');
            $table->integer('contract_id');
        });
    }
}
