<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosting_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('hosting_services')->insert([
            ['name' => 'Хостинг'],
            ['name' => 'Домен'],
            ['name' => 'Хостинг и домен'],
            ['name' => 'DNS'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hosting_services');
    }
}
