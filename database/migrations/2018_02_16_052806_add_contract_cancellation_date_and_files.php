<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractCancellationDateAndFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->string('cancellation_letter')->nullable()->after('scanned_copy');
            $table->string('cancellation_agreement')->nullable()->after('cancellation_letter');
            $table->date('cancellation_date')->nullable()->nullable()->after('cancellation_agreement');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });

        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->dropColumn('cancellation_letter');
            $table->dropColumn('cancellation_agreement');
            $table->dropColumn('cancellation_date');
        });
    }
}
