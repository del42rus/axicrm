<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppContractStages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_contract_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('position')->default(0);
        });

        DB::table('app_contract_stages')->insert([
            ['name' => 'Создание ТЗ', 'position' => 0],
            ['name' => 'Создание дизайн-макета', 'position' => 1],
            ['name' => 'Верстка', 'position' => 2],
            ['name' => 'Программирование', 'position' => 3],
            ['name' => 'Тестирование', 'position' => 4],
            ['name' => 'Наполнение', 'position' => 5]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_contract_stages');
    }
}
