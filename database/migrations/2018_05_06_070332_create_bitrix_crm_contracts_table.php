<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitrixCrmContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitrix_crm_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bitrix_crm_edition_id')->unsigned();
            $table->string('scanned_copy');
            $table->string('site')->nullable();
            $table->date('start_date');
            $table->date('finish_date');
            $table->float('cost');
            $table->float('renewal_cost');
            $table->string('key');
            $table->string('admin_login');
            $table->string('admin_password');
            $table->string('cancellation_letter')->nullable();
            $table->string('cancellation_agreement')->nullable();
            $table->date('cancellation_date')->nullable()->nullable();
            $table->integer('active')->default(1);
            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('salesman_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bitrix_crm_edition_id')
                ->references('id')->on('bitrix_crm_editions');

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('salesman_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitrix_crm_contracts');
    }
}
