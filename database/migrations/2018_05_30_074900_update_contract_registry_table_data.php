<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\AdmContract;

class UpdateContractRegistryTableData extends Migration
{
    private $map = [
        'smm' => 'App\SMMContract',
        'seo' => 'App\SeoContract',
        'ca' => 'App\CAContract',
        'adm' => 'App\AdmContract',
        'app' => 'App\AppContract',
        'ci' => 'App\CIContract',
        'hosting' => 'App\HostingContract',
        'additional' => 'App\AdditionalContract',
        'additional-digital' => 'App\AdditionalDigitalContract',
        'bitrix' => 'App\BitrixContract',
        'bitrix-crm' => 'App\BitrixCRMContract',
        'amo-crm' => 'App\AmoCRMContract'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->map as $key => $class) {
            foreach ($class::withTrashed()->get() as $contract) {
                DB::table('contract_registry')->where('contract_id', $contract->id)
                    ->where('contract_type', $key)
                    ->update([
                        'status' => $contract->status,
                        'marketer_id' => $contract->marketer_id,
                        'salesman_id' => $contract->salesman_id,
                        'responsible_person_id' => $contract->responsible_person_id
                    ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('contract_registry')
            ->update([
                'status' => null,
                'marketer_id' => null,
                'salesman_id' => null,
                'responsible_person_id' => null,
            ]);
    }
}
