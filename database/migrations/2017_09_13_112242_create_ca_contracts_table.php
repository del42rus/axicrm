<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('number')->unique();
            $table->string('scanned_copy');
            $table->string('site')->unique()->nullable();
            $table->date('start_date');
            $table->integer('is_budget_opened')->default(0);
            $table->float('setup_cost');
            $table->float('support_cost');
            $table->float('budget');
            $table->string('metrika_link')->nullable();
            $table->integer('status')->default(0);

            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ca_contracts');
    }
}
