<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActualFinishDateToAppContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_contracts', function (Blueprint $table) {
            $table->date('actual_finish_date')->nullable()->after('finish_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_contracts', function (Blueprint $table) {
            $table->dropColumn('actual_finish_date');
        });
    }
}
