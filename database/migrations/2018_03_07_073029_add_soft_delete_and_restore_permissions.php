<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteAndRestorePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [];

        $contracts = [
            'adm' => 'Договоры Администрирование',
            'app' => 'Договоры Сайты и приложения',
            'ca' => 'Договоры Контекстная реклама',
            'ci' => 'Договоры Фирменный стиль',
            'hosting' => 'Договоры Хостинг',
            'smm' => 'Договоры SMM',
            'seo' => 'Договоры SEO',
        ];

        foreach ($contracts as $key => $name) {
            $data[] = [
                'name' => 'soft-delete-' . $key . '-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => $name
            ];

            $data[] = [
                'name' => 'restore-' . $key . '-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => $name
            ];
        }

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
