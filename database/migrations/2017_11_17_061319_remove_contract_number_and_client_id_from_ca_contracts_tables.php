<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveContractNumberAndClientIdFromCaContractsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->dropForeign('ca_contracts_client_id_foreign');
            $table->dropColumn('client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->nullable()->after('id');
            $table->string('number')->unique()->after('client_id');

            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');
        });
    }
}
