<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHostingServiceIdColumnToHostingContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->dropColumn('service_code');
            $table->integer('hosting_service_id')->unsigned()->after('id');

            $table->foreign('hosting_service_id')
                ->references('id')->on('hosting_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hosting_contracts', function (Blueprint $table) {
            $table->tinyInteger('service_code');
            $table->dropForeign('hosting_contracts_hosting_service_id_foreign');
            $table->dropColumn('hosting_service_id');
        });
    }
}
