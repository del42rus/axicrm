<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractIdColumnToAllContractTables extends Migration
{
    private $tables = [
        'ca_contracts',
        'ci_contracts',
        'app_contracts',
        'smm_contracts',
        'seo_contracts',
        'additional_contracts',
        'additional_digital_contracts',
        'hosting_contracts',
        'bitrix_contracts',
        'bitrix_crm_contracts',
        'amo_crm_contracts'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->integer('contract_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('contract_id');
            });
        }
    }
}
