<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosting_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domain')->unique();
            $table->tinyInteger('service_code');
            $table->string('scanned_copy');
            $table->date('finish_date');
            $table->float('amount');
            $table->integer('responsible_person_id')->unsigned()->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();

            $table->foreign('responsible_person_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hosting_contracts');
    }
}
