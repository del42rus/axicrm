<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ownership_type_id')->unsigned()->default(1);
            $table->string('legal_name')->unique();
            $table->string('brand')->nullable();
            $table->string('kind_of_activity')->nullable();
            $table->string('site')->nullable();
            $table->timestamps();

            $table->foreign('ownership_type_id')
                ->references('id')->on('ownership_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
