<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalDigitalContractsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'create-additional-digital-contracts',
                'display_name' => 'Создавать договоры доп. работ',
                'description' => 'Создавать договоры доп. работ',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'read-additional-digital-contracts',
                'display_name' => 'Просматривать договоры доп. работ',
                'description' => 'Просматривать договоры доп. работ',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'update-additional-digital-contracts',
                'display_name' => 'Редактировать договоры доп. работ',
                'description' => 'Редактировать договоры доп. работ',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'delete-additional-digital-contracts',
                'display_name' => 'Удалять договоры доп. работ',
                'description' => 'Удалять договоры доп. работ',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'activate-additional-digital-contracts',
                'display_name' => 'Возобновлять договоры',
                'description' => 'Возобновлять договоры',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'decline-additional-digital-contracts',
                'display_name' => 'Отклонять договоры',
                'description' => 'Отклонять договоры',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'soft-delete-additional-digital-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => 'Доп. работы ОАиП'
            ],
            [
                'name' => 'restore-additional-digital-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => 'Доп. работы ОАиП'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
