<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBitrixCrmContractsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'create-bitrix-crm-contracts',
                'display_name' => 'Создавать договоры',
                'description' => 'Создавать договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'read-bitrix-crm-contracts',
                'display_name' => 'Просматривать договоры',
                'description' => 'Просматривать договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'update-bitrix-crm-contracts',
                'display_name' => 'Редактировать договоры',
                'description' => 'Редактировать договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'delete-bitrix-crm-contracts',
                'display_name' => 'Удалять договоры',
                'description' => 'Удалять договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'activate-bitrix-crm-contracts',
                'display_name' => 'Возобновлять договоры',
                'description' => 'Возобновлять договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'decline-bitrix-crm-contracts',
                'display_name' => 'Отклонять договоры',
                'description' => 'Отклонять договоры',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'soft-delete-bitrix-crm-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => 'CRM:Битрикс-24'
            ],
            [
                'name' => 'restore-bitrix-crm-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => 'CRM:Битрикс-24'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
