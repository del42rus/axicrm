<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserForeignKeyContractsConstraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ca_contracts', function (Blueprint $table) {
            $table->dropForeign('ca_contracts_marketer_id_foreign');
            $table->dropForeign('ca_contracts_salesman_id_foreign');
            $table->dropForeign('ca_contracts_responsible_person_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });

        Schema::table('seo_contracts', function (Blueprint $table) {
            $table->dropForeign('seo_contracts_marketer_id_foreign');
            $table->dropForeign('seo_contracts_salesman_id_foreign');
            $table->dropForeign('seo_contracts_responsible_person_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });

        Schema::table('ci_contracts', function (Blueprint $table) {
            $table->dropForeign('ci_contracts_marketer_id_foreign');
            $table->dropForeign('ci_contracts_salesman_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });

        Schema::table('app_contracts', function (Blueprint $table) {
            $table->dropForeign('app_contracts_marketer_id_foreign');
            $table->dropForeign('app_contracts_salesman_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });

        Schema::table('adm_contracts', function (Blueprint $table) {
            $table->dropForeign('adm_contracts_marketer_id_foreign');
            $table->dropForeign('adm_contracts_salesman_id_foreign');
            $table->dropForeign('adm_contracts_responsible_person_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });

        Schema::table('smm_contracts', function (Blueprint $table) {
            $table->dropForeign('smm_contracts_marketer_id_foreign');
            $table->dropForeign('smm_contracts_salesman_id_foreign');
            $table->dropForeign('smm_contracts_responsible_person_id_foreign');

            $table->foreign('marketer_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('salesman_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('responsible_person_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
