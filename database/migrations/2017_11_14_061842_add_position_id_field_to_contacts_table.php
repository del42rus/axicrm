<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionIdFieldToContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('position');

            $table->integer('position_id')->unsigned()->nullable()->after('id');

            $table->foreign('position_id')
                ->references('id')->on('positions')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('position')->nullable();

            $table->dropForeign('contacts_position_id_foreign');
            $table->dropColumn('position_id');
        });
    }
}
