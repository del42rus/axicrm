<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalContractsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'create-additional-contracts',
                'display_name' => 'Создавать договоры доп. работ',
                'description' => 'Создавать договоры доп. работ',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'read-additional-contracts',
                'display_name' => 'Просматривать договоры доп. работ',
                'description' => 'Просматривать договоры доп. работ',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'update-additional-contracts',
                'display_name' => 'Редактировать договоры доп. работ',
                'description' => 'Редактировать договоры доп. работ',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'delete-additional-contracts',
                'display_name' => 'Удалять договоры доп. работ',
                'description' => 'Удалять договоры доп. работ',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'activate-additional-contracts',
                'display_name' => 'Возобновлять договоры',
                'description' => 'Возобновлять договоры',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'decline-additional-contracts',
                'display_name' => 'Отклонять договоры',
                'description' => 'Отклонять договоры',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'soft-delete-additional-contracts',
                'display_name' => 'Удалять договоры в архив',
                'description' => 'Удалять договоры в архив',
                'group' => 'Доп. работы'
            ],
            [
                'name' => 'restore-additional-contracts',
                'display_name' => 'Восстанавливать договоры из архива',
                'description' => 'Восстанавливать договоры из архива',
                'group' => 'Доп. работы'
            ],
        ];

        DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
