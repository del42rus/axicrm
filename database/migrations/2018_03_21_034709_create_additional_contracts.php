<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scanned_copy');
            $table->string('site')->nullable();
            $table->string('social_media')->nullable();
            $table->date('start_date');
            $table->date('finish_date');
            $table->float('budget');
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->integer('active')->default(1);
            $table->integer('marketer_id')->unsigned()->nullable();
            $table->integer('salesman_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('marketer_id')
                ->references('id')->on('users');

            $table->foreign('salesman_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_contracts');
    }
}
