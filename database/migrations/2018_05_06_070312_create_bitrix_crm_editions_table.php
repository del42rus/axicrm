<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitrixCrmEditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitrix_crm_editions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
        });

        DB::table('bitrix_crm_editions')->insert([
            ['name' => 'Проект', 'type' => 'cloud'],
            ['name' => 'Проект+', 'type' => 'cloud'],
            ['name' => 'Команда', 'type' => 'cloud'],
            ['name' => 'Компания', 'type' => 'cloud'],
            ['name' => 'CRM', 'type' => 'box'],
            ['name' => 'Корпоративный портал', 'type' => 'box'],
            ['name' => 'Энтерпрайз', 'type' => 'box'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitrix_crm_editions');
    }
}
