<?php

return [
    'create' => 'Create',
    'read' => 'Read',
    'update' => 'Update',
    'delete' => 'Delete',
    'basic' => 'Basic',
    'crud' => 'CRUD',
];