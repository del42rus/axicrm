<?php

return [
    'create' => 'Создание',
    'read' => 'Чтение',
    'update' => 'Правка',
    'delete' => 'Удаление',
    'basic' => 'Базовое',
    'crud' => 'CRUD',
];