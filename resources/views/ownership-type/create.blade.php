@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('ownership-types.index') }}">{{ __('Ownership Types') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Ownership Type') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('ownership-type.partial.form')
        </div>
    </div>
@endsection
