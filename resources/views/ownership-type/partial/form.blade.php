<form method="post" action="@if (isset($type)){{ route('ownership-types.update', $type->id) }}@else{{ route('ownership-types.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($type))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $type->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Ownership Type Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($type) ? $type->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>