<form method="post" action="@if (isset($region)){{ route('regions.update', $region->id) }}@else{{ route('regions.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($region))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $region->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Region') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($region) ? $region->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>