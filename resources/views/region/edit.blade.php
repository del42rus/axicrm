@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('regions.index') }}">{{ __('Regions') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Edit Region') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('region.partial.form', ['region' => $region])
        </div>
    </div>
@endsection
