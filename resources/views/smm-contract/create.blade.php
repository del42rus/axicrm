@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.smm.index') }}">{{ __('SMM') }}</a></li>
        <li class="breadcrumb-item">{{ $contract->client->getFullName() }}</li>
    </ol>
    <h3 class="page-header">{{ __('Create Contract') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('smm-contract.partial.form')
        </div>
    </div>
@endsection
