@php

use App\User;

@endphp

<form method="post" action="@if ($contract instanceof \App\SMMContract){{ route('contracts.smm.update', $contract->id) }}@else{{ route('contracts.smm.store') }}@endif" enctype="multipart/form-data" novalidate>
    {{ csrf_field() }}

    @if ($contract instanceof \App\SMMContract)
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @else
        <input type="hidden" name="contract_id" value="{{ $contract->id }}">
    @endif

    @php($isCorrectiveContract = false)

    @if ($contract instanceof \App\SMMContract)
        @if ($isCorrectiveContract = $contract->contract->isCorrectiveContract())
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract->parent->contract_number }}" readonly disabled>
            </div>
            <div class="form-group">
                <label>Номер доп. соглашения</label>
                <input type="text" name="addition_number" class="form-control" value="{{ $contract->contract->contract_number }}" readonly disabled>
            </div>
        @else
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract->contract_number }}" readonly disabled>
            </div>
        @endif
    @else
        @if ($isCorrectiveContract = $contract->isCorrectiveContract())
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->parent->contract_number }}" readonly disabled>
            </div>
            <div class="form-group">
                <label>Номер доп. соглашения</label>
                <input type="text" name="addition_number" class="form-control" value="{{ $contract->contract_number }}" readonly disabled>
            </div>
        @else
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract_number }}" readonly disabled>
            </div>
        @endif
    @endif

    <div class="form-group">
        <label class="d-block">@if ($isCorrectiveContract) Скан доп. соглашения @else Скан договора @endif</label>
        <label class="custom-file">
            <input type="file" name="scanned_copy" class="custom-file-input @if ($errors->has('scanned_copy')) is-invalid @endif" required>
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('scanned_copy'))
            <div class="invalid-feedback d-block">{{ $errors->first('scanned_copy') }}</div>
        @endif
        @if ($contract instanceof \App\SMMContract && $contract->scanned_copy)
            <a class="btn btn-link" href="{{ route('contracts.smm.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
        @endif
    </div>
    @if ($contract instanceof \App\SMMContract)
        @foreach ($contract->files as $file)
            <div class="form-group">
                <label>Название файла</label>
                <input name="files[id_{{$file->id}}][name]" type="text" class="form-control @if ($errors->has('files.id_' . $file->id .'.name')) is-invalid @endif" value="{{ old('files.id_' . $file->id . '.name') ? : $file->name }}"/>
                @if ($errors->has('files.id_' . $file->id . '.name'))
                    <div class="invalid-feedback">{{ $errors->first('files.id_' . $file->id . '.name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label class="custom-file">
                    <input type="file" name="files[id_{{$file->id}}][file]" class="custom-file-input @if ($errors->has('files.id_' . $file->id . '.file')) is-invalid @endif">
                    <span class="custom-file-control"></span>
                </label>
                @if ($errors->has('files.id_' . $file->id . '.file'))
                    <div class="invalid-feedback d-block">{{ $errors->first('files.id_' . $file->id . '.file') }}</div>
                @endif
                <a class="btn btn-link" href="{{ route('contracts.smm.download-file', ['contract' => $contract->id, 'file' => $file->id]) }}">{{ __('Download') }}</a>
                <a class="btn btn-link" href="{{ route('contracts.smm.delete-file', ['contract' => $contract->id, 'file' => $file->id]) }}">Удалить</a>
            </div>
        @endforeach
    @endif
    <div class="form-group">
        <label>Название файла</label>
        <input name="files[0][name]" type="text" class="form-control @if ($errors->has('files.0.name')) is-invalid @endif" value="{{ old('files.0.name') }}"/>
        @if ($errors->has('files.0.name'))
            <div class="invalid-feedback">{{ $errors->first('files.0.name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label class="custom-file">
            <input type="file" name="files[0][file]" class="custom-file-input @if ($errors->has('files.0.file')) is-invalid @endif">
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('files.0.file'))
            <div class="invalid-feedback d-block">{{ $errors->first('files.0.file') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Start Date') }}</label>
        <input type="text" name="start_date" class="datepicker form-control @if ($errors->has('start_date')) is-invalid @endif" value="{{ old('start_date') ?: ($contract instanceof \App\SMMContract ? $contract->start_date : '')}}">
        @if ($errors->has('start_date'))
            <div class="invalid-feedback">{{ $errors->first('start_date') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Amount') }}</label>
        <input type="text" name="amount" class="form-control @if ($errors->has('amount')) is-invalid @endif" value="{{ old('amount') ?: ($contract instanceof \App\SMMContract ? $contract->amount : '')}}">
        @if ($errors->has('amount'))
            <div class="invalid-feedback">{{ $errors->first('amount') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Budget for Target') }}</label>
        <input type="text" name="budget" class="form-control @if ($errors->has('budget')) is-invalid @endif" value="{{ old('budget') ?: ($contract instanceof \App\SMMContract ? $contract->budget : '')}}">
        @if ($errors->has('budget'))
            <div class="invalid-feedback">{{ $errors->first('budget') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Способ оплаты</label>
        <select name="payment_method_id" class="@if ($errors->has('payment_method_id')) is-invalid @endif">
            <option value="">Не указан</option>
            @foreach (\App\PaymentMethod::all() as $paymentMethod)
                <option value="{{ $paymentMethod->id }}" @if (old('payment_method_id') == $paymentMethod->id) selected @elseif ($contract instanceof \App\SMMContract && $contract->paymentMethod && $contract->paymentMethod->id == $paymentMethod->id) selected @endif>{{ $paymentMethod->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('payment_method_id'))
            <div class="invalid-feedback">{{ $errors->first('payment_method_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Metrika') }}</label>
        <input type="text" name="metrika_link" class="form-control" value="{{ old('metrika') ?: ($contract instanceof \App\SMMContract ? $contract->metrika_link : '')}}">
    </div>
    <div class="form-group">
        <label>{{ __('Social Media') }}</label>
        <textarea name="social_media" class="form-control">{{ old('social_media') ?: ($contract instanceof \App\SMMContract ? $contract->social_media : '')}}</textarea>
    </div>
    <div class="form-group">
        <label>{{ __('Marketer') }}</label>
        <select name="marketer_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                <option value="{{ $marketer->id }}" @if (old('marketer_id') == $marketer->id) selected @elseif ($contract instanceof \App\SMMContract && $contract->marketer && $contract->marketer->id == $marketer->id) selected @endif>{{ $marketer->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Salesman') }}</label>
        <select name="salesman_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                <option value="{{ $salesman->id }}" @if (old('salesman_id') == $salesman->id) selected @elseif ($contract instanceof \App\SMMContract && $contract->salesman && $contract->salesman->id == $salesman->id) selected @endif>{{ $salesman->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Responsible Person') }}</label>
        <select name="responsible_person_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('user_digital_smm')->get() as $user)
                <option value="{{ $user->id }}" @if (old('responsible_person_id') == $user->id) selected @elseif ($contract instanceof \App\SMMContract && $contract->responsible && $contract->responsible->id == $user->id) selected @endif>{{ $user->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>