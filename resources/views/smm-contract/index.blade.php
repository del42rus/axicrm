@php

use App\User;
use App\Client;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('SMM') }}</h3>

    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <input class="form-control" name="contract_number" placeholder="{{ __('Contract Number') }}" value="{{ request('contract_number') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">{{ __('Client') }}</option>
                    @foreach (Client::hasContractType('smm') as $client)
                        <option value="{{ $client->id }}" @if (request('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (Client::hasContractType('smm') as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">{{ __('Marketer') }}</option>
                    @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="salesman_id">
                    <option value="">{{ __('Salesman') }}</option>
                    @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                        <option value="{{ $salesman->id }}" @if (request('salesman_id') == $salesman->id) selected @endif>{{ $salesman->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="responsible_person_id">
                    <option value="">{{ __('Responsible Person') }}</option>
                    @foreach (User::whereRoleIs('user_digital_smm')->get() as $user)
                        <option value="{{ $user->id }}" @if (request('responsible_person_id') == $user->id) selected @endif>{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
    </form>

    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link @if ($status == 'active') active @endif" href="{{ route('contracts.smm.index', ['status' => 'active']) }}">{{ __('Active') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'declined') active @endif" href="{{ route('contracts.smm.index', ['status' => 'declined']) }}">{{ __('Declined') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'deleted') active @endif" href="{{ route('contracts.ca.index', ['status' => 'deleted']) }}">Архив</a>
        </li>
    </ul>

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Start Date') }}</th>
            @if ($status == 'declined')
                <th>Дата отказа</th>
            @endif
            <th>{{ __('Legal Name') }}</th>
            <th>{{ __('Brand') }}</th>
            <th>{{ __('Social Media') }}</th>
            <th>{{ __('Scanned Copy') }}</th>
            <th>{{ __('Amount') }}, {{ __('RUB') }}</th>
            <th>{{ __('Budget for Target') }}, {{ __('RUB') }}</th>
            <th>Способ оплаты</th>
            <th>{{ __('Marketer') }}</th>
            <th>{{ __('Responsible Person') }}</th>
            <th>{{ __('Salesman') }}</th>
            <th>{{ __('Metrika') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contracts as $contract)
            <tr @if ($contract->isRecent()) class="table-success" @endif>
                <td><a href="{{ route('contracts.smm.show', ['contract' => $contract]) }}">{{ $contract->contract->contract_number }}</a></td>
                <td>{{ $contract->start_date }}</td>
                @if ($contract->isDeclined())
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <td><a href="{{ route('clients.show', ['id' => $contract->contract->client->id]) }}">{{ $contract->contract->client->getFullName() }}</a></td>
                <td>{{ $contract->contract->client->brand }}</td>
                <td><a href="{{ $contract->social_media }}" target="_blank">{{ $contract->social_media }}</a></td>
                <td>
                    @if ($contract->scanned_copy)
                        <a href="{{ route('contracts.smm.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
                        <br/>
                        @php
                            $extension = File::extension(storage_path('app/public/' . $contract->scanned_copy));
                        @endphp
                        @if ($extension == 'pdf')
                            <a data-fancybox data-type="iframe" data-src="/web/viewer.html?file=/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @else
                            <a data-fancybox data-type="iframe" data-src="/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @endif
                    @endif
                </td>
                <td>{{ $contract->getFormattedAmount() }}</td>
                <td>{{ $contract->getFormattedBudget() }}</td>
                <td>{{ $contract->paymentMethod ? $contract->paymentMethod->name : '' }}</td>
                <td>{{ $contract->marketer ? $contract->marketer->name : ''}}</td>
                <td>{{ $contract->responsible ? $contract->responsible->name : '' }}</td>
                <td>{{ $contract->salesman ? $contract->salesman->name : '' }}</td>
                <td><a href="{{ $contract->metrika_link }}">{{ $contract->metrika_link }}</a></td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @if ($contract->trashed())
                            @permission('restore-smm-contracts')
                                <a class="btn btn-success" title="Восстановить" href="{{ route('contracts.ca.restore', ['id' => $contract->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('delete-smm-contracts')
                                <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('contracts.ca.force-delete', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                        @else
                            @permission('update-smm-contracts')
                                <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('contracts.smm.edit', ['id' => $contract->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('soft-delete-smm-contracts')
                                <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('contracts.smm.destroy', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                            @if ($contract->isActive())
                                @permission('decline-smm-contracts')
                                    <a class="btn btn-warning text-white" title="{{ __('Decline') }}" data-href="{{ route('contracts.smm.decline', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-cancellation"><i class="fa fa-close" aria-hidden="true"></i></a>
                                @endpermission
                            @else
                                @permission('activate-smm-contracts')
                                    <a class="btn btn-success" title="{{ __('Activate') }}" href="{{ route('contracts.smm.activate', ['id' => $contract->id]) }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                @endpermission
                            @endif
                        @endif
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="15">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $contracts->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
    @include('partial.confirm-cancellation')
@endsection