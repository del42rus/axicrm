<div class="modal fade" id="confirm-cancellation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input name="backUrl" type="hidden" value="{{ url()->current() }}">
                <div class="modal-header">
                    {{ __('Confirm Action') }}
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Дата рассторжения</label>
                        <input type="text" name="cancellation_date" class="datepicker form-control" value="{{ date('Y-m-d') }}">
                    </div>
                    <div class="form-group">
                        <label class="d-block">Письмо о рассторжении</label>
                        <label class="custom-file">
                            <input type="file" name="cancellation_letter" class="custom-file-input" @if (isset($contract) && !$contract->isDeclined()) required @endif>
                            <span class="custom-file-control"></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Дополнительное соглашение о рассторжении</label>
                        <label class="custom-file">
                            <input type="file" name="cancellation_agreement" class="custom-file-input">
                            <span class="custom-file-control"></span>
                        </label>
                    </div>
                </div>
                @if (isset($contract) && $contract->isDeclined())
                    <input type="hidden" name="contract_id" value="{{$contract->id}}">
                @endif
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                    <button class="btn btn-primary" type="submit">{{ __('OK') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>