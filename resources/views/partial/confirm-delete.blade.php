<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Подтверждение действия
            </div>
            <div class="modal-body">
                Вы действительно хотите поместить запись в архив?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>

                <form action="" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input name="backUrl" type="hidden" value="{{ url()->current() }}">
                    <button class="btn btn-danger">Поместить в архив</button>
                </form>
            </div>
        </div>
    </div>
</div>