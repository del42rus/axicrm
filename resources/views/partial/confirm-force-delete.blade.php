<div class="modal fade" id="confirm-force-delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                {{ __('Confirm Delete') }}
            </div>
            <div class="modal-body">
                {{ __('Are you sure you want to delete this item?') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>

                <form action="" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input name="backUrl" type="hidden" value="{{ url()->current() }}">
                    <button class="btn btn-danger">{{ __('Delete') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>