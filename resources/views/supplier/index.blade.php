@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Suppliers') }}</h3>
    <p>
        @permission('create-suppliers')
            <a class="btn btn-secondary" href="{{ route('suppliers.create') }}">{{ __('Create Supplier') }}</a>
        @endpermission

        @permission('export-suppliers')
            <a class="btn btn-primary" href="{{ route('suppliers.download') }}"><i class="fa fa-file-excel-o"></i>&nbsp;{{ __('To Excel') }}</a>
        @endpermission
    </p>
    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <select name="legal_name">
                    <option value="">{{ __('Supplier') }}</option>
                    @foreach (App\Supplier::all() as $supplier)
                        <option value="{{ $supplier->legal_name }}" @if (request('legal_name') == $supplier->legal_name) selected @endif>{{ $supplier->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (App\Supplier::all() as $supplier)
                        <option value="{{ $supplier->brand }}" @if (request('brand') == $supplier->brand) selected @endif>{{ $supplier->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="site" placeholder="{{ __('Site') }}" value="{{ Request::get('site') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="activity_id">
                    <option value="">Вид деятельности</option>
                    @foreach (\App\Activity::all() as $activity)
                        <option value="{{ $activity->id }}" @if (request('activity_id') == $activity->id) selected @endif>{{ $activity->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <div class="form-check mt-2">
                    <input class="form-check-input ml-0" type="checkbox" id="checkRemoved" name="removed" value="1" @if (request('removed') == '1') checked @endif>
                    <label class="form-check-label" for="checkRemoved">Архив</label>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
            </div>
        </div>
    </form>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Legal Name') }}</th>
            <th>{{ __('Brand') }}</th>
            <th>{{ __('Kind of Activity') }}</th>
            <th>{{ __('Site') }}</th>
            <th>{{ __('Contract Scan') }}</th>
            @permission('read-contacts')
                <th>{{ __('Contacts') }}</th>
            @endpermission
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->id }}</td>
                    <td><a href="{{ route('suppliers.show', ['id' => $supplier->id]) }}">{{ $supplier->getFullName() }}</a></td>
                    <td>{{ $supplier->brand }}</td>
                    <td>{{ $supplier->activity ? $supplier->activity->name : ''}}</td>
                    <td><a href="{{ $supplier->site }}" target="_blank">{{ $supplier->site }}</a></td>
                    <td><a href="{{ route('suppliers.contract.download', ['supplier' => $supplier->id]) }}">{{ __('Download') }}</a></td>
                    @permission('read-contacts')
                        <td><a href="{{ route('suppliers.contacts', ['id' => $supplier->id]) }}">{{ __('View Contacts') }}</a></td>
                    @endpermission
                    <td class="text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            @if ($supplier->trashed())
                                @permission('restore-suppliers')
                                    <a class="btn btn-success" title="Восстановить" href="{{ route('suppliers.restore', ['id' => $supplier->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                                @endpermission
                                @permission('delete-suppliers')
                                    <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('suppliers.force-delete', ['id' => $supplier->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                @endpermission
                            @else
                                @permission('update-suppliers')
                                    <a class="btn btn-secondary" title="Редактировать" href="{{ route('suppliers.edit', ['id' => $supplier->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                @endpermission
                                @permission('soft-delete-suppliers')
                                    <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('suppliers.destroy', ['id' => $supplier->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                @endpermission
                            @endif
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="8">{{ __('Nothing found') }}</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $suppliers->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
@endsection
