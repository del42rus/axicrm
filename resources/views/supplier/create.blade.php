@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">{{ __('Suppliers') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Supplier') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('supplier.partial.form')
        </div>
    </div>
@endsection
