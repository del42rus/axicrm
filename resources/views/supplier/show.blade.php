@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('suppliers.index') }}">{{ __('Suppliers') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Supplier') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <table class="table">
                <tr>
                    <td><strong>{{ __('Legal Name') }}</strong></td>
                    <td>{{ $supplier->legal_name }}, {{ $supplier->ownershipType->name }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Brand') }}</strong></td>
                    <td>{{ $supplier->brand }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Kind of Activity') }}</strong></td>
                    <td>{{ $supplier->activity ? $supplier->activity->name : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Site') }}</strong></td>
                    <td><a href="{{ $supplier->site }}" target="_blank">{{ $supplier->site }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Actual Address') }}</strong></td>
                    <td>{{ $supplier->actualAddress ? $supplier->actualAddress->city->name . ', ' . $supplier->actualAddress->full_address : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Legal Address') }}</strong></td>
                    <td>{{ $supplier->legalAddress ? $supplier->legalAddress->city->name . ', ' . $supplier->legalAddress->full_address : '' }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection