@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.additional.index') }}">Доп. работы ПО</a></li>
        <li class="breadcrumb-item">{{ $contract->contract->client->getFullName() }}</li>
    </ol>
    <h3 class="page-header">{{ __('Edit Contract') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('additional-contract.partial.form', ['contract' => $contract])
        </div>
    </div>
@endsection
