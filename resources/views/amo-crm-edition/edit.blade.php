@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('amo-crm-editions.index') }}">Редакции AmoCRM</a></li>
    </ol>
    <h3 class="page-header">Редактировать редакцию</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('amo-crm-edition.partial.form', ['edition' => $edition])
        </div>
    </div>
@endsection
