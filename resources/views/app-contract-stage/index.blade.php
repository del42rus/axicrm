@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('App Contract Stages') }}</h3>

    @permission('create-dictionaries')
        <p>
            <a class="btn btn-secondary" href="{{ route('app-contract-stages.create') }}">{{ __('Create Stage') }}</a>
        </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Stage') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($stages as $stage)
            <tr>
                <td>{{ $stage->id }}</td>
                <td>{{ $stage->name }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                            <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('app-contract-stages.edit', ['id' => $stage->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                            <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('app-contract-stages.destroy', ['id' => $stage->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-force-delete')
@endsection