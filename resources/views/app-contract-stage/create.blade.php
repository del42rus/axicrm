@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('app-contract-stages.index') }}">{{ __('App Contract Stages') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Stage') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('app-contract-stage.partial.form')
        </div>
    </div>
@endsection
