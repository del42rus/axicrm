<form method="post" action="@if (isset($stage)){{ route('app-contract-stages.update', $stage->id) }}@else{{ route('app-contract-stages.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($stage))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $stage->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Stage Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($stage) ? $stage->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>