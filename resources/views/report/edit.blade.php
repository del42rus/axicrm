@php

use App\User;

@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Edit Report') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <form method="post" action="@if (isset($report)){{ route('reports.update', $report->id) }}@else{{ route('reports.store') }}@endif" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}

                @if (isset($report))
                    {{ method_field('PUT') }}
                    <input type="hidden" name="id" value="{{ $report->id }}">
                @endif
                <div class="form-group">
                    <label class="custom-file">
                        <input type="file" name="report_file" class="custom-file-input @if ($errors->has('report_file')) is-invalid @endif" required>
                        <span class="custom-file-control"></span>
                    </label>
                    @if ($errors->has('report_file'))
                        <div class="invalid-feedback d-block">{{ $errors->first('report_file') }}</div>
                    @endif
                    @if (isset($report) && $report->report_file)
                        <a class="btn btn-link" href="{{ route('reports.download-report', ['report' => $report->id]) }}">{{ __('Download') }}</a>
                    @endif
                </div>
                <div class="form-group">
                    <label class="custom-file">
                        <input type="file" name="act_file" class="custom-file-input @if ($errors->has('act_file')) is-invalid @endif" required>
                        <span class="custom-file-control"></span>
                    </label>
                    @if ($errors->has('act_file'))
                        <div class="invalid-feedback d-block">{{ $errors->first('act_file') }}</div>
                    @endif
                    @if (isset($report) && $report->act_file)
                        <a class="btn btn-link" href="{{ route('reports.download-act', ['report' => $report->id]) }}">{{ __('Download') }}</a>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('Act Total') }}</label>
                    <input type="text" name="act_total" class="form-control @if ($errors->has('act_total')) is-invalid @endif" value="{{ old('act_total') ?: (isset($report) ? $report->act_total : '')}}">
                    @if ($errors->has('act_total'))
                        <div class="invalid-feedback">{{ $errors->first('act_total') }}</div>
                    @endif
                </div>
                @permission('update-reports')
                    <div class="form-group">
                        <label>{{ __('Marketer') }}</label>
                        <select name="marketer_id">
                            <option>{{ __('None') }}</option>
                            @foreach (User::whereRoleIs('marketer')->get() as $marketer)
                                <option value="{{ $marketer->id }}" @if (old('marketer_id') == $marketer->id) selected @elseif (isset($report) && $report->marketer->id == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Responsible Person') }}</label>
                        <select name="responsible_person_id">
                            <option>{{ __('None') }}</option>
                            @foreach (User::all() as $user)
                                <option value="{{ $user->id }}" @if (old('responsible_person_id') == $user->id) selected @elseif (isset($report) && $report->responsible->id == $user->id) selected @endif>{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endpermission
                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
        </div>
    </div>
@endsection
