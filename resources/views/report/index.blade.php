@php

use App\User;
use App\Client;

$months = [__('January'), __('February'), __('March'), __('April'), __('May'), __('June'), __('July'), __('August'), __('September'), __('October'), __('November'), __('December'),]
@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">Отчеты ОАиП</h3>
    @permission('read-reports')
    <p>
        <a class="btn btn-secondary btn-sm" target="_blank" href="{{ route('reports.generate') }}">Сгенерировать</a>
    </p>
    @endpermission
    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link @if ($contractType == 'ca') active @endif" href="{{ route('reports.index', ['type' => 'ca']) }}">{{ __('Contextual Advertising') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($contractType == 'seo') active @endif" href="{{ route('reports.index', ['type' => 'seo']) }}">{{ __('Seo') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($contractType == 'smm') active @endif" href="{{ route('reports.index', ['type' => 'smm']) }}">{{ __('SMM') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($contractType == 'adm') active @endif" href="{{ route('reports.index', ['type' => 'adm']) }}">{{ __('Administration') }}</a>
        </li>
    </ul>

    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <input class="form-control" name="contract_number" placeholder="{{ __('Contract Number') }}" value="{{ request('contract_number') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">{{ __('Client') }}</option>
                    @foreach (Client::hasContractType($contractType) as $client)
                        <option value="{{ $client->id }}" @if (request('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (Client::hasContractType($contractType) as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="year">
                    <option value="">{{ __('Select Year') }}</option>
                    @foreach ((range(2007, date('Y'))) as $year)
                        <option value="{{ $year }}" @if (request('year') == $year) selected @endif>{{ $year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="month">
                    <option value="">{{ __('Select Month') }}</option>
                    @foreach ($months as $key => $month)
                        <option value="{{ $key + 1 }}" @if (request('month') == $key + 1) selected @endif>{{ $month }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">{{ __('Marketer') }}</option>
                    @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="responsible_person_id">
                    <option value="">{{ __('Responsible Person') }}</option>
                    @foreach (User::whereRoleIs('user_digital')->get() as $user)
                        <option value="{{ $user->id }}" @if (request('responsible_person_id') == $user->id) selected @endif>{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
            </div>
        </div>
    </form>

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Legal Name') }}</th>
            <th>
                {{ __('Brand') }}
            </th>
            <th>{{ __('Contract Number') }}</th>
            <th>
                <a href="{{ route('reports.index', ['type' => $contractType, 'sort' => (request('sort') == '-year' || !request('sort')) ? 'year' : '-year']) }}">
                    {{ __('Year') }}
                    @if (in_array(request('sort'), ['year', '-year']))
                        <i class="fa fa-sort-alpha-{{ request('sort') == '-year' ? 'desc' : 'asc' }}"></i>
                    @endif
                </a>
            </th>
            <th>{{ __('Month') }}</th>
            <th>{{ __('Report File') }}</th>
            <th>{{ __('Act File') }}</th>
            <th>{{ __('Act Total') }}</th>
            <th>{{ __('Marketer') }}</th>
            <th>{{ __('Responsible Person') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($reports as $report)
            <tr @if (!($report->getYear() == date('Y') && $report->getMonth() == date('m') && date('m') <= 15) && !($report->act_file && $report->report_file))class="table-danger"@endif>
                <td>{{ $report->id }}</td>
                <td><a href="{{ route('clients.show', ['id' => $report->contract->registry->client->id]) }}">{{ $report->contract->registry->client->getFullName() }}</a></td>
                <td>{{ $report->contract->registry->client->brand }}</td>
                <td><a href="{{ route('contracts.' . $report->contract->registry->contract_type . '.show', ['id' => $report->contract]) }}">{{ $report->contract->registry->contract_number }}</a></td>
                <td>{{ $report->created_at->format('Y') }}</td>
                <td>{{ $report->created_at->format('m') }}</td>
                <td>@if ($report->report_file)<a href="{{ route('reports.download-report', ['report' => $report->id]) }}">{{ __('Download') }}@endif</a></td>
                <td>@if ($report->act_file)<a href="{{ route('reports.download-act', ['report' => $report->id]) }}">{{ __('Download') }}</a>@endif</td>
                <td>{{ $report->act_total }}</td>
                <td>{{ $report->contract->marketer ? $report->contract->marketer->name : '' }}</td>
                <td>{{ $report->contract->responsible ? $report->contract->responsible->name : '' }}</td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        @if ((Auth::user()->hasPermission('update-reports-limited') && ($report->getYear() == date('Y') && $report->getMonth() == date('m') && date('m') <= 15)) || Auth::user()->hasPermission('update-reports'))
                            <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('reports.edit', ['id' => $report->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-reports')
                            <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('reports.destroy', ['id' => $report->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="12">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $reports->links('vendor.pagination.bootstrap-4') }}
    @include('partial.confirm-force-delete')
@endsection