<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/flatpickr.min.css">
    <link rel="stylesheet" href="/css/selectize.default.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/js/jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="/js/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
</head>
<body>
<div class="wrap">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            @if (Auth::check())
                <ul class="navbar-nav mr-auto">
                    @permission('read-users')
                        <li class="nav-item {{ Nav::isRoute('users.index') }}">
                            <a class="nav-link" href="{{ route('users.index') }}">{{ __('Users') }}</a>
                        </li>
                    @endpermission
                    @permission('read-acl')
                        <li class="nav-item dropdown {{ Nav::isRoute('roles.index') }}{{ Nav::isRoute('permissions.index') }}">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ __('Roles & Permissions') }}</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('roles.index') }}">{{ __('Roles') }}</a>
                                <a class="dropdown-item" href="{{ route('permissions.index') }}">{{ __('Permissions') }}</a>
                            </div>
                        </li>
                    @endpermission

                    @if (Auth::user()->hasPermission('read-dictionaries'))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ __('Dictionaries') }}</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('app-contract-stages.index') }}">{{ __('App Contract Stages') }}</a>
                                <a class="dropdown-item" href="{{ route('ownership-types.index') }}">{{ __('Ownership Types') }}</a>
                                <a class="dropdown-item" href="{{ route('positions.index') }}">{{ __('Positions') }}</a>
                                <a class="dropdown-item" href="{{ route('activities.index') }}">{{ __('Activities') }}</a>
                                <a class="dropdown-item" href="{{ route('hosting-services.index') }}">{{ __('Hosting Services') }}</a>
                                <a class="dropdown-item" href="{{ route('regions.index') }}">{{ __('Regions') }}</a>
                                <a class="dropdown-item" href="{{ route('cities.index') }}">{{ __('Cities') }}</a>
                                <a class="dropdown-item" href="{{ route('payment-methods.index') }}">Способы оплаты</a>
                                <a class="dropdown-item" href="{{ route('bitrix-editions.index') }}">Редакции CMS Bitrix</a>
                                <a class="dropdown-item" href="{{ route('bitrix-crm-editions.index') }}">Редакции CRM Bitrix24</a>
                                <a class="dropdown-item" href="{{ route('amo-crm-editions.index') }}">Редакции AmoCRM</a>
                            </div>
                        </li>
                    @endif

                    @if (Auth::user()->hasPermission('read-clients') || Auth::user()->hasPermission('read-suppliers'))
                        <li class="nav-item dropdown {{ Nav::isRoute('clients.index') }}{{ Nav::isRoute('suppliers.index') }}">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ __('Agents') }}</a>
                            <div class="dropdown-menu">
                                @permission('read-clients')
                                    <a class="dropdown-item" href="{{ route('clients.index') }}">{{ __('Clients') }}</a>
                                @endpermission
                                @permission('read-suppliers')
                                    <a class="dropdown-item" href="{{ route('suppliers.index') }}">{{ __('Suppliers') }}</a>
                                @endpermission
                            </div>
                        </li>
                    @endif

                    @permission('read-contacts')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ __('Contacts') }}</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ route('contacts.clients') }}">{{ __('Clients') }}</a>
                                <a class="dropdown-item" href="{{ route('contacts.suppliers') }}">{{ __('Suppliers') }}</a>
                            </div>
                        </li>
                    @endpermission
                    @if (Auth::user()->hasPermission('read-ca-contracts') ||
                        Auth::user()->hasPermission('read-seo-contracts') ||
                        Auth::user()->hasPermission('read-smm-contracts') ||
                        Auth::user()->hasPermission('read-app-contracts') ||
                        Auth::user()->hasPermission('read-hosting-contracts') ||
                        Auth::user()->hasPermission('read-adm-contracts') ||
                        Auth::user()->hasPermission('read-ci-contracts') ||
                        Auth::user()->hasPermission('read-contracts') ||
                        Auth::user()->hasPermission('read-additional-contracts') ||
                        Auth::user()->hasPermission('read-additional-digital-contracts') ||
                        Auth::user()->hasPermission('read-bitrix-contracts') ||
                        Auth::user()->hasPermission('read-amo-crm-contracts') ||
                        Auth::user()->hasPermission('read-bitrix-crm-contracts'))
                        <li class="nav-item dropdown {{ Nav::urlDoesContain('contracts') }}">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('Contracts') }}
                            </a>
                            <div class="dropdown-menu">
                                @permission('read-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.index') }}">{{ __('Registry Of Contracts') }}</a>
                                    <div class="dropdown-divider"></div>
                                @endpermission
                                @permission('read-ca-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.ca.index') }}">{{ __('Contextual Advertising') }}</a>
                                @endpermission
                                @permission('read-seo-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.seo.index') }}">{{ __('SEO') }}</a>
                                @endpermission
                                @permission('read-smm-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.smm.index') }}">{{ __('SMM') }}</a>
                                @endpermission
                                @permission('read-app-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.app.index') }}">{{ __('Site & Apps') }}</a>
                                @endpermission
                                @permission('read-adm-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.adm.index') }}">{{ __('Administration') }}</a>
                                @endpermission
                                @permission('read-ci-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.ci.index') }}">{{ __('Corporate Identity') }}</a>
                                @endpermission
                                @permission('read-hosting-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.hosting.index') }}">{{ __('Hosting') }}</a>
                                @endpermission
                                @permission('read-additional-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.additional.index') }}">Доп. работы ПО</a>
                                @endpermission
                                @permission('read-additional-digital-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.additional-digital.index') }}">Доп. работы ОАиП</a>
                                @endpermission
                                @permission('read-bitrix-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.bitrix.index') }}">CMS:1С-Битрикс</a>
                                @endpermission
                                @permission('read-bitrix-crm-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.bitrix-crm.index') }}">CRM:Битрикс-24</a>
                                @endpermission
                                @permission('read-amo-crm-contracts')
                                    <a class="dropdown-item" href="{{ route('contracts.amo-crm.index') }}">CRM:Amo.CRM</a>
                                @endpermission
                            </div>
                        </li>
                    @endpermission
                </ul>
            @endif
            <ul class="navbar-nav ml-auto">
                @if (Auth::check())
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @permission('read-profile')
                                <a class="dropdown-item" href="{{ route('profile') }}">{{ __('Profile') }}</a>
                            @endpermission
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </div>
                    </li>
                @else
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                @endif
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
        @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('success') }}
            </div>
        @endif

        @if (Session::has('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('danger') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="m-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @yield('content')
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <p class="pull-left">&copy; {{ config('app.name') }} <?= date('Y') ?></p>
    </div>
</footer>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/flatpickr.min.js"></script>
<script src="/js/flatpickr/l10n/ru.js"></script>
<script src="/js/microplugin.js"></script>
<script src="/js/sifter.min.js"></script>
<script src="/js/selectize.min.js"></script>
<script src="/js/jquery.inputmask.bundle.min.js"></script>
<script src="/js/jquery.fancybox.min.js"></script>
<script src="/js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="/js/app.js"></script>
@yield('js')
</body>
</html>
