@php

use App\User;
use App\Client;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">CRM:Amo.CRM</h3>

    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <input class="form-control" name="contract_number" placeholder="Номер договора" value="{{ request('contract_number') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">Клиент</option>
                    @foreach (Client::hasContractType('amo-crm') as $client)
                        <option value="{{ $client->id }}" @if (request('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">Бренд</option>
                    @foreach (Client::hasContractType('amo-crm') as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="finish_date" placeholder="Срок окончания" value="{{ request('finish_date') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">Интернет-маркетолог</option>
                    @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="salesman_id">
                    <option value="">Продажник</option>
                    @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                        <option value="{{ $salesman->id }}" @if (request('salesman_id') == $salesman->id) selected @endif>{{ $salesman->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
    </form>

    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link @if ($status == 'active') active @endif" href="{{ route('contracts.amo-crm.index', ['status' => 'active']) }}">{{ __('Active') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'declined') active @endif" href="{{ route('contracts.amo-crm.index', ['status' => 'declined']) }}">{{ __('Declined') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'deleted') active @endif" href="{{ route('contracts.amo-crm.index', ['status' => 'deleted']) }}">Архив</a>
        </li>
    </ul>

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Дата начала</th>
            <th>Срок окончания</th>
            @if ($status == 'declined')
                <th>Дата отказа</th>
            @endif
            <th>Юридическое название</th>
            <th>Бренд</th>
            <th>Сайт</th>
            <th>Скан договора</th>
            <th>Стоимость CRM, руб</th>
            <th>Стоимость продления CRM, руб</th>
            <th>Редакция CRM</th>
            <th>Продажник</th>
            <th>Действие</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contracts as $contract)
            <tr @if ($contract->isRecent()) class="table-success" @elseif ($contract->isExpired()) class="table-danger" @elseif($contract->isExpiring()) class="table-warning" @endif>
                <td><a href="{{ route('contracts.amo-crm.show', ['contract' => $contract]) }}" target="_blank">{{ $contract->contract->contract_number }}</a></td>
                <td>{{ $contract->start_date }}</td>
                <td>{{ $contract->finish_date }}</td>
                @if ($contract->isDeclined())
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <td><a href="{{ route('clients.show', ['id' => $contract->contract->client->id]) }}">{{ $contract->contract->client->getFullName() }}</a></td>
                <td>{{ $contract->contract->client->brand }}</td>
                <td><a href="{{ $contract->site }}" target="_blank">{{ $contract->site }}</a></td>
                <td>
                    @if ($contract->scanned_copy)
                        <a href="{{ route('contracts.amo-crm.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
                        <br/>
                        @php
                            $extension = File::extension(storage_path('app/public/' . $contract->scanned_copy));
                        @endphp
                        @if ($extension == 'pdf')
                            <a data-fancybox data-type="iframe" data-src="/web/viewer.html?file=/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @else
                            <a data-fancybox data-type="iframe" data-src="/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @endif
                    @endif
                </td>
                <td>{{ $contract->getFormattedCost() }}</td>
                <td>{{ $contract->getFormattedRenewalCost() }}</td>
                <td>{{ $contract->amoCRMEdition ? $contract->amoCRMEdition->name : '' }}</td>
                <td>@if ($contract->salesman) {{ $contract->salesman->name }} @endif</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @if ($contract->trashed())
                            @permission('restore-amo-crm-contracts')
                                <a class="btn btn-success" title="Восстановить" href="{{ route('contracts.amo-crm.restore', ['id' => $contract->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('delete-amo-crm-contracts')
                                <button class="btn btn-danger" title="Удалить" data-href="{{ route('contracts.amo-crm.force-delete', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                        @else
                            @permission('update-amo-crm-contracts')
                                <a class="btn btn-secondary" title="Редактировать" href="{{ route('contracts.amo-crm.edit', ['id' => $contract->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('soft-delete-amo-crm-contracts')
                                <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('contracts.amo-crm.destroy', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                            @if ($contract->isActive())
                                @permission('decline-amo-crm-contracts')
                                <a class="btn btn-warning text-white" title="Отклонить" data-href="{{ route('contracts.amo-crm.decline', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-cancellation"><i class="fa fa-close" aria-hidden="true"></i></a>
                                @endpermission
                                <a class="btn btn-success text-white" title="Продлить" data-href="{{ route('contracts.amo-crm.renew', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-renewal"><i class="fa fa-clock-o" aria-hidden="true"></i></a>
                            @else
                                @permission('activate-amo-crm-contracts')
                                <a class="btn btn-success" title="Активировать" href="{{ route('contracts.amo-crm.activate', ['id' => $contract->id]) }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                @endpermission
                            @endif
                        @endif
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="13">Ничего не найдено</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $contracts->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
    @include('partial.confirm-cancellation')

    <div class="modal fade" id="confirm-renewal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input name="backUrl" type="hidden" value="{{ url()->current() }}">
                    <div class="modal-header">
                        Подтвердить действие
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Срок окончания</label>
                            <input type="text" name="finish_date" class="datepicker form-control" value="{{ date('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label>Стоимость продления CMS, руб</label>
                            <input type="text" name="renewal_cost" class="form-control" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                        <button class="btn btn-primary" type="submit">Применить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('input[name="finish_date"]:not(.datepicker)').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'mm.yy',
            currentText: 'Сегодня',
            closeText: 'Применить',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
            onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });
    })
</script>
@endsection