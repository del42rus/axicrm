@php

use App\User;

@endphp
<form method="post" action="@if ($contract instanceof \App\AmoCRMContract){{ route('contracts.amo-crm.update', $contract->id) }}@else{{ route('contracts.amo-crm.store') }}@endif" enctype="multipart/form-data" novalidate>
    {{ csrf_field() }}

    @if ($contract instanceof \App\AmoCRMContract)
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @else
        <input type="hidden" name="contract_id" value="{{ $contract->id }}">
    @endif
    <div class="form-group">
        <label>Номер договора</label>
        <input type="text" name="number" class="form-control" value="{{ $contract instanceof \App\AmoCRMContract ? $contract->contract->contract_number : $contract->contract_number }}" readonly disabled>
    </div>
    <div class="form-group">
        <label class="d-block">Скан договора</label>
        <label class="custom-file">
            <input type="file" name="scanned_copy" class="custom-file-input @if ($errors->has('scanned_copy')) is-invalid @endif" required>
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('scanned_copy'))
            <div class="invalid-feedback d-block">{{ $errors->first('scanned_copy') }}</div>
        @endif
        @if ($contract instanceof \App\AmoCRMContract && $contract->scanned_copy)
            <a class="btn btn-link" href="{{ route('contracts.amo-crm.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
        @endif
    </div>
    @if ($contract instanceof \App\AmoCRMContract)
        @foreach ($contract->files as $file)
            <div class="form-group">
                <label>Название файла</label>
                <input name="files[id_{{$file->id}}][name]" type="text" class="form-control @if ($errors->has('files.id_' . $file->id .'.name')) is-invalid @endif" value="{{ old('files.id_' . $file->id . '.name') ? : $file->name }}"/>
                @if ($errors->has('files.id_' . $file->id . '.name'))
                    <div class="invalid-feedback">{{ $errors->first('files.id_' . $file->id . '.name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label class="custom-file">
                    <input type="file" name="files[id_{{$file->id}}][file]" class="custom-file-input @if ($errors->has('files.id_' . $file->id . '.file')) is-invalid @endif">
                    <span class="custom-file-control"></span>
                </label>
                @if ($errors->has('files.id_' . $file->id . '.file'))
                    <div class="invalid-feedback d-block">{{ $errors->first('files.id_' . $file->id . '.file') }}</div>
                @endif
                <a class="btn btn-link" href="{{ route('contracts.amo-crm.download-file', ['contract' => $contract->id, 'file' => $file->id]) }}">{{ __('Download') }}</a>
                <a class="btn btn-link" href="{{ route('contracts.amo-crm.delete-file', ['contract' => $contract->id, 'file' => $file->id]) }}">Удалить</a>
            </div>
        @endforeach
    @endif
    <div class="form-group">
        <label>Название файла</label>
        <input name="files[0][name]" type="text" class="form-control @if ($errors->has('files.0.name')) is-invalid @endif" value="{{ old('files.0.name') }}"/>
        @if ($errors->has('files.0.name'))
            <div class="invalid-feedback">{{ $errors->first('files.0.name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label class="custom-file">
            <input type="file" name="files[0][file]" class="custom-file-input @if ($errors->has('files.0.file')) is-invalid @endif">
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('files.0.file'))
            <div class="invalid-feedback d-block">{{ $errors->first('files.0.file') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Сайт</label>
        <input type="text" name="site" class="form-control" value="{{ old('site') ?: ($contract instanceof \App\AmoCRMContract ? $contract->site : '')}}">
    </div>
    <div class="form-group">
        <label>Дата начала</label>
        <input type="text" name="start_date" class="datepicker form-control @if ($errors->has('start_date')) is-invalid @endif" value="{{ old('start_date') ?: ($contract instanceof \App\AmoCRMContract ? $contract->start_date : '')}}">
        @if ($errors->has('start_date'))
            <div class="invalid-feedback">{{ $errors->first('start_date') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Срок окончания</label>
        <input type="text" name="finish_date" class="datepicker form-control @if ($errors->has('finish_date')) is-invalid @endif" value="{{ old('finish_date') ?: ($contract instanceof \App\AmoCRMContract ? $contract->finish_date : '')}}">
        @if ($errors->has('finish_date'))
            <div class="invalid-feedback">{{ $errors->first('finish_date') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Стоимость CMS, руб</label>
        <input type="text" name="cost" class="form-control @if ($errors->has('cost')) is-invalid @endif" value="{{ old('cost') ?: ($contract instanceof \App\AmoCRMContract ? $contract->cost : '')}}">
        @if ($errors->has('cost'))
            <div class="invalid-feedback">{{ $errors->first('cost') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Стоимость продления CMS, руб</label>
        <input type="text" name="renewal_cost" class="form-control @if ($errors->has('renewal_cost')) is-invalid @endif" value="{{ old('renewal_cost') ?: ($contract instanceof \App\AmoCRMContract ? $contract->renewal_cost : '')}}">
        @if ($errors->has('renewal_cost'))
            <div class="invalid-feedback">{{ $errors->first('renewal_cost') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Редакция CRM</label>
        <select name="amo_crm_edition_id" class="@if ($errors->has('amo_crm_edition_id')) is-invalid @endif">
            @foreach (\App\AmoCRMEdition::all() as $edition)
                <option value="{{ $edition->id }}" @if (old('amo_crm_edition_id') == $edition->id) selected @elseif ($contract instanceof \App\AmoCRMContract && $contract->amoCRMEdition && $contract->amoCRMEdition->id == $edition->id) selected @endif>{{ $edition->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('amo_crm_edition_id'))
            <div class="invalid-feedback">{{ $errors->first('amo_crm_edition_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Количество пользователей</label>
        <input type="text" name="user_count" class="form-control @if ($errors->has('user_count')) is-invalid @endif" value="{{ old('user_count') ?: ($contract instanceof \App\AmoCRMContract ? $contract->user_count : '')}}">
        @if ($errors->has('user_count'))
            <div class="invalid-feedback">{{ $errors->first('user_count') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Логин админа</label>
        <input type="text" name="admin_login" class="form-control @if ($errors->has('admin_login')) is-invalid @endif" value="{{ old('admin_login') ?: ($contract instanceof \App\AmoCRMContract ? $contract->admin_login : '')}}">
        @if ($errors->has('admin_login'))
            <div class="invalid-feedback">{{ $errors->first('admin_login') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Пароль админа</label>
        <input type="text" name="admin_password" class="form-control @if ($errors->has('admin_password')) is-invalid @endif" value="{{ old('admin_password') ?: ($contract instanceof \App\AmoCRMContract ? $contract->admin_password : '')}}">
        @if ($errors->has('admin_password'))
            <div class="invalid-feedback">{{ $errors->first('admin_password') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Продажник</label>
        <select name="salesman_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                <option value="{{ $salesman->id }}" @if (old('salesman_id') == $salesman->id) selected @elseif ($contract instanceof \App\AmoCRMContract && isset($contract->salesman) && $contract->salesman->id == $salesman->id) selected @endif>{{ $salesman->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>
@section('js')
    <script type="text/javascript">
        $(function() {
            $()
        })
    </script>
@endsection