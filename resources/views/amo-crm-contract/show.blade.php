@php
    use Illuminate\Support\Facades\File;
@endphp

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.amo-crm.index') }}">CRM:Amo.CRM</a></li>
    </ol>
    <h3 class="page-header">Информация по договору</h3>
    <div class="row">
        <div class="col-lg-6 col-md-9">
            <table class="table">
                <tr>
                    <td><strong>Номер договора</strong></td>
                    <td>{{ $contract->contract->contract_number }}</td>
                </tr>
                <tr>
                    <td><strong>Статус</strong></td>
                    <td>
                        @if ($contract->isActive())
                            Текущий
                        @else
                            Отказ
                            @permission('decline-amo-crm-contracts')
                                <a class="btn btn-warning text-white btn-sm" title="Отклонить" data-href="{{ route('contracts.amo-crm.decline', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-cancellation"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            <br />
                            @if ($contract->cancellation_letter)
                                <a href="{{ route('contracts.amo-crm.download-cancellation-letter', ['contract' => $contract]) }}">Письмо о расторжении договора</a><br />
                            @endif
                            @if ($contract->cancellation_agreement)
                                <a href="{{ route('contracts.amo-crm.download-cancellation-agreement', ['contract' => $contract]) }}">Дополнительное соглашение о расторжении договора</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @if ($contract->isDeclined())
                    <td><strong>Дата отказа</strong></td>
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <tr>
                    <td><strong>{{ __('Client') }}</strong></td>
                    <td><a href="{{ route('clients.show', ['id' => $contract->contract->client->id]) }}">{{ $contract->contract->client->getFullName() }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Scanned Copy') }}</strong></td>
                    <td>
                        @if ($contract->scanned_copy)
                            <a href="{{ route('contracts.amo-crm.download', ['contract' => $contract->id]) }}">Скачать</a>
                            &nbsp;/&nbsp;
                            @php
                                $extension = File::extension(storage_path('app/public/' . $contract->scanned_copy));
                            @endphp
                            @if ($extension == 'pdf')
                                <a data-fancybox data-type="iframe" data-src="/web/viewer.html?file=/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                            @else
                                <a data-fancybox data-type="iframe" data-src="/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @foreach($contract->files as $file)
                    <tr>
                        <td><strong>{{ $file->name }}</strong></td>
                        <td><a href="{{ route('contracts.amo-crm.download-file', ['contract' => $contract->id, 'file' => $file]) }}">{{ __('Download') }}</a></td>
                    </tr>
                @endforeach
                <tr>
                    <td><strong>Дата начала</strong></td>
                    <td>{{ $contract->start_date }}</td>
                </tr>
                <tr>
                    <td><strong>Срок окончания</strong></td>
                    <td>{{ $contract->finish_date }}</td>
                </tr>
                <tr>
                    <td><strong>Стоимость CRM, руб</strong></td>
                    <td>{{ $contract->getFormattedCost() }}</td>
                </tr>
                <tr>
                    <td><strong>Стоимость продления CRM, руб</strong></td>
                    <td>{{ $contract->getFormattedRenewalCost() }}</td>
                </tr>
                <tr>
                    <td><strong>Редакция CRM</strong></td>
                    <td>{{ $contract->amoCRMEdition ? $contract->amoCRMEdition->name : '' }}</td>
                </tr>
                <tr>
                    <td><strong>Сайт</strong></td>
                    <td><a href="{{ $contract->site }}" target="_blank">{{ $contract->site }}</a></td>
                </tr>
                <tr>
                    <td><strong>Количество пользователей</strong></td>
                    <td>{{ $contract->user_count }}</td>
                </tr>
                <tr>
                    <td><strong>Логин админа</strong></td>
                    <td>{{ $contract->admin_login }}</td>
                </tr>
                <tr>
                    <td><strong>Пароль админа</strong></td>
                    <td>{{ $contract->admin_password }}</td>
                </tr>
                <tr>
                    <td><strong>Продажник</strong></td>
                    <td>
                        @if ($contract->salesman)
                            {{ $contract->salesman->name }}
                            @if ($contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            </table>
            @if (!$contract->trashed())
                @permission('update-amo-crm-contracts')
                    <a class="btn btn-primary" href="{{ route('contracts.amo-crm.edit', ['id' => $contract->id]) }}">Редактировать</a>
                @endpermission
            @endif
        </div>
    </div>
    @if ($contract->isDeclined())
        @include('partial.confirm-cancellation')
    @endif
@endsection