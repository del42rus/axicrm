@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Create Permission') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <form name="PermissionCreate" method="post" action="{{ route('permissions.store') }}" novalidate>
                {{ csrf_field() }}
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="permission_type" value="basic" checked> {{ __('crud.basic') }}
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="permission_type" value="crud"> {{ __('CRUD') }}
                    </label>
                </div>
                <div id="basic">
                    <div class="form-group">
                        <label>{{ __('Permission Name') }}</label>
                        <input type="text" name="display_name" class="form-control @if ($errors->has('display_name')) is-invalid @endif" value="{{ old('display_name') }}">
                        @if ($errors->has('display_name'))
                            <div class="invalid-feedback">{{ $errors->first('display_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ __('Slug') }}</label>
                        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ __('Description') }}</label>
                        <input type="text" name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" value="{{ old('description') }}">
                        @if ($errors->has('description'))
                            <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ __('Permission Group') }}</label>
                        <input type="text" name="group" class="form-control @if ($errors->has('group')) is-invalid @endif" value="{{ old('group') }}">
                        @if ($errors->has('group'))
                            <div class="invalid-feedback">{{ $errors->first('group') }}</div>
                        @endif
                    </div>
                </div>

                <div id="crud" class="d-none">
                    <div class="form-group">
                        <label>{{ __('Resource Name') }}</label>
                        <input type="text" name="resource" class="form-control @if ($errors->has('resource')) is-invalid @endif" value="{{ old('resource') }}">
                        @if ($errors->has('resource'))
                            <div class="invalid-feedback">{{ $errors->first('resource') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{ __('Permission Group') }}</label>
                        <input type="text" name="group_crud" class="form-control @if ($errors->has('group_crud')) is-invalid @endif" value="{{ old('group_crud') }}">
                        @if ($errors->has('group_crud'))
                            <div class="invalid-feedback">{{ $errors->first('group_crud') }}</div>
                        @endif
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="operation[]" value="create">
                            {{ __('crud.create') }}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="operation[]" value="read">
                            {{ __('crud.read') }}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="operation[]" value="update">
                            {{ __('crud.update') }}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="operation[]" value="delete">
                            {{ __('crud.delete') }}
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
        </div>
    </div>
@endsection
