@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Edit Permission') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <form name="PermissionCreate" method="post" action="{{ route('permissions.update', $permission->id) }}" novalidate>
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label>{{ __('Permission Name') }}</label>
                    <input type="text" name="display_name" class="form-control @if ($errors->has('display_name')) is-invalid @endif" value="{{ old('display_name') ?: $permission->display_name }}">
                    @if ($errors->has('display_name'))
                        <div class="invalid-feedback">{{ $errors->first('display_name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('Slug') }}</label>
                    <input type="text" name="name" class="form-control" value="{{ $permission->name }}" disabled>
                </div>
                <div class="form-group">
                    <label>{{ __('Description') }}</label>
                    <input type="text" name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" value="{{ old('description') ?: $permission->description }}">
                    @if ($errors->has('description'))
                        <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('Permission Group') }}</label>
                    <input type="text" name="group" class="form-control @if ($errors->has('group')) is-invalid @endif" value="{{ old('group') ?: $permission->group }}">
                    @if ($errors->has('group'))
                        <div class="invalid-feedback">{{ $errors->first('group') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
        </div>
    </div>
@endsection
