@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Permissions') }}</h3>
    <p>
        <a class="btn btn-secondary" href="{{ route('permissions.create') }}">{{ __('Create Permission') }}</a>
    </p>

    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
        @foreach(array_keys($groupedPermissions) as $key => $group)
            <li class="nav-item">
                <a class="nav-link @if (!$key) active @endif" data-toggle="tab" href="#tab{{ $key }}" role="tab" aria-selected="true">{{ $group }}</a>
            </li>
        @endforeach
    </ul>

    <div class="tab-content">
        @php($key = 0)
        @foreach($groupedPermissions as $group => $permissions)
            <div class="tab-pane fade @if (!$key) show active @endif" id="tab{{$key}}" role="tabpanel">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>{{ __('Permission Name') }}</th>
                        <th>{{ __('Slug') }}</th>
                        <th>{{ __('Description') }}</th>
                        <th>{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->display_name }}</td>
                            <td>{{ $permission->name }}</td>
                            <td>{{ $permission->description }}</td>
                            <td class="text-right">
                                <div class="btn-group btn-group-sm" role="group">
                                    <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('permissions.edit', ['id' => $permission->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">{{  __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            @php(++$key)
        @endforeach
    </div>
@endsection
