@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Services') }}</h3>

    @permission('create-dictionaries')
        <p>
            <a class="btn btn-secondary" href="{{ route('hosting-services.create') }}">{{ __('Create Service') }}</a>
        </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Service Name') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($services as $service)
            <tr>
                <td>{{ $service->id }}</td>
                <td>{{ $service->name }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                            <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('hosting-services.edit', ['id' => $service->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                            <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('hosting-services.destroy', ['id' => $service->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-force-delete')
@endsection