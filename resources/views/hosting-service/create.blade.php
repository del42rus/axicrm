@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('hosting-services.index') }}">{{ __('Services') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Service') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('hosting-service.partial.form')
        </div>
    </div>
@endsection
