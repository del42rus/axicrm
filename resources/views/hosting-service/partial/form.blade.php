<form method="post" action="@if (isset($service)){{ route('hosting-services.update', $service->id) }}@else{{ route('hosting-services.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($service))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $service->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Service Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($service) ? $service->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>