@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Edit Profile') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <form method="post" action="@if (isset($user)){{ route('profile.update', $user->id) }}@else{{ route('users.store') }}@endif" novalidate>
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label>{{ __('Name') }}</label>
                    <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: $user->name }}">
                    @if ($errors->has('name'))
                        <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" value="{{ old('email') ?: $user->email }}">
                    @if ($errors->has('email'))
                        <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('Password') }}</label>
                    <input type="password" name="password" class="form-control @if ($errors->has('password')) is-invalid @endif">
                    @if ($errors->has('password'))
                        <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('Confirm Password') }}</label>
                    <input type="password" name="password_confirmation" class="form-control @if ($errors->has('password')) is-invalid @endif">
                </div>
                <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
        </div>
    </div>
@endsection