@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.bitrix.index') }}">CMS:1С-Битрикс</a></li>
        <li class="breadcrumb-item">{{ $contract->contract->client->getFullName() }}</li>
    </ol>
    <h3 class="page-header">{{ __('Edit Contract') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('bitrix-contract.partial.form', ['contract' => $contract])
        </div>
    </div>
@endsection
