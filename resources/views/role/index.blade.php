@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Roles') }}</h3>
    <p>
        <a class="btn btn-secondary" href="{{ route('roles.create') }}">{{ __('Create Role') }}</a>
    </p>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Role Name') }}</th>
            <th>{{ __('Slug') }}</th>
            <th>{{ __('Description') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($roles as $role)
            <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->display_name }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->description }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('roles.edit', ['id' => $role->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">{{ __('Nothing found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
