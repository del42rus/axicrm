<form method="post" action="@if (isset($role)){{ route('roles.update', $role->id) }}@else{{ route('roles.store') }}@endif" novalidate>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            {{ csrf_field() }}
            @if (isset($role))
                {{ method_field('PUT') }}
                <input type="hidden" name="id" value="{{ $role->id }}">
            @endif
            <div class="form-group">
                <label>{{ __('Role Name') }}</label>
                <input type="text" name="display_name" class="form-control @if ($errors->has('display_name')) is-invalid @endif" value="{{ old('display_name') ?: (isset($role) ? $role->display_name : '') }}">
                @if ($errors->has('display_name'))
                    <div class="invalid-feedback">{{ $errors->first('display_name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>{{ __('Slug') }}</label>
                <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($role) ? $role->name : '') }}" @if (isset($role)) disabled @endif>

                @if ($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>{{ __('Description') }}</label>
                <input type="text" name="description" class="form-control @if ($errors->has('description')) is-invalid @endif" value="{{ old('description') ?: (isset($role) ? $role->description : '') }}">
                @if ($errors->has('description'))
                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="h4 mb-2 mt-4">{{ __('Permissions') }}</div>
    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
        @foreach(array_keys($groupedPermissions) as $key => $group)
            <li class="nav-item">
                <a class="nav-link @if (!$key) active @endif" data-toggle="tab" href="#tab{{ $key }}" role="tab" aria-selected="true">{{ $group }}</a>
            </li>
        @endforeach
    </ul>

    <div class="tab-content mb-4">
        @php($key = 0)
        @foreach($groupedPermissions as $group => $permissions)
            <div class="tab-pane fade @if (!$key) show active @endif" id="tab{{$key}}" role="tabpanel">
                @foreach ($permissions as $permission)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="permissions[]" value="{{ $permission->id }}" @if (old('permissions') && in_array($permission->id, old('permissions'))) checked @elseif (isset($role) && in_array($permission->id, $role->permissions->pluck('id')->toArray())) checked @endif>
                            {{ $permission->display_name }}
                        </label>
                    </div>
                @endforeach
            </div>
            @php(++$key)
        @endforeach
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>