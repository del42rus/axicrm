@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Edit Role') }}</h3>
    @include('role.partial.form', ['role' => $role])
@endsection
