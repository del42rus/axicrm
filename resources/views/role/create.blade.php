@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Create Role') }}</h3>
    @include('role.partial.form')
@endsection
