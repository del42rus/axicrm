@extends('layouts.app')

@section('content')
    <h3 class="page-header">Редакции CRM Bitrix24</h3>

    @permission('create-dictionaries')
    <p>
        <a class="btn btn-secondary" href="{{ route('bitrix-crm-editions.create') }}">Создать редакцию</a>
    </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Название редакции</th>
            <th>Тип редакции</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($editions as $edition)
            <tr>
                <td>{{ $edition->id }}</td>
                <td>{{ $edition->name }}</td>
                <td>{{ $edition->type == 'cloud' ? 'Облако' : 'Коробка' }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                        <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('bitrix-crm-editions.edit', ['id' => $edition->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                        <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('bitrix-crm-editions.destroy', ['id' => $edition->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-force-delete')
@endsection