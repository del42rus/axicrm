<form method="post" action="@if (isset($edition)){{ route('bitrix-crm-editions.update', $edition->id) }}@else{{ route('bitrix-crm-editions.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($edition))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $edition->id }}">
    @endif
    <div class="form-group">
        <label>Название редакции</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($edition) ? $edition->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Тип редакции</label>
        <select name="type" class="@if ($errors->has('edition')) is-invalid @endif">
            <option value="cloud" @if (old('type') == 'cloud') selected @elseif (isset($edition) && $edition->type == 'cloud') selected @endif>Облако</option>
            <option value="box" @if (old('type') == 'box') selected @elseif (isset($edition) && $edition->type == 'box') selected @endif>Коробка</option>
        </select>
        @if ($errors->has('type'))
            <div class="invalid-feedback">{{ $errors->first('type') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>