@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('bitrix-crm-editions.index') }}">Редакции CRM Bitrix24</a></li>
    </ol>
    <h3 class="page-header">Редактировать редакцию</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('bitrix-crm-edition.partial.form', ['edition' => $edition])
        </div>
    </div>
@endsection
