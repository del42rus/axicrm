@php
    use Illuminate\Support\Facades\File;
@endphp

@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.seo.index') }}">{{ __('SEO') }}</a></li>
    </ol>
    <h3 class="page-header">Информация по договору</h3>
    <div class="row">
        <div class="col-lg-6 col-md-9">
            <table class="table">
                <tr>
                    <td><strong>{{ __('Contract Number') }}</strong></td>
                    <td>{{ $contract->contract->contract_number }}</td>
                </tr>
                <tr>
                    <td><strong>Статус</strong></td>
                    <td>
                        @if ($contract->isActive())
                            Текущий
                        @else
                            Отказ
                            @permission('decline-seo-contracts')
                                <a class="btn btn-warning text-white btn-sm" title="{{ __('Decline') }}" data-href="{{ route('contracts.seo.decline', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-cancellation"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            <br />
                            @if ($contract->cancellation_letter)
                                <a href="{{ route('contracts.seo.download-cancellation-letter', ['contract' => $contract]) }}">Письмо о расторжении договора</a><br />
                            @endif
                            @if ($contract->cancellation_agreement)
                                <a href="{{ route('contracts.seo.download-cancellation-agreement', ['contract' => $contract]) }}">Дополнительное соглашение о расторжении договора</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @if ($contract->isDeclined())
                    <td><strong>Дата отказа</strong></td>
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <tr>
                    <td><strong>{{ __('Client') }}</strong></td>
                    <td><a href="{{ route('clients.show', ['id' => $contract->contract->client->id]) }}">{{ $contract->contract->client->getFullName() }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Scanned Copy') }}</strong></td>
                    <td>
                        @if ($contract->scanned_copy)
                            <a href="{{ route('contracts.seo.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
                            &nbsp;/&nbsp;
                            @php
                                $extension = File::extension(storage_path('app/public/' . $contract->scanned_copy));
                            @endphp
                            @if ($extension == 'pdf')
                                <a data-fancybox data-type="iframe" data-src="/web/viewer.html?file=/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                            @else
                                <a data-fancybox data-type="iframe" data-src="/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @foreach($contract->files as $file)
                    <tr>
                        <td><strong>{{ $file->name }}</strong></td>
                        <td><a href="{{ route('contracts.seo.download-file', ['contract' => $contract->id, 'file' => $file]) }}">{{ __('Download') }}</a></td>
                    </tr>
                @endforeach
                <tr>
                    <td><strong>{{ __('Start Date') }}</strong></td>
                    <td>{{ $contract->start_date }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Budget') }}</strong></td>
                    <td>{{ $contract->getFormattedBudget() }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Payment Type') }}</strong></td>
                    <td>@if ($contract->payment_type == 1) {{ __('Fixed') }} @else {{ __('By Position') }} @endif</td>
                </tr>
                <tr>
                    <td><strong>Способ оплаты</strong></td>
                    <td>{{ $contract->paymentMethod ? $contract->paymentMethod->name : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Metrika') }}</strong></td>
                    <td><a href="{{ $contract->metrika_link }}">{{ $contract->metrika_link }}</a></td>
                </tr>
                <tr>
                    <th>{{ __('AllPositions') }}</th>
                    <td><a href="{{ $contract->allpositions_link }}" target="_blank">{{ $contract->allpositions_link }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Site') }}</strong></td>
                    <td><a href="{{ $contract->site }}" target="_blank">{{ $contract->site }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Marketer') }}</strong></td>
                    <td>
                        @if ($contract->marketer)
                            {{ $contract->marketer->name }}
                            @if ($contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ __('Salesman') }}</strong></td>
                    <td>
                        @if ($contract->salesman)
                            {{ $contract->salesman->name }}
                            @if ($contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ __('Responsible Person') }}</strong></td>
                    <td>
                        @if ($contract->responsible)
                            {{ $contract->responsible->name }}
                            @if ($contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            </table>
            @if (!$contract->trashed())
                @permission('update-smm-contracts')
                    <a class="btn btn-primary" href="{{ route('contracts.seo.edit', ['id' => $contract->id]) }}">{{ __('Edit') }}</a>
                @endpermission
                <a class="btn btn-warning" href="{{ route('contracts.correct', ['contract' => $contract->contract->id]) }}">Корректировать</a>
            @endif
        </div>
    </div>
    @if ($contract->isDeclined())
        @include('partial.confirm-cancellation')
    @endif
@endsection