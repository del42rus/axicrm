@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('activities.index') }}">{{ __('Activities') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Edit Activity') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('activity.partial.form', ['activity' => $activity])
        </div>
    </div>
@endsection
