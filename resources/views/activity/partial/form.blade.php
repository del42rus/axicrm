<form method="post" action="@if (isset($activity)){{ route('activities.update', $activity->id) }}@else{{ route('activities.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($activity))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $activity->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Activity Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($activity) ? $activity->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>