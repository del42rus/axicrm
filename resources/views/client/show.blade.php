@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('clients.index') }}">{{ __('Clients') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Client') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            <table class="table">
                <tr>
                    <td><strong>{{ __('Legal Name') }}</strong></td>
                    <td>{{ $client->legal_name }}, {{ $client->ownershipType->name }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Brand') }}</strong></td>
                    <td>{{ $client->brand }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Kind of Activity') }}</strong></td>
                    <td>{{ $client->activity ? $client->activity->name : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Site') }}</strong></td>
                    <td><a href="{{ $client->site }}" target="_blank">{{ $client->site }}</a></td>
                </tr>
                <tr>
                    <td><strong>{{ __('Actual Address') }}</strong></td>
                    <td>{{ $client->actualAddress ? $client->actualAddress->city->name . ', ' . $client->actualAddress->full_address : '' }}</td>
                </tr>
                <tr>
                    <td><strong>{{ __('Legal Address') }}</strong></td>
                    <td>{{ $client->legalAddress ? $client->legalAddress->city->name . ', ' . $client->legalAddress->full_address : '' }}</td>
                </tr>
            </table>
        </div>
    </div>
    @if ($client->caContracts()->count())
        <h3 class="page-header">Договоры Контекстная реклама</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
                <th>{{ __('Responsible Person') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->caContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.ca.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->responsible)
                            {{ $contract->contract->responsible->name }}
                            @if ($contract->contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->seoContracts()->count())
        <h3 class="page-header">Договоры SEO</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
                <th>{{ __('Responsible Person') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->seoContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.seo.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->responsible)
                            {{ $contract->contract->responsible->name }}
                            @if ($contract->contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->smmContracts()->count())
        <h3 class="page-header">Договоры SMM</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
                <th>{{ __('Responsible Person') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->smmContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.smm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->responsible)
                            {{ $contract->contract->responsible->name }}
                            @if ($contract->contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->appContracts()->count())
        <h3 class="page-header">Договоры Сайты и приложения</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->appContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.app.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->admContracts()->count())
        <h3 class="page-header">Договоры Администрирование</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
                <th>{{ __('Responsible Person') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->admContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.adm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->responsible)
                            {{ $contract->contract->responsible->name }}
                            @if ($contract->contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->ciContracts()->count())
    <h3 class="page-header">Договоры Фирменный стиль</h3>
    <table class="table">
        <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($client->ciContracts() as $contract)
            <tr>
                <td><a target="_blank" href="{{ route('contracts.ci.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                <td>
                    @if ($contract->contract->marketer)
                        {{ $contract->contract->marketer->name }}
                        @if ($contract->contract->marketer->internal_phone_number)
                            {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                        @endif
                    @endif
                </td>
                <td>
                    @if ($contract->contract->salesman)
                        {{ $contract->contract->salesman->name }}
                        @if ($contract->contract->salesman->internal_phone_number)
                            {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif
    @if ($client->hostingContracts()->count())
        <h3 class="page-header">Договоры Хостинг/Домен</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Responsible Person') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->hostingContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.hosting.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->responsible)
                            {{ $contract->contract->responsible->name }}
                            @if ($contract->contract->responsible->internal_phone_number)
                                {{ ' (' . $contract->contract->responsible->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->additionalContracts()->count())
        <h3 class="page-header">Доп. работы ПО</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->additionalContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.additional.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->additionalDigitalContracts()->count())
        <h3 class="page-header">Доп. работы ОАиП</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->additionalDigitalContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.additional-digital.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->bitrixContracts()->count())
        <h3 class="page-header">CMS:1С-Битрикс</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->bitrixContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.bitrix.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->bitrixCrmContracts()->count())
        <h3 class="page-header">CRM:Битрикс-24</h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Marketer') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->bitrixCrmContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.bitrix-crm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->marketer)
                            {{ $contract->contract->marketer->name }}
                            @if ($contract->contract->marketer->internal_phone_number)
                                {{ ' (' . $contract->contract->marketer->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->amoCrmContracts()->count())
        <h3 class="page-header">CRM:Amo.CRM/h3>
        <table class="table">
            <thead>
            <tr>
                <th>{{ __('Contract Number') }}</th>
                <th>{{ __('Salesman') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($client->amoCrmContracts() as $contract)
                <tr>
                    <td><a target="_blank" href="{{ route('contracts.amo-crm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}</a></td>
                    <td>
                        @if ($contract->contract->salesman)
                            {{ $contract->contract->salesman->name }}
                            @if ($contract->contract->salesman->internal_phone_number)
                                {{ ' (' . $contract->contract->salesman->internal_phone_number . ')' }}
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    @if ($client->contacts()->count())
        <h3 class="page-header">Контакты</h3>

        <table class="table table-striped table-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Full Name') }}</th>
                <th>{{ __('E-mail') }}</th>
                <th>{{ __('Phone') }}</th>
                <th>{{ __('Position') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($client->contacts as $contact)
                <tr>
                    <td>{{ $contact->id }}</td>
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->phone }}</td>
                    <td>{{ $contact->position ? $contact->position->name : '' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection