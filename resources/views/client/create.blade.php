@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('clients.index') }}">{{ __('Clients') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Client') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('client.partial.form')
        </div>
    </div>
@endsection
