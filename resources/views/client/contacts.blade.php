@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('clients.index') }}">{{ __('Clients') }}</a></li>
        <li class="breadcrumb-item">{{ $client->getFullName() }}</li>
    </ol>
    <h3 class="page-header">{{ __('Contacts') }}</h3>
    <p>
        <a class="btn btn-secondary" href="{{ route('contacts.create', ['client_id' => $client->id]) }}">{{ __('Create Contact') }}</a>
    </p>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Full Name') }}</th>
            <th>{{ __('E-mail') }}</th>
            <th>{{ __('Phone') }}</th>
            <th>{{ __('Position') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contacts as $contact)
            <tr>
                <td>{{ $contact->id }}</td>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->phone }}</td>
                <td>{{ $contact->position ? $contact->position->name : '' }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-contacts')
                            <a class="btn btn-secondary" title="Edit" href="{{ route('contacts.edit', ['id' => $contact->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-contacts')
                            <button class="btn btn-danger" title="Delete" data-href="{{ route('contacts.destroy', ['id' => $contact->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">{{ __('Nothing found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-delete')
@endsection
