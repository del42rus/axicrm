<form method="post" action="@if (isset($client)){{ route('clients.update', $client->id) }}@else{{ route('clients.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($client))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $client->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Type of Property Ownership') }}</label>
        <select class="@if ($errors->has('ownership_type_id')) is-invalid @endif" name="ownership_type_id">
            @foreach (App\OwnershipType::all() as $ownershipType)
                <option value="{{ $ownershipType->id }}" @if (old('ownership_type_id') == $ownershipType->id) selected="selected" @elseif (isset($client) && $client->ownershipType->id == $ownershipType->id) selected="selected" @endif>{{ $ownershipType->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('ownership_type_id'))
            <div class="invalid-feedback">{{ $errors->first('ownership_type_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Legal Name') }}</label>
        <input type="text" name="legal_name" class="form-control @if ($errors->has('legal_name')) is-invalid @endif" value="{{ old('legal_name') ?: (isset($client) ? $client->legal_name : '')}}">
        @if ($errors->has('legal_name'))
            <div class="invalid-feedback">{{ $errors->first('legal_name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Brand') }}</label>
        <input type="text" name="brand" class="form-control" value="{{ old('brand') ?: (isset($client) ? $client->brand : '')}}">
    </div>
    <div class="form-group">
        <label>{{ __('Kind of Activity') }}</label>
        <select class="@if ($errors->has('activity_id')) is-invalid @endif" name="activity_id">
            <option value="">{{ __('None') }}</option>
            @foreach (App\Activity::all() as $activity)
                <option value="{{ $activity->id }}" @if (old('activity_id') == $activity->id) selected="selected" @elseif (isset($client) && $client->activity && $client->activity->id == $activity->id) selected="selected" @endif>{{ $activity->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('activity_id'))
            <div class="invalid-feedback">{{ $errors->first('activity_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Site') }}</label>
        <input type="text" name="site" class="form-control" value="{{ old('site') ?: (isset($client) ? $client->site : '')}}">
    </div>
    <div>
        <label>{{ __('Actual Address') }}</label>
        <div class="form-group">
            <label>{{ __('City') }}</label>
            <select class="@if ($errors->has('actual_address.city_id')) is-invalid @endif" name="actual_address[city_id]">
                @foreach (App\City::all() as $city)
                    <option value="{{ $city->id }}" @if (old('actual_address.city_id') == $city->id) selected="selected" @elseif (isset($client) && $client->actualAddress && $client->actualAddress->city->id == $city->id) selected="selected" @endif>{{ $city->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('actual_address.city_id'))
                <div class="invalid-feedback">{{ $errors->first('actual_address.city_id') }}</div>
            @endif
        </div>
        <div class="form-group">
            <label>{{ __('Full Address') }}</label>
            <input type="text" name="actual_address[full_address]" class="@if ($errors->has('actual_address.full_address')) is-invalid @endif form-control" value="{{ old('actual_address.full_address') ?: (isset($client) && $client->actualAddress ? $client->actualAddress->full_address : '')}}">
            @if ($errors->has('actual_address.full_address'))
                <div class="invalid-feedback">{{ $errors->first('actual_address.full_address') }}</div>
            @endif
        </div>
    </div>
    <div>
        <label>{{ __('Legal Address') }}</label>
        <div class="form-group">
            <label>{{ __('City') }}</label>
            <select class="@if ($errors->has('legal_address.city_id')) is-invalid @endif" name="legal_address[city_id]">
                @foreach (App\City::all() as $city)
                    <option value="{{ $city->id }}" @if (old('legal_address.city_id') == $city->id) selected="selected" @elseif (isset($client) && $client->legalAddress && $client->legalAddress->city->id == $city->id) selected="selected" @endif>{{ $city->name }}</option>
                @endforeach
            </select>
            @if ($errors->has('legal_address.city_id'))
                <div class="invalid-feedback">{{ $errors->first('legal_address.city_id') }}</div>
            @endif
        </div>
        <div class="form-group">
            <label>{{ __('Full Address') }}</label>
            <input type="text" name="legal_address[full_address]" class="@if ($errors->has('legal_address.full_address')) is-invalid @endif form-control" value="{{ old('legal_address.full_address') ?: (isset($client) && $client->legalAddress ? $client->legalAddress->full_address : '')}}">
            @if ($errors->has('legal_address.full_address'))
                <div class="invalid-feedback">{{ $errors->first('legal_address.full_address') }}</div>
            @endif
        </div>
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>