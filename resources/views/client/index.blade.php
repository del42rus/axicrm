@php

use App\User;

@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Clients') }}</h3>
    <p>
        @permission('create-clients')
            <a class="btn btn-secondary" href="{{ route('clients.create') }}">{{ __('Create Client') }}</a>
        @endpermission

        @permission('export-clients')
            <a class="btn btn-primary" href="{{ route('clients.download') }}"><i class="fa fa-file-excel-o"></i>&nbsp;{{ __('To Excel') }}</a>
        @endpermission
    </p>
    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <select name="legal_name">
                    <option value="">{{ __('Client') }}</option>
                    @foreach (App\Client::all() as $client)
                        <option value="{{ $client->legal_name }}" @if (request('legal_name') == $client->legal_name) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (App\Client::all() as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="site" placeholder="{{ __('Site') }}" value="{{ request('site') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="activity_id">
                    <option value="">Вид деятельности</option>
                    @foreach (\App\Activity::all() as $activity)
                        <option value="{{ $activity->id }}" @if (request('activity_id') == $activity->id) selected @endif>{{ $activity->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">{{ __('Marketer') }}</option>
                    @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                    @foreach (User::whereRoleIs('marketer')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="salesman_id">
                    <option value="">{{ __('Salesman') }}</option>
                    @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                        <option value="{{ $salesman->id }}" @if (request('salesman_id') == $salesman->id) selected @endif>{{ $salesman->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="contract_status">
                    <option value="">Статус договора</option>
                    <option value="1" @if (request('contract_status') == '1') selected @endif>Текущий</option>
                    <option value="2" @if (request('contract_status') == '2') selected @endif>Выполнен</option>
                    <option value="0" @if (request('contract_status') === '0') selected @endif>Отказ</option>
                </select>
            </div>
            <div class="form-group col-lg-2">
                <div class="form-check mt-2">
                    <input class="form-check-input ml-0" type="checkbox" id="checkRemoved" name="removed" value="1" @if (request('removed') == '1') checked @endif>
                    <label class="form-check-label" for="checkRemoved">Архив</label>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
            </div>
        </div>
    </form>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th><a href="{{ route('clients.index', ['sort' => (request('sort') == '-client' || !request('sort')) ? 'client' : '-client']) }}">
                    {{ __('Legal Name') }}
                    @if (in_array(request('sort'), ['client', '-client']))
                        <i class="fa fa-sort-alpha-{{ request('sort') == '-client' ? 'desc' : 'asc' }}"></i>
                    @endif
                </a>
            </th>
            <th>{{ __('Brand') }}</th>
            <th>{{ __('Kind of Activity') }}</th>
            <th>{{ __('Site') }}</th>
            <th>{{ __('Contracts') }}</th>
            @permission('read-clients')
                <th>{{ __('Contacts') }}</th>
            @endpermission
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($clients as $client)
                <tr>
                    <td>{{ $client->id }}</td>
                    <td><a href="{{ route('clients.show', ['id' => $client->id]) }}">{{ $client->getFullName() }}</a></td>
                    <td>{{ $client->brand }}</td>
                    <td>{{ $client->activity ? $client->activity->name : '' }}</td>
                    <td><a href="{{ $client->site }}" target="_blank">{{ $client->site }}</a></td>
                    <td>
                        @if ($client->caContracts()->count())
                            Контекстная реклама:&nbsp;
                            @foreach ($client->caContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.ca.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->seoContracts()->count())
                            SEO:&nbsp;
                            @foreach ($client->seoContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.seo.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->smmContracts()->count())
                            SMM:&nbsp;
                            @foreach ($client->smmContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.smm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->appContracts()->count())
                            Сайты и Приложения:&nbsp;
                            @foreach ($client->appContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.app.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isCompleted())
                                    <span>(Выполнен)</span>
                                @endif
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->admContracts()->count())
                            Администрирование:&nbsp;
                            @foreach ($client->admContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.adm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->ciContracts()->count())
                            Фирменный стиль:&nbsp;
                            @foreach ($client->ciContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.ci.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isCompleted())
                                    <span>(Выполнен)</span>
                                @endif
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->hostingContracts()->count())
                            Хостинг/Домен:&nbsp;
                            @foreach ($client->hostingContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.hosting.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                @if ($contract->contract->isDeclined())
                                    <span>(Отказались)</span>
                                @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->additionalContracts()->count())
                            Доп. работы ПО:&nbsp;
                            @foreach ($client->additionalContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.additional.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                    @if ($contract->contract->isDeclined())
                                        <span>(Отказались)</span>
                                    @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->additionalDigitalContracts()->count())
                            Доп. работы ОАиП:&nbsp;
                            @foreach ($client->additionalDigitalContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.additional-digital.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                    @if ($contract->contract->isDeclined())
                                        <span>(Отказались)</span>
                                    @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->bitrixContracts()->count())
                            CMS:1C-Битрикс:&nbsp;
                            @foreach ($client->bitrixContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.bitrix.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                    @if ($contract->contract->isDeclined())
                                        <span>(Отказались)</span>
                                    @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->bitrixCrmContracts()->count())
                            CRM:Битрикс-24:&nbsp;
                            @foreach ($client->bitrixCrmContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.bitrix-crm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                    @if ($contract->contract->isDeclined())
                                        <span>(Отказались)</span>
                                    @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                        @if ($client->amoCrmContracts()->count())
                            CRM:Amo.CRM:&nbsp;
                            @foreach ($client->amoCrmContracts() as $contract)
                                <a target="_blank" href="{{ route('contracts.amo-crm.show', ['id' => $contract->contract->id]) }}">{{ $contract->contract_number }}
                                    @if ($contract->contract->isDeclined())
                                        <span>(Отказались)</span>
                                    @endif
                                </a>
                                @if (!$loop->last), @else <br /> @endif
                            @endforeach
                        @endif
                    </td>
                    @permission('read-clients')
                        <td><a href="{{ route('clients.contacts', ['id' => $client->id]) }}">{{ __('View Contacts') }}</a></td>
                    @endpermission
                    <td class="text-right">
                        <div class="btn-group btn-group-sm" role="group">
                            @if ($client->trashed())
                                @permission('restore-clients')
                                    <a class="btn btn-success" title="Восстановить" href="{{ route('clients.restore', ['id' => $client->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                                @endpermission
                                @permission('delete-clients')
                                    <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('clients.force-delete', ['id' => $client->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                @endpermission
                            @else
                                @permission('update-clients')
                                    <a class="btn btn-secondary" title="Редактировать" href="{{ route('clients.edit', ['id' => $client->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                @endpermission
                                @permission('soft-delete-clients')
                                    <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('clients.destroy', ['id' => $client->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                @endpermission
                            @endif
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="8">{{ __('Nothing found') }}</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $clients->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
@endsection
