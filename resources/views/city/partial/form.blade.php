<form method="post" action="@if (isset($city)){{ route('cities.update', $city->id) }}@else{{ route('cities.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($city))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $city->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Region') }}</label>
        <select class="@if ($errors->has('region_id')) is-invalid @endif" name="region_id">
            @foreach (App\Region::all() as $region)
                <option value="{{ $region->id }}" @if (old('region_id') == $region->id) selected="selected" @elseif (isset($city) && $city->region->id == $region->id) selected="selected" @endif>{{ $region->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('region_id'))
            <div class="invalid-feedback">{{ $errors->first('region_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('City') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($city) ? $city->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>