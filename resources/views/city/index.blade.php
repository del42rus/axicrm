@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Cities') }}</h3>

    @permission('create-dictionaries')
    <p>
        <a class="btn btn-secondary" href="{{ route('cities.create') }}">{{ __('Create City') }}</a>
    </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('City') }}</th>
            <th>{{ __('Region') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($cities as $city)
            <tr>
                <td>{{ $city->id }}</td>
                <td>{{ $city->name }}</td>
                <td>{{ $city->region->name }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                            <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('cities.edit', ['id' => $city->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                            <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('cities.destroy', ['id' => $city->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-delete')
@endsection