@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('cities.index') }}">{{ __('Cities') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create City') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('city.partial.form')
        </div>
    </div>
@endsection
