@php

use App\User;
use App\Client;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Sites & Apps') }}</h3>

    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <input class="form-control" name="contract_number" placeholder="{{ __('Contract Number') }}" value="{{ request('contract_number') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">{{ __('Client') }}</option>
                    @foreach (Client::hasContractType('app') as $client)
                        <option value="{{ $client->id }}" @if (request('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (Client::hasContractType('app') as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="stage_id">
                    <option value="">{{ __('Stage') }}</option>
                    @foreach (App\AppContractStage::all() as $stage)
                        <option value="{{ $stage->id }}" @if (request('stage_id') == $stage->id) selected @endif>{{ $stage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">{{ __('Marketer') }}</option>
                    @foreach (User::whereRoleIs('marketer')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="salesman_id">
                    <option value="">{{ __('Salesman') }}</option>
                    @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                        <option value="{{ $salesman->id }}" @if (request('salesman_id') == $salesman->id) selected @endif>{{ $salesman->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
    </form>

    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link @if ($status == 'active') active @endif" href="{{ route('contracts.app.index', ['status' => 'active']) }}">{{ __('Active') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'declined') active @endif" href="{{ route('contracts.app.index', ['status' => 'declined']) }}">{{ __('Declined') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'completed') active @endif" href="{{ route('contracts.app.index', ['status' => 'completed']) }}">{{ __('Completed') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'deleted') active @endif" href="{{ route('contracts.app.index', ['status' => 'deleted']) }}">Архив</a>
        </li>
    </ul>

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Start Date') }}</th>
            <th>{{ __('Date of Delivery') }}</th>
            @if ($status == 'completed' || $status == 'deleted')
                <th>
                    @php
                        $sortParam = ['sort' => ((request('sort') == '-actual_finish_date' || !request('sort')) ? 'actual_finish_date' : '-actual_finish_date')];

                        $query = http_build_query(
                            array_merge(request()->query(), $sortParam)
                        );
                    @endphp
                    <a href="/{{ request()->path() . '?' . $query }}">
                        {{ __('Actual Date of Delivery') }}
                        @if (in_array(request('sort'), ['actual_finish_date', '-actual_finish_date']))
                            <i class="fa fa-sort-alpha-{{ request('sort') == '-actual_finish_date' ? 'desc' : 'asc' }}"></i>
                        @endif
                    </a>
                </th>
            @endif
            @if ($status == 'declined')
                <th>Дата отказа</th>
            @endif
            <th>{{ __('Legal Name') }}</th>
            <th>{{ __('Brand') }}</th>
            <th>{{ __('Site') }}</th>
            <th>{{ __('Scanned Copy') }}</th>
            <th>{{ __('Stage') }}</th>
            <th>{{ __('Budget') }}, {{ __('RUB') }}</th>
            <th>Способ оплаты</th>
            <th>{{ __('Marketer') }}</th>
            <th>{{ __('Salesman') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contracts as $contract)
            <tr @if ($contract->isRecent()) class="table-success" @endif>
                <td><a href="{{ route('contracts.app.show', ['contract' => $contract]) }}">{{ $contract->contract->contract_number }}</a></td>
                <td>{{ $contract->start_date }}</td>
                <td>{{ $contract->finish_date }}</td>
                @if ($contract->isCompleted() || $contract->trashed())
                    <td>{{ $contract->actual_finish_date }}</td>
                @endif
                @if ($contract->isDeclined())
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <td><a href="{{ route('clients.show', ['id' => $contract->contract->client->id]) }}">{{ $contract->contract->client->getFullName() }}</a></td>
                <td>{{ $contract->contract->client->brand }}</td>
                <td><a href="{{ $contract->site }}" target="_blank">{{ $contract->site }}</a></td>
                <td>
                    @if ($contract->scanned_copy)
                        <a href="{{ route('contracts.app.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
                        <br/>
                        @php
                            $extension = File::extension(storage_path('app/public/' . $contract->scanned_copy));
                        @endphp
                        @if ($extension == 'pdf')
                            <a data-fancybox data-type="iframe" data-src="/web/viewer.html?file=/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @else
                            <a data-fancybox data-type="iframe" data-src="/storage/{{ $contract->scanned_copy }}" href="javascript:;">Открыть</a>
                        @endif
                    @endif
                </td>
                <td>@if ($contract->stage) {{ $contract->stage->name }} @else &mdash; @endif</td>
                <td>{{ $contract->getFormattedBudget() }}</td>
                <td>{{ $contract->paymentMethod ? $contract->paymentMethod->name : '' }}</td>
                <td>@if ($contract->marketer) {{ $contract->marketer->name }} @endif</td>
                <td>@if ($contract->salesman) {{ $contract->salesman->name }} @endif</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @if ($contract->trashed())
                            @permission('restore-app-contracts')
                                <a class="btn btn-success" title="Восстановить" href="{{ route('contracts.app.restore', ['id' => $contract->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('delete-app-contracts')
                                <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('contracts.app.force-delete', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                        @else
                            @permission('update-app-contracts')
                                <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('contracts.app.edit', ['id' => $contract->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('soft-delete-app-contracts')
                                <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('contracts.app.destroy', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                            @if ($contract->status == 1)
                                @permission('decline-app-contracts')
                                    <a class="btn btn-warning text-white" title="{{ __('Decline') }}" data-href="{{ route('contracts.app.decline', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-cancellation"><i class="fa fa-close" aria-hidden="true"></i></a>
                                @endpermission
                                @permission('complete-app-contracts')
                                    <button class="btn btn-success text-white" title="{{ __('Complete') }}" data-toggle="modal" data-target="#confirm-decline-complete" data-href="{{ route('contracts.app.complete', ['id' => $contract->id]) }}"><i class="fa fa-check" aria-hidden="true"></i></button>
                                @endpermission
                            @elseif ($contract->status == 0)
                                @permission('activate-app-contracts')
                                    <a class="btn btn-success" title="{{ __('Activate') }}" href="{{ route('contracts.app.activate', ['id' => $contract->id]) }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                @endpermission
                            @elseif ($contract->status == 2)
                                @permission('activate-app-contracts')
                                    <a class="btn btn-success" title="{{ __('Activate') }}" href="{{ route('contracts.app.activate', ['id' => $contract->id]) }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                @endpermission
                            @endif
                        @endif
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="14">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $contracts->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
    @include('partial.confirm-cancellation')

    <div class="modal fade" id="confirm-decline-complete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post">
                    {{ csrf_field() }}
                    <input name="backUrl" type="hidden" value="{{ url()->current() }}">
                    <div class="modal-header">
                        {{ __('Confirm Action') }}
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{ __('Actual Date of Delivery') }}</label>
                            <input type="text" name="actual_finish_date" class="datepicker form-control" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                        <button class="btn btn-primary" type="submit">{{ __('OK') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection