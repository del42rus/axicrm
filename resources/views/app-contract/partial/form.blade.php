@php

use App\User;

@endphp

<form method="post" action="@if ($contract instanceof \App\AppContract){{ route('contracts.app.update', $contract->id) }}@else{{ route('contracts.app.store') }}@endif" enctype="multipart/form-data" novalidate>
    {{ csrf_field() }}

    @if ($contract instanceof \App\AppContract)
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @else
        <input type="hidden" name="contract_id" value="{{ $contract->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Contract Number') }}</label>
        <input type="text" name="number" class="form-control" value="{{ $contract instanceof \App\AppContract ? $contract->contract->contract_number : $contract->contract_number }}" readonly disabled>
    </div>
    <div class="form-group">
        <label class="d-block">Скан договора</label>
        <label class="custom-file">
            <input type="file" name="scanned_copy" class="custom-file-input @if ($errors->has('scanned_copy')) is-invalid @endif" required>
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('scanned_copy'))
            <div class="invalid-feedback d-block">{{ $errors->first('scanned_copy') }}</div>
        @endif
        @if ($contract instanceof \App\AppContract && $contract->scanned_copy)
            <a class="btn btn-link" href="{{ route('contracts.app.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
        @endif
    </div>
    @if ($contract instanceof \App\AppContract)
        @foreach ($contract->files as $file)
            <div class="form-group">
                <label>Название файла</label>
                <input name="files[id_{{$file->id}}][name]" type="text" class="form-control @if ($errors->has('files.id_' . $file->id .'.name')) is-invalid @endif" value="{{ old('files.id_' . $file->id . '.name') ? : $file->name }}"/>
                @if ($errors->has('files.id_' . $file->id . '.name'))
                    <div class="invalid-feedback">{{ $errors->first('files.id_' . $file->id . '.name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label class="custom-file">
                    <input type="file" name="files[id_{{$file->id}}][file]" class="custom-file-input @if ($errors->has('files.id_' . $file->id . '.file')) is-invalid @endif">
                    <span class="custom-file-control"></span>
                </label>
                @if ($errors->has('files.id_' . $file->id . '.file'))
                    <div class="invalid-feedback d-block">{{ $errors->first('files.id_' . $file->id . '.file') }}</div>
                @endif
                <a class="btn btn-link" href="{{ route('contracts.app.download-file', ['contract' => $contract->id, 'file' => $file->id]) }}">{{ __('Download') }}</a>
                <a class="btn btn-link" href="{{ route('contracts.app.delete-file', ['contract' => $contract->id, 'file' => $file->id]) }}">Удалить</a>
            </div>
        @endforeach
    @endif
    <div class="form-group">
        <label>Название файла</label>
        <input name="files[0][name]" type="text" class="form-control @if ($errors->has('files.0.name')) is-invalid @endif" value="{{ old('files.0.name') }}"/>
        @if ($errors->has('files.0.name'))
            <div class="invalid-feedback">{{ $errors->first('files.0.name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label class="custom-file">
            <input type="file" name="files[0][file]" class="custom-file-input @if ($errors->has('files.0.file')) is-invalid @endif">
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('files.0.file'))
            <div class="invalid-feedback d-block">{{ $errors->first('files.0.file') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Site') }}</label>
        <input type="text" name="site" class="form-control" value="{{ old('site') ?: ($contract instanceof \App\AppContract ? $contract->site : '')}}">
    </div>
    <div class="form-group">
        <label>{{ __('Start Date') }}</label>
        <input type="text" name="start_date" class="datepicker form-control @if ($errors->has('start_date')) is-invalid @endif" value="{{ old('start_date') ?: ($contract instanceof \App\AppContract ? $contract->start_date : '')}}">
        @if ($errors->has('start_date'))
            <div class="invalid-feedback">{{ $errors->first('start_date') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Date of Delivery') }}</label>
        <input type="text" name="finish_date" class="datepicker form-control @if ($errors->has('finish_date')) is-invalid @endif" value="{{ old('finish_date') ?: ($contract instanceof \App\AppContract ? $contract->finish_date : '')}}">
        @if ($errors->has('finish_date'))
            <div class="invalid-feedback">{{ $errors->first('finish_date') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Budget') }}</label>
        <input type="text" name="budget" class="form-control @if ($errors->has('budget')) is-invalid @endif" value="{{ old('budget') ?: ($contract instanceof \App\AppContract ? $contract->budget : '')}}">
        @if ($errors->has('budget'))
            <div class="invalid-feedback">{{ $errors->first('budget') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Способ оплаты</label>
        <select name="payment_method_id" class="@if ($errors->has('payment_method_id')) is-invalid @endif">
            <option value="">Не указан</option>
            @foreach (\App\PaymentMethod::all() as $paymentMethod)
                <option value="{{ $paymentMethod->id }}" @if (old('payment_method_id') == $paymentMethod->id) selected @elseif ($contract instanceof \App\AppContract && $contract->paymentMethod && $contract->paymentMethod->id == $paymentMethod->id) selected @endif>{{ $paymentMethod->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('payment_method_id'))
            <div class="invalid-feedback">{{ $errors->first('payment_method_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Stage') }}</label>
        <select name="stage_id">
            <option value="">{{ __('None') }}</option>
            @foreach (App\AppContractStage::orderBy('position', 'asc')->get() as $stage)
                <option value="{{ $stage->id }}" @if (old('stage_id') == $stage->id) selected @elseif ($contract instanceof \App\AppContract && $contract->stage && $contract->stage->id == $stage->id) selected @endif>{{ $stage->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Marketer') }}</label>
        <select name="marketer_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('marketer')->get() as $marketer)
                <option value="{{ $marketer->id }}" @if (old('marketer_id') == $marketer->id) selected @elseif ($contract instanceof \App\AppContract && isset($contract->marketer) && $contract->marketer->id == $marketer->id) selected @endif>{{ $marketer->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Salesman') }}</label>
        <select name="salesman_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                <option value="{{ $salesman->id }}" @if (old('salesman_id') == $salesman->id) selected @elseif ($contract instanceof \App\AppContract && isset($contract->salesman) && $contract->salesman->id == $salesman->id) selected @endif>{{ $salesman->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>