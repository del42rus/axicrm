@php

use App\User;
use App\Client;

@endphp

@extends('layouts.app')

@section('content')
    <h3 class="page-header">Реестр договоров</h3>

    <p>
        @permission('create-contracts')
            <a class="btn btn-secondary" href="{{ route('contracts.create') }}">Зарезервировать договор</a>
        @endpermission

        <a class="btn btn-primary" href="{{ route('contracts.download', ['status' => $status]) }}?{{ request()->getQueryString() }}"><i class="fa fa-file-excel-o"></i>&nbsp;Экспорт в Excel</a>
    </p>
    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <input class="form-control" name="contract_number" placeholder="Номер договора" value="{{ request('contract_number') }}">
            </div>
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">Клиент</option>
                    @foreach (Client::all() as $client)
                        <option value="{{ $client->id }}" @if (request('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">Бренд</option>
                    @foreach (Client::all() as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="marketer_id">
                    <option value="">Интернет-маркетолог</option>
                    @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                    @foreach (User::whereRoleIs('marketer')->get() as $marketer)
                        <option value="{{ $marketer->id }}" @if (request('marketer_id') == $marketer->id) selected @endif>{{ $marketer->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="salesman_id">
                    <option value="">Продажник</option>
                    @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                        <option value="{{ $salesman->id }}" @if (request('salesman_id') == $salesman->id) selected @endif>{{ $salesman->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="contract_type">
                    <option value="">Тип договора</option>
                    @foreach (\App\Contract::getTypes() as $key => $name)
                        @if (Auth::user()->hasPermission('read-' . $key . '-contracts'))
                            <option value="{{ $key }}" @if (request('contract_type') == $key) selected @endif>{{ $name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <button type="submit" class="btn btn-primary">Найти</button>
            </div>
        </div>
    </form>

    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link @if ($status == 'active') active @endif" href="{{ route('contracts.index', ['status' => 'active']) }}">Текущие</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'declined') active @endif" href="{{ route('contracts.index', ['status' => 'declined']) }}">Отклоненные</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'completed') active @endif" href="{{ route('contracts.index', ['status' => 'completed']) }}">Выполненные</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'new') active @endif" href="{{ route('contracts.index', ['status' => 'new']) }}">Неподписанные</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($status == 'unsigned') active @endif" href="{{ route('contracts.index', ['status' => 'unsigned']) }}">Загрузить скан</a>
        </li>
    </ul>

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Номер договра</th>
            <th>Дата</th>
            @if ($status == 'declined')
                <th>Дата отказа</th>
            @endif
            <th>Номер папки</th>
            <th>Тип договора</th>
            <th>Юридическое название</th>
            <th>Бренд</th>
            <th>Интернет-маркетолог</th>
            @if ($status != 'unsigned')
                <th>Скан договора</th>
            @endif
            <th>Способ оплаты</th>
            <th>Статус</th>
            <th>Действие</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contracts as $contract)
            <tr>
                <td>{{ $contract->id }}</td>
                <td>
                    @if ($contract->contract)
                        <a href="{{ route('contracts.' . $contract->contract_type . '.show', ['contract' => $contract->contract]) }}">{{ $contract->contract_number }}</a>
                    @else
                        {{ $contract->contract_number }}
                    @endif
                </td>
                <td>{{ $contract->date }}</td>
                @if ($contract->isDeclined())
                    <td>{{ $contract->cancellation_date }}</td>
                @endif
                <td>{{ $contract->folder_number }}</td>
                <td>{{ \App\Contract::getTypeByAlias($contract->contract_type) }}</td>
                <td><a href="{{ route('clients.show', ['id' => $contract->client->id]) }}">{{ $contract->client->legal_name }}, {{ $contract->client->ownershipType->name }}</a></td>
                <td>{{ $contract->client->brand }}</td>
                <td>
                    {{ $contract->marketer ? $contract->marketer->name : ''}}
                </td>
                @if ($status != 'unsigned')
                <td>
                    @if ($contract->contract && $contract->contract->scanned_copy)
                        <a href="{{ route('contracts.' . $contract->contract_type . '.download', ['contract' => $contract->contract->id]) }}">Скачать</a>
                    @endif
                </td>
                @endif
                <td>
                    @if ($contract->contract)
                    {{ $contract->contract->paymentMethod ? $contract->contract->paymentMethod->name : '' }}
                    @endif
                </td>
                <td>
                    @if ($contract->status !== null && $contract->status != \App\Contract::STATUS_PENDING)
                        <span class="badge badge-success">{{ __('Signed') }}</span>
                    @else
                        <span class="badge badge-danger">{{ __('Not Signed') }}</span>
                    @endif
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @if ($contract->contract)
                            @permission('update-' . $contract->contract_type . '-contracts')
                                <a class="btn btn-light" href="{{ route('contracts.' . $contract->contract_type . '.edit', ['id' => $contract->contract->id]) }}">Редактировать договор</a>
                            @endpermission
                            @if (in_array($contract->contract_type, ['adm', 'seo', 'smm', 'ca']))
                                <a class="btn btn-light" href="{{ route('contracts.' . $contract->contract_type . '.show', ['contract' => $contract->contract]) }}">Корректировать договор</a>
                            @endif
                        @else
                            @permission('create-' . $contract->contract_type . '-contracts')
                                <a class="btn btn-light" href="{{ route('contracts.' . $contract->contract_type . '.create') }}?contract_id={{ $contract->id }}">
                                    @if ($contract->isCorrectiveContract())
                                        Создать доп. соглашение
                                    @elseif ($contract->isAdditionalContract())
                                        Создать доп. работы
                                    @else
                                        {{ __('Create Contract') }}
                                    @endif
                                </a>
                            @endpermission
                        @endif
                        @permission('update-contracts')
                            <a class="btn btn-secondary" title="Редактировать" href="{{ route('contracts.edit', ['id' => $contract->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-contracts')
                            <button class="btn btn-danger" title="Удалить" data-href="{{ route('contracts.destroy', ['id' => $contract->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="15">Ничего не найдено</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $contracts->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-force-delete')
@endsection