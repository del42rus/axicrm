@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.index') }}">Реестр договоров</a></li>
    </ol>

    @if ($contract->isCorrectiveContract())
        <h3 class="page-header">Редактировать доп. соглашение</h3>
        <div class="row">
            <div class="col-lg-4 col-md-9">
                @include('contract.partial.form-correct', ['contract' => $contract])
            </div>
        </div>
    @else
        <h3 class="page-header">Редактировать договор</h3>
        <div class="row">
            <div class="col-lg-4 col-md-9">
                @include('contract.partial.form', ['contract' => $contract])
            </div>
        </div>
    @endif
@endsection
