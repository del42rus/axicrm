<form method="post" action="@if (isset($contract)) {{ route('contracts.update', $contract->id) }} @endif">
    {{ csrf_field() }}
    @if (isset($contract))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @endif
    <div class="form-group">
        <label>Номер доп. соглашения</label>
        <input type="text" name="number" class="form-control @if ($errors->has('number')) is-invalid @endif" value="{{ old('number') ?: (isset($contract) ? $contract->additional_number : '')}}">
        @if ($errors->has('number'))
            <div class="invalid-feedback">{{ $errors->first('number') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Дата</label>
        <input type="text" name="date" class="datepicker form-control @if ($errors->has('date')) is-invalid @endif" value="{{ old('date') ?: (isset($contract) ? $contract->date : '')}}">
        @if ($errors->has('date'))
            <div class="invalid-feedback">{{ $errors->first('date') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>