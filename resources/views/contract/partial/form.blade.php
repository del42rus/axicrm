<form method="post" action="@if (isset($contract)){{ route('contracts.update', $contract->id) }}@else{{ route('contracts.store') }}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($contract))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @endif

    <div class="form-group">
        <label>Тип договора</label>
        <select name="contract_type">
            @foreach(\App\Contract::getTypes() as $key => $name)
                @if (Auth::user()->hasPermission('read-' . $key . '-contracts'))
                    <option value="{{ $key }}" @if (old('contract_type') == $key) selected="selected" @elseif (isset($contract) && $contract->contract_type == $key) selected="selected" @endif>{{ $name }}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Клиент</label>
        <select name="client_id" class="@if ($errors->has('client_id')) is-invalid @endif">
            <option value="">Нет</option>
            @foreach (App\Client::all() as $client)
                <option value="{{ $client->id }}" @if (old('client_id') == $client->id) selected="selected" @elseif (isset($contract) && $contract->client->id == $client->id) selected="selected" @endif>{{ $client->getFullName() }}</option>
            @endforeach
        </select>
        @if ($errors->has('client_id'))
            <div class="invalid-feedback">{{ $errors->first('client_id') }}</div>
        @endif
    </div>

    @php
        $isAdditionalContract = (isset($contract) && $contract->isAdditionalContract()) || (!isset($contract) && strpos(old('contract_type'), 'additional') === 0);
    @endphp

    <div class="form-group @if (!$isAdditionalContract) d-none @endif">
        <label>Привязать к договору</label>
        <select name="parent_id" class="@if ($errors->has('parent_id')) is-invalid @endif">
            <option value="">Нет</option>
            @if (old('client_id'))
                @php
                    $query = App\Contract::where('client_id', old('client_id'))->whereNull('parent_id');

                    if (isset($contract)) {
                        $query->where('id', '<>', $contract->id);
                    }

                    $contracts = $query->get();
                @endphp
                @foreach ($contracts as $parent)
                    <option value="{{ $parent->id }}" @if (old('parent_id') == $parent->id) selected @endif>{{ $parent->contract_number . ' (' . \App\Contract::getTypeByAlias($parent->contract_type) . ')' }}</option>
                @endforeach
            @elseif (isset($contract) && $contract->isAdditionalContract())
                @php
                    $query = App\Contract::where('client_id', $contract->client_id)
                        ->whereNull('parent_id')
                        ->where('id', '<>', $contract->id);

                    $contracts = $query->get();
                @endphp
                @foreach ($contracts as $parent)
                    <option value="{{ $parent->id }}" @if (old('parent_id') == $parent->id) selected @elseif ($contract->parent_id == $parent->id) selected @endif>{{ $parent->contract_number . ' (' . \App\Contract::getTypeByAlias($parent->contract_type) . ')' }}</option>
                @endforeach
            @endif
        </select>
        @if ($errors->has('parent_id'))
            <div class="invalid-feedback">{{ $errors->first('parent_id') }}</div>
        @endif
    </div>

    @php
        $isHidden = (isset($contract) && $contract->isAttachedAdditionalContract() && !$errors->has('contract_number')) || (!isset($contract) && old('parent_id')) || $errors->has('number');
    @endphp

    <div class="form-group @if ($isHidden) d-none @endif">
        <label>Номер договора</label>
        <input type="text" name="contract_number" class="form-control @if ($errors->has('contract_number')) is-invalid @endif" value="{{ old('contract_number') ?: (isset($contract) ? $contract->contract_number : '')}}">
        @if ($errors->has('contract_number'))
            <div class="invalid-feedback">{{ $errors->first('contract_number') }}</div>
        @endif
    </div>

    @php
        $isHidden = (isset($contract) && !$contract->isAttachedAdditionalContract() && !$errors->has('number')) || (!isset($contract) && !old('parent_id')) || $errors->has('contract_number');
    @endphp

    <div class="form-group @if ($isHidden) d-none @endif ">
        <label>Номер доп.соглашения</label>
        <input type="text" name="number" class="form-control @if ($errors->has('number')) is-invalid @endif" value="{{ old('number') ?: (isset($contract) ? $contract->additional_number : '') }}">
        @if ($errors->has('number'))
            <div class="invalid-feedback">{{ $errors->first('number') }}</div>
        @endif
    </div>

    <div class="form-group">
        <label>Дата</label>
        <input type="text" name="date" class="datepicker form-control @if ($errors->has('date')) is-invalid @endif" value="{{ old('date') ?: (isset($contract) ? $contract->date : '')}}">
        @if ($errors->has('date'))
            <div class="invalid-feedback">{{ $errors->first('date') }}</div>
        @endif
    </div>

    @permission('edit-folder-contracts')
        <div class="form-group">
            <label>Номер папки</label>
            <input type="text" name="folder_number" class="form-control @if ($errors->has('folder_number')) is-invalid @endif" value="{{ old('folder_number') ?: (isset($contract) ? $contract->folder_number : '')}}">
            @if ($errors->has('folder_number'))
                <div class="invalid-feedback">{{ $errors->first('folder_number') }}</div>
            @endif
        </div>
    @endpermission

    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>

@section('js')
<script>
    $(function () {
        var $contractTypeEl = $('select[name="contract_type"]');

        var $contractNumberEl = $('input[name="contract_number"]');
        var $contractNumberElWrapper = $contractNumberEl.parent('.form-group');
        var $additionalNumberEl = $('input[name="number"]');
        var $additionalNumberElWrapper = $additionalNumberEl.parent('.form-group');

        var $parentIdEl = $('select[name="parent_id"]');
        var $parentIdElWrapper = $parentIdEl.parent('.form-group');

        var $clientIdEl = $('select[name="client_id"]');
        var $contractIdEl = $('input[name="id"]');

        $contractTypeEl.on('change', function () {
            var isAdditionalContract = $(this).val().indexOf('additional') == 0;
            
            $parentIdElWrapper.toggleClass('d-none', !isAdditionalContract);

            if (isAdditionalContract) {
                $parentIdEl.removeAttr('disabled');

                if ($clientIdEl.val()) {
                    $clientIdEl.trigger('change')
                }
            } else {
                $parentIdEl.attr('disabled', 'disabled');
            }
        });

        $clientIdEl.on('change', function () {
            var isAdditionalContract = $contractTypeEl.val().indexOf('additional') == 0;

            if (!isAdditionalContract) {
                return;
            }

            $.ajax({
                url: '/contracts/get-client-contracts',
                type: 'post',
                dataType: 'json',
                data: {client_id: $clientIdEl.val(), 'id': $contractIdEl.val()},
                success: function (res) {
                    $parentIdEl[0].selectize.destroy();
                    $parentIdEl.html('');

                    $parentIdEl.append('<option value="">Нет</option>');
                    for (var key in res) {
                        $parentIdEl.append('<option value="' + key + '">' + res[key] + '</option>')
                    }

                    $parentIdEl.selectize();
                }
            })
        });

        $parentIdEl.on('change', function () {

            $contractNumberElWrapper.toggleClass('d-none', $(this).val());
            $additionalNumberElWrapper.toggleClass('d-none', !$(this).val());

        });
    })
</script>
@endsection