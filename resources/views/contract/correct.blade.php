@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.index') }}">Реестр договоров</a></li>
    </ol>
    <h3 class="page-header">Корректировать договор</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('contract.partial.form-correct')
        </div>
    </div>
@endsection