@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('contracts.adm.index') }}">{{ __('Administration') }}</a></li>
        <li class="breadcrumb-item">{{ $contract->contract->client->getFullName() }}</li>
    </ol>
    <h3 class="page-header">
        @if (!$contract->parent)
            {{ __('Edit Contract') }}
        @else
            Редактировать доп. соглашение
        @endif
    </h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('adm-contract.partial.form', ['contract' => $contract])
        </div>
    </div>
@endsection
