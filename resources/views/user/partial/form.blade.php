<form method="post" action="@if (isset($user)){{ route('users.update', $user->id) }}@else{{ route('users.store') }}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($user))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $user->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Full Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($user) ? $user->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" value="{{ old('email') ?: (isset($user) ? $user->email : '')}}">
        @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Phone') }}</label>
        <input type="text" name="phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" value="{{ old('phone') ?: (isset($user) ? $user->phone : '')}}">
        @if ($errors->has('phone'))
            <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Internal Phone Number') }}</label>
        <input type="text" name="internal_phone_number" class="form-control @if ($errors->has('internal_phone_number')) is-invalid @endif" value="{{ old('internal_phone_number') ?: (isset($user) ? $user->internal_phone_number : '')}}">
        @if ($errors->has('internal_phone_number'))
            <div class="invalid-feedback">{{ $errors->first('internal_phone_number') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Birthday') }}</label>
        <input type="text" name="birthday" class="datepicker form-control @if ($errors->has('birthday')) is-invalid @endif" value="{{ old('birthday') ?: (isset($user) ? $user->birthday : '')}}">
        @if ($errors->has('birthday'))
            <div class="invalid-feedback">{{ $errors->first('birthday') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Position') }}</label>
        <select class="@if ($errors->has('position_id')) is-invalid @endif" name="position_id">
            <option value="">{{ __('None') }}</option>
            @foreach (App\Position::where('inhouse', 1)->get() as $position)
                <option value="{{ $position->id }}" @if (old('position_id') == $position->id) selected="selected" @elseif (isset($user) && $user->position && $user->position->id == $position->id) selected="selected" @endif>{{ $position->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('position_id'))
            <div class="invalid-feedback">{{ $errors->first('position_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Password') }}</label>
        <input type="password" name="password" class="form-control @if ($errors->has('password')) is-invalid @endif">
        @if ($errors->has('password'))
            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Confirm Password') }}</label>
        <input type="password" name="password_confirmation" class="form-control @if ($errors->has('password')) is-invalid @endif">
    </div>
    <div class="form-group">
        @foreach ($roles as $role)
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="roles[]" value="{{ $role->id }}" @if (old('roles') && in_array($role->id, old('roles'))) checked @elseif (isset($user) && in_array($role->id, $user->roles->pluck('id')->toArray())) checked @endif>
                    {{ $role->display_name }}
                </label>
            </div>
        @endforeach
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>