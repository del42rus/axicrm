@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('users.index') }}">{{ __('Users') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Edit User') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('user.partial.form', ['user' => $user])
        </div>
    </div>
@endsection
