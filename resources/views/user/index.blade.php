@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Users') }}</h3>
    @permission('create-users')
    <p>
        <a class="btn btn-secondary" href="{{ route('users.create') }}">{{ __('Create User') }}</a>
    </p>
    @endpermission
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Full Name') }}</th>
            <th>{{ __('Position') }}</th>
            <th>Email</th>
            <th>{{ __('Phone') }}</th>
            <th>{{ __('Internal Phone Number') }}</th>
            <th>{{ __('Birthday') }}</th>
            <th>{{ __('Created At') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->position ? $user->position->name : '' }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->internal_phone_number }}</td>
                <td>{{ $user->birthday ? $user->birthday : ''}}</td>
                <td>{{ $user->created_at }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-users')
                        <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('users.edit', ['id' => $user->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-users')
                        <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('users.destroy', ['id' => $user->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="8">{{ __('Nothing found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $users->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-force-delete')
@endsection
