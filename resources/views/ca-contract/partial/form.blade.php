@php

use App\User;

@endphp

<form method="post" action="@if ($contract instanceof \App\CAContract){{ route('contracts.ca.update', $contract->id) }}@else{{ route('contracts.ca.store') }}@endif" enctype="multipart/form-data" novalidate>
    {{ csrf_field() }}

    @if ($contract instanceof \App\CAContract)
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contract->id }}">
    @else
        <input type="hidden" name="contract_id" value="{{ $contract->id }}">
    @endif

    @php($isCorrectiveContract = false)

    @if ($contract instanceof \App\CAContract)
        @if ($isCorrectiveContract = $contract->contract->isCorrectiveContract())
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract->parent->contract_number }}" readonly disabled>
            </div>
            <div class="form-group">
                <label>Номер доп. соглашения</label>
                <input type="text" name="addition_number" class="form-control" value="{{ $contract->contract->contract_number }}" readonly disabled>
            </div>
        @else
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract->contract_number }}" readonly disabled>
            </div>
        @endif
    @else
        @if ($isCorrectiveContract = $contract->isCorrectiveContract()))
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->parent->contract_number }}" readonly disabled>
            </div>
            <div class="form-group">
                <label>Номер доп. соглашения</label>
                <input type="text" name="addition_number" class="form-control" value="{{ $contract->contract_number }}" readonly disabled>
            </div>
        @else
            <div class="form-group">
                <label>Номер договора</label>
                <input type="text" name="number" class="form-control" value="{{ $contract->contract_number }}" readonly disabled>
            </div>
        @endif
    @endif

    <div class="form-group">
        <label class="d-block">@if ($isCorrectiveContract) Скан доп. соглашения @else Скан договора @endif</label>
        <label class="custom-file">
            <input type="file" name="scanned_copy" class="custom-file-input @if ($errors->has('scanned_copy')) is-invalid @endif" required>
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('scanned_copy'))
            <div class="invalid-feedback d-block">{{ $errors->first('scanned_copy') }}</div>
        @endif
        @if ($contract instanceof \App\CAContract && $contract->scanned_copy)
            <a class="btn btn-link" href="{{ route('contracts.ca.download', ['contract' => $contract->id]) }}">{{ __('Download') }}</a>
        @endif
    </div>
    @if ($contract instanceof \App\CAContract)
        @foreach ($contract->files as $file)
            <div class="form-group">
                <label>Название файла</label>
                <input name="files[id_{{$file->id}}][name]" type="text" class="form-control @if ($errors->has('files.id_' . $file->id .'.name')) is-invalid @endif" value="{{ old('files.id_' . $file->id . '.name') ? : $file->name }}"/>
                @if ($errors->has('files.id_' . $file->id . '.name'))
                    <div class="invalid-feedback">{{ $errors->first('files.id_' . $file->id . '.name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label class="custom-file">
                    <input type="file" name="files[id_{{$file->id}}][file]" class="custom-file-input @if ($errors->has('files.id_' . $file->id . '.file')) is-invalid @endif">
                    <span class="custom-file-control"></span>
                </label>
                @if ($errors->has('files.id_' . $file->id . '.file'))
                    <div class="invalid-feedback d-block">{{ $errors->first('files.id_' . $file->id . '.file') }}</div>
                @endif
                <a class="btn btn-link" href="{{ route('contracts.ca.download-file', ['contract' => $contract->id, 'file' => $file->id]) }}">{{ __('Download') }}</a>
                <a class="btn btn-link" href="{{ route('contracts.ca.delete-file', ['contract' => $contract->id, 'file' => $file->id]) }}">Удалить</a>
            </div>
        @endforeach
    @endif
    <div class="form-group">
        <label>Название файла</label>
        <input name="files[0][name]" type="text" class="form-control @if ($errors->has('files.0.name')) is-invalid @endif" value="{{ old('files.0.name') }}"/>
        @if ($errors->has('files.0.name'))
            <div class="invalid-feedback">{{ $errors->first('files.0.name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label class="custom-file">
            <input type="file" name="files[0][file]" class="custom-file-input @if ($errors->has('files.0.file')) is-invalid @endif">
            <span class="custom-file-control"></span>
        </label>
        @if ($errors->has('files.0.file'))
            <div class="invalid-feedback d-block">{{ $errors->first('files.0.file') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Start Date') }}</label>
        <input type="text" name="start_date" class="datepicker form-control @if ($errors->has('start_date')) is-invalid @endif" value="{{ old('start_date') ?: ($contract instanceof \App\CAContract ? $contract->start_date : '')}}">
        @if ($errors->has('start_date'))
            <div class="invalid-feedback">{{ $errors->first('start_date') }}</div>
        @endif
    </div>

    @if (!$isCorrectiveContract)
        <div class="form-group">
            <label>{{ __('Setup Cost') }}</label>
            <input type="text" name="setup_cost" class="form-control @if ($errors->has('setup_cost')) is-invalid @endif" value="{{ old('setup_cost') ?: ($contract instanceof \App\CAContract ? $contract->setup_cost : '')}}">
            @if ($errors->has('setup_cost'))
                <div class="invalid-feedback">{{ $errors->first('setup_cost') }}</div>
            @endif
        </div>
    @else
        <div class="form-group">
            <label>{{ __('Setup Cost') }}</label>
            <input type="text" name="setup_cost" class="form-control" value="{{ $contract instanceof \App\CAContract ? $contract->setup_cost : $contract->parent->contract->setup_cost }}" readonly>
        </div>
    @endif

    <div class="form-group">
        <label>{{ __('Support Cost') }}</label>
        <input type="text" name="support_cost" class="form-control @if ($errors->has('support_cost')) is-invalid @endif" value="{{ old('support_cost') ?: ($contract instanceof \App\CAContract ? $contract->support_cost : '')}}">
        @if ($errors->has('support_cost'))
            <div class="invalid-feedback">{{ $errors->first('support_cost') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Budget') }}</label>
        <input type="text" name="budget" class="form-control @if ($errors->has('budget')) is-invalid @endif" value="{{ old('budget') ?: ($contract instanceof \App\CAContract ? $contract->budget : '')}}">
        @if ($errors->has('budget'))
            <div class="invalid-feedback">{{ $errors->first('budget') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>Способ оплаты</label>
        <select name="payment_method_id" class="@if ($errors->has('payment_method_id')) is-invalid @endif">
            <option value="">Не указан</option>
            @foreach (\App\PaymentMethod::all() as $paymentMethod)
                <option value="{{ $paymentMethod->id }}" @if (old('payment_method_id') == $paymentMethod->id) selected @elseif ($contract instanceof \App\CAContract && $contract->paymentMethod && $contract->paymentMethod->id == $paymentMethod->id) selected @endif>{{ $paymentMethod->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('payment_method_id'))
            <div class="invalid-feedback">{{ $errors->first('payment_method_id') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Metrika') }}</label>
        <input type="text" name="metrika_link" class="form-control" value="{{ old('metrika') ?: ($contract instanceof \App\CAContract ? $contract->metrika_link : '')}}">
    </div>
    <div class="form-group">
        <label>{{ __('Site') }}</label>
        <input type="text" name="site" class="form-control" value="{{ old('site') ?: ($contract instanceof \App\CAContract ? $contract->site : '')}}">
    </div>
    <div class="form-check">
        <input name="is_budget_opened" type="hidden" value="0">
        <label class="form-check-label">
            <input name="is_budget_opened" type="checkbox" class="form-check-input" value="1" @if (old('is_budget_opened')) checked="checked" @elseif ($contract instanceof \App\CAContract && $contract->is_budget_opened) checked="checked" @endif>
            {{ __('Opened Budget') }}
        </label>
    </div>
    <div class="form-group">
        <label>{{ __('Marketer') }}</label>
        <select name="marketer_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('market_digital')->get() as $marketer)
                <option value="{{ $marketer->id }}" @if (old('marketer_id') == $marketer->id) selected @elseif ($contract instanceof \App\CAContract && $contract->marketer && $contract->marketer->id == $marketer->id) selected @endif>{{ $marketer->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Salesman') }}</label>
        <select name="salesman_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('salesman')->get() as $salesman)
                <option value="{{ $salesman->id }}" @if (old('salesman_id') == $salesman->id) selected @elseif ($contract instanceof \App\CAContract && $contract->salesman && $contract->salesman->id == $salesman->id) selected @endif>{{ $salesman->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>{{ __('Responsible Person') }}</label>
        <select name="responsible_person_id">
            <option value="">{{ __('None') }}</option>
            @foreach (User::whereRoleIs('user_digital_context')->get() as $user)
                <option value="{{ $user->id }}" @if (old('responsible_person_id') == $user->id) selected @elseif ($contract instanceof \App\CAContract && $contract->responsible && $contract->responsible->id == $user->id) selected @endif>{{ $user->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>