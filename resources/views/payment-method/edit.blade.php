@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('payment-methods.index') }}">Способы оплаты</a></li>
    </ol>
    <h3 class="page-header">Редактировать способы оплаты</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('payment-method.partial.form', ['paymentMethod' => $paymentMethod])
        </div>
    </div>
@endsection
