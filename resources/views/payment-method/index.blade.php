@extends('layouts.app')

@section('content')
    <h3 class="page-header">Способы оплаты</h3>

    @permission('create-dictionaries')
    <p>
        <a class="btn btn-secondary" href="{{ route('payment-methods.create') }}">Создать способ оплаты</a>
    </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Способ оплаты</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($paymentMethods as $paymentMethod)
            <tr>
                <td>{{ $paymentMethod->id }}</td>
                <td>{{ $paymentMethod->name }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                        <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('payment-methods.edit', ['id' => $paymentMethod->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                        <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('payment-methods.destroy', ['id' => $paymentMethod->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-force-delete')
@endsection