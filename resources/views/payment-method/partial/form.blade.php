<form method="post" action="@if (isset($paymentMethod)){{ route('payment-methods.update', $paymentMethod->id) }}@else{{ route('payment-methods.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($paymentMethod))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $paymentMethod->id }}">
    @endif
    <div class="form-group">
        <label>Способ оплаты</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($paymentMethod) ? $paymentMethod->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>