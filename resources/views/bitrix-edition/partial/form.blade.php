<form method="post" action="@if (isset($edition)){{ route('bitrix-editions.update', $edition->id) }}@else{{ route('bitrix-editions.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($edition))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $edition->id }}">
    @endif
    <div class="form-group">
        <label>Название редакции</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($edition) ? $edition->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>