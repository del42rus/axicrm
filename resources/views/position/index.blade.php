@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Positions') }}</h3>

    @permission('create-dictionaries')
    <p>
        <a class="btn btn-secondary" href="{{ route('positions.create') }}">{{ __('Create Position') }}</a>
    </p>
    @endpermission

    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Position Name') }}</th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($positions as $position)
            <tr>
                <td>{{ $position->id }}</td>
                <td>{{ $position->name }}</td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @permission('update-dictionaries')
                        <a class="btn btn-secondary" title="{{ __('Edit') }}" href="{{ route('positions.edit', ['id' => $position->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        @endpermission
                        @permission('delete-dictionaries')
                        <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('positions.destroy', ['id' => $position->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        @endpermission
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">{{ __('Nothing Found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>

    @include('partial.confirm-force-delete')
@endsection