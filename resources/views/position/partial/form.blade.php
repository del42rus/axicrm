<form method="post" action="@if (isset($position)){{ route('positions.update', $position->id) }}@else{{ route('positions.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($position))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $position->id }}">
    @endif
    <div class="form-group">
        <label>{{ __('Position Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($position) ? $position->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input name="inhouse" type="hidden" value="0">
            <input name="inhouse" type="checkbox" class="form-check-input" value="1" @if (old('inhouse')) checked="checked" @elseif (isset($position) && $position->inhouse) checked="checked" @endif>
            Внутрифирменная
        </label>
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>