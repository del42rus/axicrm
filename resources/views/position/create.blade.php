@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('positions.index') }}">{{ __('Positions') }}</a></li>
    </ol>
    <h3 class="page-header">{{ __('Create Position') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('position.partial.form')
        </div>
    </div>
@endsection
