@extends('layouts.app')

@section('content')
    <h3 class="page-header">{{ __('Client\'s Contacts') }}</h3>
    <p>
        @permission('create-contacts')
            <a class="btn btn-secondary" href="{{ route('contacts.create', ['contactable' => 'client']) }}">{{ __('Create Contact') }}</a>
        @endpermission

        @permission('export-contacts')
            <a class="btn btn-primary" href="{{ route('contacts.clients.download') }}?{{http_build_query(request()->query())}}"><i class="fa fa-file-excel-o"></i>&nbsp;{{ __('To Excel') }}</a>
        @endpermission
    </p>
    <form action="" method="get" class="mb-3">
        <div class="form-row">
            <div class="form-group col-lg-2">
                <select name="client_id">
                    <option value="">{{ __('Client') }}</option>
                    @foreach (\App\Client::all() as $client)
                        <option value="{{ $client->id }}" @if (Request::get('client_id') == $client->id) selected @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="brand">
                    <option value="">{{ __('Brand') }}</option>
                    @foreach (App\Client::all() as $client)
                        <option value="{{ $client->brand }}" @if (request('brand') == $client->brand) selected @endif>{{ $client->brand }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <select name="activity_id">
                    <option value="">Вид деятельности</option>
                    @foreach (\App\Activity::all() as $activity)
                        <option value="{{ $activity->id }}" @if (request('activity_id') == $activity->id) selected @endif>{{ $activity->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="name" placeholder="{{ __('Full Name') }}" value="{{ request('name') }}">
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="email" placeholder="Email" value="{{ Request::get('email') }}">
            </div>
            <div class="form-group col-lg-2">
                <input class="form-control" name="phone" placeholder="{{ __('Phone') }}" value="{{ Request::get('phone') }}">
            </div>
            <div class="form-group col-lg-2">
                <div class="form-check mt-2">
                    <input class="form-check-input ml-0" type="checkbox" id="checkRemoved" name="removed" value="1" @if (request('removed') == '1') checked @endif>
                    <label class="form-check-label" for="checkRemoved">Архив</label>
                </div>
            </div>
            <div class="form-group col-lg-2">
                <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
            </div>
        </div>
    </form>
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('Full Name') }}</th>
            <th>{{ __('E-mail') }}</th>
            <th>{{ __('Phone') }}</th>
            <th>{{ __('Position') }}</th>
            <th><a href="{{ route('contacts.clients', ['sort' => (request('sort') == '-client' || !request('sort')) ? 'client' : '-client']) }}">
                    {{ __('Client') }}
                    @if (in_array(request('sort'), ['client', '-client']))
                        <i class="fa fa-sort-alpha-{{ request('sort') == '-client' ? 'desc' : 'asc' }}"></i>
                    @endif
                </a>
            </th>
            <th>{{ __('Action') }}</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($contacts as $contact)
            <tr>
                <td>{{ $contact->id }}</td>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->phone }}</td>
                <td>{{ $contact->position ? $contact->position->name : '' }}</td>
                <td>
                    <a href="{{ route('clients.show', ['id' => $contact->contactable->id]) }}">{{ $contact->contactable->getFullName() }}</a>
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        @if ($contact->trashed())
                            @permission('restore-contacts')
                                <a class="btn btn-success" title="Восстановить" href="{{ route('contacts.restore', ['id' => $contact->id]) }}"><i class="fa fa-history" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('delete-contacts')
                                <button class="btn btn-danger" title="{{ __('Delete') }}" data-href="{{ route('contacts.force-delete', ['id' => $contact->id]) }}" data-toggle="modal" data-target="#confirm-force-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                        @else
                            @permission('update-contacts')
                                <a class="btn btn-secondary" title="Edit" href="{{ route('contacts.edit', ['id' => $contact->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endpermission
                            @permission('soft-delete-contacts')
                                <button class="btn btn-danger" title="Поместить в архив" data-href="{{ route('contacts.destroy', ['id' => $contact->id]) }}" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            @endpermission
                        @endif
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="7">{{ __('Nothing found') }}</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $contacts->links('vendor.pagination.bootstrap-4') }}

    @include('partial.confirm-delete')
    @include('partial.confirm-force-delete')
@endsection
