@extends('layouts.app')

@section('content')
    @if ($contact->contactable_type == 'client')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('clients.contacts', ['client' => $contact->contactable->id]) }}">{{ __('Contacts') }}</a></li>
            <li class="breadcrumb-item active">{{ $contact->contactable->getFullName() }}</li>
        </ol>
    @elseif ($contact->contactable_type == 'supplier')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('suppliers.contacts', ['supplier' => $contact->contactable->id]) }}">{{ __('Contacts') }}</a></li>
            <li class="breadcrumb-item active">{{ $contact->contactable->getFullName() }}</li>
        </ol>
    @endif
    <h3 class="page-header">{{ __('Edit Contact') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('contact.partial.form', ['contact' => $contact])
        </div>
    </div>
@endsection
