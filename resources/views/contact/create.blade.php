@extends('layouts.app')

@section('content')
    @if (isset($client))
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('clients.contacts', ['client' => $client->id]) }}">{{ $client->getFullName() }}</a></li>
        </ol>
    @elseif (isset($supplier))
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('suppliers.contacts', ['supplier' => $supplier->id]) }}">{{ $supplier->getFullName() }}</a></li>
        </ol>
    @elseif (request('contactable') == 'client')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('contacts.clients') }}">{{ __('Contacts') }}</a></li>
        </ol>
    @elseif (request('contactable') == 'supplier')
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('contacts.suppliers') }}">{{ __('Contacts') }}</a></li>
        </ol>
    @elseif (request('client_id'))
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('clients.contacts', ['client' => request('client_id')]) }}">{{ __('Contacts') }}</a></li>
            <li class="breadcrumb-item active">{{ \App\Client::findOrFail(request('client_id'))->getFullName() }}</li>
        </ol>
    @elseif (request('supplier_id'))
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('suppliers.contacts', ['supplier' => request('supplier_id')]) }}">{{ __('Contacts') }}</a></li>
            <li class="breadcrumb-item active">{{ \App\Supplier::findOrFail(request('supplier_id'))->getFullName() }}</li>
        </ol>
    @endif
    <h3 class="page-header">{{ __('Create Contact') }}</h3>
    <div class="row">
        <div class="col-lg-4 col-md-9">
            @include('contact.partial.form')
        </div>
    </div>
@endsection
