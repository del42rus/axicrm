<form method="post" action="@if (isset($contact)){{ route('contacts.update', $contact->id) }}@else{{ route('contacts.store')}}@endif" novalidate>
    {{ csrf_field() }}
    @if (isset($contact))
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $contact->id }}">
    @endif

    @if (request('client_id'))
        <input type="hidden" name="client_id" value="{{ request('client_id') }}">
    @endif

    @if (request('supplier_id'))
        <input type="hidden" name="supplier_id" value="{{ request('supplier_id') }}">
    @endif

    @if (!isset($contact) && !request('client_id') && !request('supplier_id'))
        @if (request('contactable') == 'client')
            <div class="form-group">
                <label>{{ __('Client') }}</label>
                <select class="@if ($errors->has('client_id')) is-invalid @endif" name="client_id">
                    <option value="">{{ __('None') }}</option>
                    @foreach (App\Client::all() as $client)
                        <option value="{{ $client->id }}" @if (old('client_id') == $client->id) selected="selected" @elseif (isset($contact) && $contact->client->id == $client->id) selected="selected" @endif>{{ $client->getFullName() }}</option>
                    @endforeach
                </select>
                @if ($errors->has('client_id'))
                    <div class="invalid-feedback">{{ $errors->first('client_id') }}</div>
                @endif
            </div>
        @endif

        @if (request('contactable') == 'supplier')
            <div class="form-group">
                <label>{{ __('Supplier') }}</label>
                <select class="@if ($errors->has('client_id')) is-invalid @endif" name="supplier_id">
                    <option value="">{{ __('None') }}</option>
                    @foreach (App\Supplier::all() as $supplier)
                        <option value="{{ $supplier->id }}" @if (old('supplier_id') == $supplier->id) selected="selected" @elseif (isset($contact) && $contact->client->id == $supplier->id) selected="selected" @endif>{{ $supplier->getFullName() }}</option>
                    @endforeach
                </select>
                @if ($errors->has('supplier_id'))
                    <div class="invalid-feedback">{{ $errors->first('supplier_id') }}</div>
                @endif
            </div>
        @endif
    @endif
    <div class="form-group">
        <label>{{ __('Full Name') }}</label>
        <input type="text" name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') ?: (isset($contact) ? $contact->name : '')}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Email') }}</label>
        <input type="email" name="email" class="form-control @if ($errors->has('email')) is-invalid @endif"  value="{{ old('email') ?: (isset($contact) ? $contact->email : '')}}">
        @if ($errors->has('email'))
            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Phone') }}</label>
        <input type="text" name="phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" value="{{ old('phone') ?: (isset($contact) ? $contact->phone : '')}}">
        @if ($errors->has('phone'))
            <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label>{{ __('Position') }}</label>
        <select class="@if ($errors->has('position_id')) is-invalid @endif" name="position_id">
            <option value="">{{ __('None') }}</option>
            @foreach (App\Position::where('inhouse', 0)->get() as $position)
                <option value="{{ $position->id }}" @if (old('position_id') == $position->id) selected="selected" @elseif (isset($contact) && $contact->position && $contact->position->id == $position->id) selected="selected" @endif>{{ $position->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('position_id'))
            <div class="invalid-feedback">{{ $errors->first('position_id') }}</div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
</form>