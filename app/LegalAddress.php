<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalAddress extends Model
{
    protected $fillable = ['agent_id', 'agent_type', 'city_id', 'full_address'];

    public function agent()
    {
        return $this->morphTo();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
