<?php

namespace App\Console\Commands;

use App\Report;
use App\SeoContract;
use App\Service\ReportGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate monthly reports of contracts';

    protected $reportGenerator;

    public function __construct(ReportGenerator $reportGenerator)
    {
        $this->reportGenerator = $reportGenerator;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->reportGenerator->generateSeoContractReports();
        $this->reportGenerator->generateAdmContractReports();
        $this->reportGenerator->generateCaContractReports();
        $this->reportGenerator->generateSmmContractReports();
    }
}
