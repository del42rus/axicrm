<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AmoCRMContract extends Model
{
    use SoftDeletes;

    protected $table = 'amo_crm_contracts';

    protected $fillable = [
        'site', 'cost', 'renewal_cost', 'status', 'amo_crm_edition_id', 'contract_id',
        'salesman_id', 'start_date', 'finish_date', 'user_count', 'admin_login',
        'admin_password'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_date',
        'finish_date'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id');
    }

    public function amoCRMEdition()
    {
        return $this->belongsTo(AmoCRMEdition::class, 'amo_crm_edition_id');
    }

    public function files()
    {
        return $this->morphMany(ContractFile::class, 'contract');
    }

    public function setMarketerIdAttribute($value)
    {
        $this->attributes['marketer_id'] = $value ?? null;
    }

    public function setSalesmanIdAttribute($value)
    {
        $this->attributes['salesman_id'] = $value ?? null;
    }

    private function formatLink($value)
    {
        if (strpos($value, 'http://') === 0 || strpos($value, 'https://') === 0 || empty($value)) {
            return $value;
        }

        return 'http://' . $value;
    }

    public function getSiteAttribute($value)
    {
        return $this->formatLink($value);
    }

    public function getFormattedCost()
    {
        return number_format($this->cost, 0, ',', ' ');
    }

    public function getFormattedRenewalCost()
    {
        return number_format($this->renewal_cost, 0, ',', ' ');
    }

    public function getStartDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getFinishDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setFinishDateAttribute($value)
    {
        $this->attributes['finish_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function isActive()
    {
        return $this->status == Contract::STATUS_ACTIVE;
    }

    public function isDeclined()
    {
        return $this->status === Contract::STATUS_DECLINED;
    }

    public function getCancellationDateAttribute($value)
    {
        return $value ? DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y') : '';
    }

    public function setCancellationDateAttribute($value)
    {
        $this->attributes['cancellation_date'] = $value ? DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d') : null;
    }

    public function isExpired()
    {
        $currentDate = new DateTime();
        $finishDate = DateTime::createFromFormat('d/m/Y', $this->finish_date);

        return $finishDate < $currentDate;
    }

    public function isExpiring()
    {
        $currentDate = new DateTime();
        $finishDate = DateTime::createFromFormat('d/m/Y', $this->finish_date);

        $interval = $finishDate->diff($currentDate);

        return $interval->days < 7;
    }

    public function isRecent()
    {
        $currentDate = new DateTime();
        $createdDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);

        $interval = $createdDate->diff($currentDate);

        return $interval->days <= 30;
    }
}