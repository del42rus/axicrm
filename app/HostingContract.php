<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HostingContract extends Model
{
    use SoftDeletes;

    const SERVICE_DOMAIN = 1;
    const SERVICE_HOSTING = 2;
    const SERVICE_HOSTING_AND_DOMAIN = 3;
    const SERVICE_DNS = 4;

    protected $fillable = [
        'domain', 'hosting_service_id', 'status', 'responsible_person_id', 'finish_date', 'payment_method_id', 'contract_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'finish_date'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_person_id');
    }

    public function service()
    {
        return $this->belongsTo(HostingService::class, 'hosting_service_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function files()
    {
        return $this->morphMany(ContractFile::class, 'contract');
    }

    public function setResponsiblePersonIdAttribute($value)
    {
        $this->attributes['responsible_person_id'] = $value ?? null;
    }

    public function getFinishDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setFinishDateAttribute($value)
    {
        $this->attributes['finish_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function isActive()
    {
        return $this->status == Contract::STATUS_ACTIVE;
    }

    public function isDeclined()
    {
        return $this->status === Contract::STATUS_DECLINED;
    }

    public function getCancellationDateAttribute($value)
    {
        return $value ? DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y') : '';
    }

    public function setCancellationDateAttribute($value)
    {
        $this->attributes['cancellation_date'] = $value ? DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d') : null;
    }

    public function isRecent()
    {
        $currentDate = new DateTime();
        $createdDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);

        $interval = $createdDate->diff($currentDate);

        return $interval->days <= 30;
    }
}