<?php

namespace App\Observers;

use App\Client;
use App\Contact;
use App\Supplier;

class UserObserver
{
    public function deleting(User $user)
    {
        Contact::where([['contactable_id', '=', $user->id], ['contactable_type', '=', 'supplier']])->update();
    }
}