<?php

namespace App\Observers;

use App\Contract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ContractObserver
{
    public function deleting(Contract $contract)
    {
        $files = DB::table(str_replace('-', '_', $contract->contract_type) . '_contracts')
            ->where(['contract_id' => $contract->id])
            ->pluck('scanned_copy');

        foreach ($files as $file) {
            Storage::delete('public/' . $file);
        }

        DB::table(str_replace('-', '_', $contract->contract_type) . '_contracts')->where(['contract_id' => $contract->id])->delete();
    }
}