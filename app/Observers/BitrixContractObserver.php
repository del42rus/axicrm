<?php

namespace App\Observers;

use App\BitrixContract;
use App\BitrixCRMContract;
use App\ContractRegistry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BitrixContractObserver
{
    public function deleting(BitrixContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'salesman_id' => null,
        ]);
    }

    public function saved(BitrixContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'salesman_id' => $contract->salesman_id,
        ]);
    }

    public function restored(BitrixContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'salesman_id' => $contract->salesman_id,
        ]);
    }
}