<?php

namespace App\Observers;

use App\BitrixCRMContract;
use App\ContractRegistry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BitrixCRMContractObserver
{
    public function deleting(BitrixCRMContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'salesman_id' => null,
        ]);
    }

    public function saved(BitrixCRMContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'salesman_id' => $contract->salesman_id,
        ]);
    }

    public function restored(BitrixCRMContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'salesman_id' => $contract->salesman_id,
        ]);
    }
}