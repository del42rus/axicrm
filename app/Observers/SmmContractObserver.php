<?php

namespace App\Observers;

use App\ContractRegistry;
use App\SMMContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SmmContractObserver
{
    public function deleting(SMMContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'marketer_id' => null,
            'salesman_id' => null,
            'responsible_person_id' => null,
        ]);
    }

    public function saved(SMMContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }

    public function restored(SMMContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }
}