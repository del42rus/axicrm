<?php

namespace App\Observers;

use App\ContractRegistry;
use App\HostingContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HostingContractObserver
{
    public function deleting(HostingContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'responsible_person_id' => null,
        ]);
    }

    public function saved(HostingContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }

    public function restored(HostingContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }
}