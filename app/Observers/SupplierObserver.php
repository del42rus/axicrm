<?php

namespace App\Observers;

use App\ActualAddress;
use App\Client;
use App\Contact;
use App\LegalAddress;
use App\Supplier;

class SupplierObserver
{
    public function deleting(Supplier $supplier)
    {
        if ($supplier->isForceDeleting()) {
            Contact::where([['contactable_id', '=', $supplier->id], ['contactable_type', '=', 'supplier']])->delete();
            LegalAddress::where([['agent_id', '=', $supplier->id], ['agent_type', '=', 'supplier']])->delete();
            ActualAddress::where([['agent_id', '=', $supplier->id], ['agent_type', '=', 'supplier']])->delete();
        }
    }
}