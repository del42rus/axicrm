<?php

namespace App\Observers;

use App\Client;
use App\Contact;

class ClientObserver
{
    public function deleting(Client $client)
    {
        if ($client->isForceDeleting()) {
            Contact::where([['contactable_id', '=', $client->id], ['contactable_type', '=', 'client']])->delete();
            LegalAddress::where([['agent_id', '=', $client->id], ['agent_type', '=', 'supplier']])->delete();
            ActualAddress::where([['agent_id', '=', $client->id], ['agent_type', '=', 'supplier']])->delete();
        }
    }
}