<?php

namespace App\Observers;

use App\CAContract;
use App\ContractRegistry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CAContractObserver
{
    public function deleting(CAContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'marketer_id' => null,
            'salesman_id' => null,
            'responsible_person_id' => null,
        ]);
    }

    public function saved(CAContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }

    public function restored(CAContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
            'responsible_person_id' => $contract->responsible_person_id,
        ]);
    }
}