<?php

namespace App\Observers;

use App\AdditionalDigitalContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdditionalDigitalContractObserver
{
    public function deleting(AdditionalDigitalContract $contract)
    {
        if ($contract->isForceDeleting()) {
            Storage::delete('public/' . $contract->contract->scanned_copy);
        }

        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => null,
            'marketer_id' => null,
            'salesman_id' => null,
        ]);
    }

    public function saved(AdditionalDigitalContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
        ]);
    }

    public function restored(AdditionalDigitalContract $contract)
    {
        DB::table('contracts')->where('id', $contract->contract_id)->update([
            'status' => $contract->status,
            'marketer_id' => $contract->marketer_id,
            'salesman_id' => $contract->salesman_id,
        ]);
    }
}