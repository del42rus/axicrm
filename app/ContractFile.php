<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractFile extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function contract()
    {
        return $this->morphTo();
    }
}