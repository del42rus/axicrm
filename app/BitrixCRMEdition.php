<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitrixCRMEdition extends Model
{
    protected $table = 'bitrix_crm_editions';

    protected $fillable = ['name', 'type'];

    public $timestamps = false;

    public function getType()
    {
        switch ($this->type) {
            case 'cloud': return 'Облако';
            case 'box': return 'Коробка';
        }

        return '';
    }
}
