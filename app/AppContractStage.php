<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppContractStage extends Model
{
    protected $fillable = ['name'];

    protected $table = 'app_contract_stages';

    public $timestamps = false;
}
