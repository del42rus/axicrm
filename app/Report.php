<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['marketer_id', 'responsible_person_id', 'act_total'];

    public function contract()
    {
        return $this->morphTo();
    }

    public function marketer()
    {
        return $this->belongsTo(User::class, 'marketer_id');
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_person_id');
    }

    public function setMarketerIdAttribute($value)
    {
        $this->attributes['marketer_id'] = $value ?? null;
    }

    public function setResponsiblePersonIdAttribute($value)
    {
        $this->attributes['responsible_person_id'] = $value ?? null;
    }

    public function getYear()
    {
        return (int) $this->created_at->format('Y');
    }

    public function getMonth()
    {
        return (int) $this->created_at->format('m');
    }

    public function getDay()
    {
        return (int) $this->created_at->format('d');
    }
}
