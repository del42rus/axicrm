<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnershipType extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;
}
