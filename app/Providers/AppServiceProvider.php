<?php

namespace App\Providers;

use App\AdditionalContract;
use App\AdditionalDigitalContract;
use App\AdmContract;
use App\AmoCRMContract;
use App\AppContract;
use App\BitrixContract;
use App\BitrixCRMContract;
use App\CAContract;
use App\CIContract;
use App\Client;
use App\Contract;
use App\HostingContract;
use App\Observers\AdditionalContractObserver;
use App\Observers\AdditionalDigitalContractObserver;
use App\Observers\AdmContractObserver;
use App\Observers\AmoCRMContractObserver;
use App\Observers\AppContractObserver;
use App\Observers\BitrixContractObserver;
use App\Observers\BitrixCRMContractObserver;
use App\Observers\CAContractObserver;
use App\Observers\CIContractObserver;
use App\Observers\ClientObserver;
use App\Observers\ContractObserver;
use App\Observers\ContractRegistryObserver;
use App\Observers\HostingContractObserver;
use App\Observers\SeoContractObserver;
use App\Observers\SmmContractObserver;
use App\Observers\SupplierObserver;
use App\SeoContract;
use App\SMMContract;
use App\Supplier;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'client' => 'App\Client',
            'supplier' => 'App\Supplier',
        ]);

        Relation::morphMap([
            Contract::SMM_CONTRACT_TYPE => SMMContract::class,
            Contract::SEO_CONTRACT_TYPE => SeoContract::class,
            Contract::CA_CONTRACT_TYPE => CAContract::class,
            Contract::ADM_CONTRACT_TYPE => AdmContract::class,
            Contract::APP_CONTRACT_TYPE => AppContract::class,
            Contract::CI_CONTRACT_TYPE => CIContract::class,
            Contract::HOSTING_CONTRACT_TYPE => HostingContract::class,
            Contract::ADDITIONAL_CONTRACT_TYPE => AdditionalContract::class,
            Contract::ADDITIONAL_DIGITAL_CONTRACT_TYPE => AdditionalDigitalContract::class,
            Contract::BITRIX_CONTRACT_TYPE => BitrixContract::class,
            Contract::BITRIX_CRM_CONTRACT_TYPE => BitrixCRMContract::class,
            Contract::AMO_CRM_CONTRACT_TYPE => AmoCRMContract::class
        ]);

        Client::observe(ClientObserver::class);
        Supplier::observe(SupplierObserver::class);

        AdmContract::observe(AdmContractObserver::class);
        AppContract::observe(AppContractObserver::class);
        CAContract::observe(CAContractObserver::class);
        CIContract::observe(CIContractObserver::class);
        SeoContract::observe(SeoContractObserver::class);
        SMMContract::observe(SmmContractObserver::class);
        HostingContract::observe(HostingContractObserver::class);
        AdditionalContract::observe(AdditionalContractObserver::class);
        AdditionalDigitalContract::observe(AdditionalDigitalContractObserver::class);
        BitrixContract::observe(BitrixContractObserver::class);
        BitrixCRMContract::observe(BitrixCRMContractObserver::class);
        AmoCRMContract::observe(AmoCRMContractObserver::class);

        Contract::observe(ContractObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
