<?php

namespace App\Service;

use App\AdmContract;
use App\CAContract;
use App\Report;
use App\SeoContract;
use App\SMMContract;
use Illuminate\Support\Facades\DB;

class ReportGenerator
{
    public function generateSeoContractReports()
    {
        $contracts = SeoContract::where('active', 1)->get();

        foreach ($contracts as $contract) {
            $count = Report::where(DB::raw('MONTH(created_at)'), date('m'))
                ->where(DB::raw('YEAR(created_at)'), date('Y'))
                ->where('contract_id', '=', $contract->id)
                ->where('contract_type', '=', 'seo')
                ->count();

            if ($count) {
                continue;
            }

            $report = new Report();
            $report->contract_id = $contract->id;
            $report->contract_type = 'seo';
            $report->marketer_id = $contract->marketer_id;
            $report->responsible_person_id = $contract->responsible_person_id;
            $report->save();
        }
    }

    public function generateSmmContractReports()
    {
        $contracts = SMMContract::where('active', 1)->get();

        foreach ($contracts as $contract) {
            $count = Report::where(DB::raw('MONTH(created_at)'), date('m'))
                ->where(DB::raw('YEAR(created_at)'), date('Y'))
                ->where('contract_id', '=', $contract->id)
                ->where('contract_type', '=', 'smm')
                ->count();

            if ($count) {
                continue;
            }

            $report = new Report();
            $report->contract_id = $contract->id;
            $report->contract_type = 'smm';
            $report->marketer_id = $contract->marketer_id;
            $report->responsible_person_id = $contract->responsible_person_id;
            $report->save();
        }
    }

    public function generateAdmContractReports()
    {
        $contracts = AdmContract::where('active', 1)->get();

        foreach ($contracts as $contract) {
            $count = Report::where(DB::raw('MONTH(created_at)'), date('m'))
                ->where(DB::raw('YEAR(created_at)'), date('Y'))
                ->where('contract_id', '=', $contract->id)
                ->where('contract_type', '=', 'adm')
                ->count();

            if ($count) {
                continue;
            }

            $report = new Report();
            $report->contract_id = $contract->id;
            $report->contract_type = 'adm';
            $report->marketer_id = $contract->marketer_id;
            $report->responsible_person_id = $contract->responsible_person_id;
            $report->save();
        }
    }

    public function generateCaContractReports()
    {
        $contracts = CAContract::where('active', 1)->get();

        foreach ($contracts as $contract) {
            $count = Report::where(DB::raw('MONTH(created_at)'), date('m'))
                ->where(DB::raw('YEAR(created_at)'), date('Y'))
                ->where('contract_id', '=', $contract->id)
                ->where('contract_type', '=', 'ca')
                ->count();

            if ($count) {
                continue;
            }

            $report = new Report();
            $report->contract_id = $contract->id;
            $report->contract_type = 'ca';
            $report->marketer_id = $contract->marketer_id;
            $report->responsible_person_id = $contract->responsible_person_id;
            $report->save();
        }
    }
}