<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = ['name', 'inhouse'];

    public $timestamps = false;

    public function isInHouse()
    {
        return (bool) $this->inhouse;
    }
}
