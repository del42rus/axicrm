<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalDigitalContract extends Model
{
    use SoftDeletes;

    const PAYMENT_TYPE_FIXED = 1;
    const PAYMENT_TYPE_BY_POSITION = 2;

    protected $table = 'additional_digital_contracts';

    protected $fillable = [
        'site', 'budget', 'status', 'payment_method_id', 'contract_id',
        'marketer_id', 'salesman_id', 'start_date', 'finish_date', 'social_media'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'start_date',
        'finish_date',
        'actual_finish_date'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function marketer()
    {
        return $this->belongsTo(User::class, 'marketer_id');
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id');
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_person_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function files()
    {
        return $this->morphMany(ContractFile::class, 'contract');
    }

    public function setMarketerIdAttribute($value)
    {
        $this->attributes['marketer_id'] = $value ?? null;
    }

    public function setSalesmanIdAttribute($value)
    {
        $this->attributes['salesman_id'] = $value ?? null;
    }

    private function formatLink($value)
    {
        if (strpos($value, 'http://') === 0 || strpos($value, 'https://') === 0 || empty($value)) {
            return $value;
        }

        return 'http://' . $value;
    }

    public function getSiteAttribute($value)
    {
        return $this->formatLink($value);
    }

    public function getFormattedBudget()
    {
        return number_format($this->budget, 0, ',', ' ');
    }

    public function getStartDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getFinishDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setFinishDateAttribute($value)
    {
        $this->attributes['finish_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getActualFinishDateAttribute($value)
    {
        if ($value) {
            return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
        }

        return null;
    }

    public function setActualFinishDateAttribute($value)
    {
        $this->attributes['actual_finish_date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function isCompleted()
    {
        return $this->status == Contract::STATUS_COMPLETED;
    }

    public function isDeclined()
    {
        return $this->status === Contract::STATUS_DECLINED;
    }

    public function isActive()
    {
        return $this->status == Contract::STATUS_ACTIVE;
    }

    public function getCancellationDateAttribute($value)
    {
        return $value ? DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y') : '';
    }

    public function setCancellationDateAttribute($value)
    {
        $this->attributes['cancellation_date'] = $value ? DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d') : null;
    }

    public function isRecent()
    {
        $currentDate = new DateTime();
        $createdDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at);

        $interval = $createdDate->diff($currentDate);

        return $interval->days <= 30;
    }
}