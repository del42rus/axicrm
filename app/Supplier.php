<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ownership_type_id', 'legal_name', 'brand', 'site', 'activity_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class);
    }

    public function actualAddress()
    {
        return $this->morphOne(ActualAddress::class, 'agent');
    }

    public function legalAddress()
    {
        return $this->morphOne(LegalAddress::class, 'agent');
    }

    public function getFullName()
    {
        return $this->legal_name . ', ' . $this->ownershipType->name;
    }

    public function contacts()
    {
        return $this->morphMany(Contact::class, 'contactable');
    }

    public function contract()
    {
        return $this->hasOne(SupplierContract::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function getSiteAttribute($value)
    {
        if (strpos($value, 'http://') === 0 || strpos($value, 'https://') === 0 || empty($value)) {
            return $value;
        }

        return 'http://' . $value;
    }
}
