<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmoCRMEdition extends Model
{
    protected $table = 'amo_crm_editions';

    protected $fillable = ['name'];

    public $timestamps = false;
}
