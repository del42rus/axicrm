<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitrixEdition extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;
}
