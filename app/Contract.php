<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    const STATUS_DECLINED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_PENDING = 3;

    const SMM_CONTRACT_TYPE = 'smm';
    const SEO_CONTRACT_TYPE = 'seo';
    const APP_CONTRACT_TYPE = 'app';
    const ADM_CONTRACT_TYPE = 'adm';
    const CI_CONTRACT_TYPE = 'ci';
    const CA_CONTRACT_TYPE = 'ca';
    const HOSTING_CONTRACT_TYPE = 'hosting';
    const BITRIX_CONTRACT_TYPE = 'bitrix';
    const BITRIX_CRM_CONTRACT_TYPE = 'bitrix-crm';
    const AMO_CRM_CONTRACT_TYPE = 'amo-crm';
    const ADDITIONAL_CONTRACT_TYPE = 'additional';
    const ADDITIONAL_DIGITAL_CONTRACT_TYPE = 'additional-digital';

    protected $table = 'contracts';

    protected $fillable = ['client_id', 'contract_number', 'contract_type', 'date', 'folder_number'];

    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    public function contract()
    {
        switch ($this->contract_type) {
            case self::SMM_CONTRACT_TYPE:
                return $this->hasOne(SMMContract::class);
            case self::SEO_CONTRACT_TYPE:
                return $this->hasOne(SeoContract::class);
            case self::APP_CONTRACT_TYPE:
                return $this->hasOne(AppContract::class);
            case self::ADM_CONTRACT_TYPE:
                return $this->hasOne(AdmContract::class);
            case self::CI_CONTRACT_TYPE:
                return $this->hasOne(CIContract::class);
            case self::CA_CONTRACT_TYPE:
                return $this->hasOne(CAContract::class);
            case self::HOSTING_CONTRACT_TYPE:
                return $this->hasOne(HostingContract::class);
            case self::BITRIX_CONTRACT_TYPE:
                return $this->hasOne(BitrixContract::class);
            case self::BITRIX_CRM_CONTRACT_TYPE:
                return $this->hasOne(BitrixCRMContract::class);
            case self::AMO_CRM_CONTRACT_TYPE:
                return $this->hasOne(AmoCRMContract::class);
            case self::ADDITIONAL_CONTRACT_TYPE:
                return $this->hasOne(AdditionalContract::class);
            case self::ADDITIONAL_DIGITAL_CONTRACT_TYPE:
                return $this->hasOne(AdditionalDigitalContract::class);
        }
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function parent()
    {
        return $this->belongsTo(Contract::class);
    }

    public function children()
    {
        return $this->hasMany(Contract::class);
    }

    public function marketer()
    {
        return $this->belongsTo(User::class, 'marketer_id');
    }

    public function salesman()
    {
        return $this->belongsTo(User::class, 'salesman_id');
    }

    public function responsible()
    {
        return $this->belongsTo(User::class, 'responsible_person_id');
    }

    public function getDateAttribute($value)
    {
        return DateTime::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function isCorrectiveContract()
    {
        return $this->parent_id && $this->additional_number && strpos($this->contract_type, 'additional') === false;
    }

    public function isAdditionalContract()
    {
        return strpos($this->contract_type, 'additional') === 0;
    }

    public function isAttachedAdditionalContract()
    {
        return strpos($this->contract_type, 'additional') === 0 && $this->parent_id;
    }

    public static function getTypes()
    {
        return [
            self::ADM_CONTRACT_TYPE => 'Администрирование',
            self::APP_CONTRACT_TYPE => 'Сайты и приложения',
            self::CA_CONTRACT_TYPE => 'Контекстная реклама',
            self::CI_CONTRACT_TYPE => 'Фирменный стиль',
            self::SMM_CONTRACT_TYPE => 'SMM',
            self::SEO_CONTRACT_TYPE => 'SEO',
            self::HOSTING_CONTRACT_TYPE => 'Хостинг/Домен',
            self::ADDITIONAL_CONTRACT_TYPE => 'Доп. работы ПО',
            self::ADDITIONAL_DIGITAL_CONTRACT_TYPE => 'Доп. работы ОАиП',
            self::BITRIX_CONTRACT_TYPE => 'CMS:1С-Битрикс',
            self::BITRIX_CRM_CONTRACT_TYPE => 'CRM:Битрикс-24',
            self::AMO_CRM_CONTRACT_TYPE => 'CRM:Amo.CRM',
        ];
    }

    public static function getTypeByAlias($alias)
    {
        return self::getTypes()[$alias];
    }

    public function setMarketerIdAttribute($value)
    {
        $this->attributes['marketer_id'] = $value ?? null;
    }

    public function setSalesmanIdAttribute($value)
    {
        $this->attributes['salesman_id'] = $value ?? null;
    }

    public function setResponsiblePersonIdAttribute($value)
    {
        $this->attributes['responsible_person_id'] = $value ?? null;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeclined()
    {
        return $this->status === self::STATUS_DECLINED;
    }

    public function isCompleted()
    {
        return $this->status === self::STATUS_COMPLETED;
    }

    public function isPending()
    {
        return $this->status === self::STATUS_PENDING;
    }
}
