<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierContract extends Model
{
    protected $table = 'supplier_contracts';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}