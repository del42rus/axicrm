<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ownership_type_id', 'legal_name', 'brand', 'site', 'activity_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class);
    }

    public function getFullName()
    {
        return $this->legal_name . ', ' . $this->ownershipType->name;
    }

    public function actualAddress()
    {
        return $this->morphOne(ActualAddress::class, 'agent');
    }

    public function legalAddress()
    {
        return $this->morphOne(LegalAddress::class, 'agent');
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    public function caContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'ca')
            ->join('ca_contracts', 'ca_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('ca_contracts.deleted_at')
            ->get();
    }

    public function seoContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'seo')
            ->join('seo_contracts', 'seo_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('seo_contracts.deleted_at')
            ->get();
    }

    public function smmContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'smm')
            ->join('smm_contracts', 'smm_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('smm_contracts.deleted_at')
            ->get();
    }

    public function appContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'app')
            ->join('app_contracts', 'app_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('app_contracts.deleted_at')
            ->get();
    }

    public function admContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'adm')
            ->join('adm_contracts', 'adm_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('adm_contracts.deleted_at')
            ->get();
    }

    public function ciContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'ci')
            ->join('ci_contracts', 'ci_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('ci_contracts.deleted_at')
            ->get();
    }

    public function hostingContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'hosting')
            ->join('hosting_contracts', 'hosting_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('hosting_contracts.deleted_at')
            ->get();
    }

    public function additionalContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'additional')
            ->join('additional_contracts', 'additional_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('additional_contracts.deleted_at')
            ->get();
    }

    public function additionalDigitalContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'additional-digital')
            ->join('additional_digital_contracts', 'additional_digital_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('additional_digital_contracts.deleted_at')
            ->get();
    }

    public function bitrixContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'bitrix')
            ->join('bitrix_contracts', 'bitrix_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('bitrix_contracts.deleted_at')
            ->get();
    }

    public function bitrixCrmContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'bitrix-crm')
            ->join('bitrix_crm_contracts', 'bitrix_crm_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('bitrix_crm_contracts.deleted_at')
            ->get();
    }

    public function amoCrmContracts()
    {
        return $this->contracts()
            ->select('contracts.*')
            ->where('contract_type', 'amo-crm')
            ->join('amo_crm_contracts', 'amo_crm_contracts.contract_id', '=', 'contracts.id')
            ->whereNull('amo_crm_contracts.deleted_at')
            ->get();
    }

    public function contacts()
    {
        return $this->morphMany(Contact::class, 'contactable');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function getSiteAttribute($value)
    {
        if (strpos($value, 'http://') === 0 || strpos($value, 'https://') === 0 || empty($value)) {
            return $value;
        }

        return 'http://' . $value;
    }

    public function scopeHasContractType($query, $type)
    {
        return $query->select('clients.*')
            ->join('contracts', function ($join) use ($type) {
                $join->on('contracts.client_id', '=', 'clients.id')->where('contracts.contract_type', '=', $type);
            })->get();
    }
}
