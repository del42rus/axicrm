<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAmoCRMContract extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_id' => 'required_without:id',
            'scanned_copy' => 'mimes:jpeg,png,pdf',
            'files.*.name' => 'required_with:files.*.file',
            'files.*.file' => 'required_with_all:files.*.name,contract_id|mimes:jpeg,png,pdf',
            'start_date' => 'required|date_format:d/m/Y',
            'finish_date' => 'required|date_format:d/m/Y',
            'cost' => 'required|integer',
            'renewal_cost' => 'required|integer',
            'amo_crm_edition_id' => 'required|exists:amo_crm_editions,id',
            'user_count' => 'required|integer',
            'admin_login' => 'required',
            'admin_password' => 'required',
        ];
    }
}
