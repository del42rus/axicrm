<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class StoreContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required_without_all:supplier_id,id',
            'supplier_id' => 'required_without_all:client_id,id',
            'name' => 'required',
            'email' => [
                'required',
            ],
            'phone' => [
                'required',
            ],
            'position_id' => 'required|exists:positions,id',
        ];
    }
}
