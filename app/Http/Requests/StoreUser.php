<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => [
                'required',
                Rule::unique('users')->ignore($this->id)
            ],
            'phone' => [
                'nullable',
                Rule::unique('users')->ignore($this->id)
            ],
            'internal_phone_number' => [
                'nullable',
                'digits:3',
                Rule::unique('users')->ignore($this->id)
            ],
            'position_id' => 'nullable|exists:positions,id',
            'birthday' => 'nullable|date_format:d/m/Y',
            'password' => 'required_without:id|nullable|min:6|string|confirmed'
        ];
    }
}
