<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CancelContract extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cancellation_letter' => 'required_without:contract_id|mimes:jpeg,png,pdf',
            'cancellation_agreement' => 'mimes:jpeg,png,pdf',
            'cancellation_date' => 'required|date_format:d/m/Y',
        ];
    }
}
