<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreReport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'act_file' => 'nullable|image',
            'report_file' => 'nullable|image',
            'marketer_id' => 'required|exists:users,id',
            'responsible_person_id' => 'required|exists:users,id',
            'act_total' => 'nullable|integer',
        ];
    }
}
