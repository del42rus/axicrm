<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreHostingContract extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_id' => 'required_without:id',
            'hosting_service_id' => 'required|exists:hosting_services,id',
            'scanned_copy' => 'mimes:jpeg,png,pdf',
            'files.*.name' => 'required_with:files.*.file',
            'files.*.file' => 'required_with_all:files.*.name,contract_id|mimes:jpeg,png,pdf',
            'domain' => 'required',
            'finish_date' => 'required|date_format:d/m/Y',
            'payment_method_id' => 'nullable|exists:payment_methods,id',
        ];
    }
}
