<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSupplier extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ownership_type_id' => 'required|exists:ownership_types,id',
            'contract_scan' => 'required_without:contract_id|image',
            'contract_number' => 'required',
            'activity_id' => 'required|exists:activities,id',
            'legal_name' => [
                'required',
                Rule::unique('suppliers')->ignore($this->id)
            ],
            'legal_address.full_address' => 'required',
            'actual_address.full_address' => 'required'
        ];
    }
}
