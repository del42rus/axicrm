<?php

namespace App\Http\Controllers;

use App\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentMethodController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $paymentMethods = PaymentMethod::orderBy('id', 'asc')->get();

        return view('payment-method.index', [
            'paymentMethods' => $paymentMethods
        ]);
    }

    public function create()
    {
        return view('payment-method.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $paymentMethod = new PaymentMethod($request->all());

        if ($paymentMethod->save()) {
            Session::flash('success', 'Запись сохранена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('payment-methods.edit', ['id' => $paymentMethod->id]);
    }

    public function edit($id)
    {
        $paymentMethod = PaymentMethod::findOrFail($id);

        return view('payment-method.edit', [
            'paymentMethod' => $paymentMethod
        ]);
    }

    public function update(Request $request, $id)
    {
        $paymentMethod = PaymentMethod::findOrFail($id);
        $paymentMethod->fill($request->all());

        if ($paymentMethod->save()) {
            Session::flash('success', 'Запись сохранена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('payment-methods.edit', ['id' => $paymentMethod->id]);
    }

    public function destroy(Request $request, $id)
    {
        $paymentMethod = PaymentMethod::findOrFail($id);
        $paymentMethod->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
