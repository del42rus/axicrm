<?php

namespace App\Http\Controllers;

use App\BitrixEdition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BitrixEditionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $editions = BitrixEdition::orderBy('id', 'asc')->get();

        return view('bitrix-edition.index', [
            'editions' => $editions
        ]);
    }

    public function create()
    {
        return view('bitrix-edition.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $edition = new BitrixEdition($request->all());

        if ($edition->save()) {
            Session::flash('success', 'Редакция CMS успешно сохранена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('bitrix-editions.edit', ['id' => $edition->id]);
    }

    public function edit($id)
    {
        $edition = BitrixEdition::findOrFail($id);

        return view('bitrix-edition.edit', [
            'edition' => $edition
        ]);
    }

    public function update(Request $request, $id)
    {
        $edition = BitrixEdition::findOrFail($id);
        $edition->fill($request->all());

        if ($edition->save()) {
            Session::flash('success', 'Редакция CMS успешно изменена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('bitrix-editions.edit', ['id' => $edition->id]);
    }

    public function destroy(Request $request, $id)
    {
        $edition = BitrixEdition::findOrFail($id);
        $edition->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
