<?php

namespace App\Http\Controllers;

use App\ContractFile;
use App\HostingContract as Contract;
use App\Contractcontract;
use App\Http\Requests\StoreHostingContract as StoreContractRequest;
use App\Http\Requests\CancelContract as CancelContractRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class HostingContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-hosting-contracts', ['only' => ['index']]);
        $this->middleware('permission:create-hosting-contracts', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-hosting-contracts', ['only' => ['edit', 'update']]);
        $this->middleware('permission:soft-delete-hosting-contracts', ['only' => ['destroy']]);
        $this->middleware('permission:restore-hosting-contracts', ['only' => ['restore']]);
        $this->middleware('permission:delete-hosting-contracts', ['only' => ['forceDelete']]);
        $this->middleware('permission:decline-hosting-contracts', ['only' => ['decline']]);
        $this->middleware('permission:activate-hosting-contracts', ['only' => ['activate']]);
    }

    public function index(Request $request, $status = 'active')
    {
        $query = Contract::select('hosting_contracts.*')
            ->join('contracts', function ($join) {
                $join->on('hosting_contracts.contract_id', '=', 'contracts.id')
                    ->where('contracts.contract_type', '=', 'hosting');
            });

        if ($status == 'deleted') {
            $query->onlyTrashed();
        } else {
            $query->where('hosting_contracts.status', (int) ($status == 'active'));
        }

        if ($request->responsible_person_id) {
            $query->where('hosting_contracts.responsible_person_id', $request->responsible_person_id);
        }

        if ($request->contract_number) {
            $query->where('contracts.contract_number', 'like', '%' . $request->contract_number . '%');
        }

        if ($request->brand) {
            $query->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
                ->where('clients.brand', $request->brand);
        }

        if ($request->client_id) {
            $query->where('contracts.client_id', $request->client_id);
        }

        return view('hosting-contract.index', [
            'contracts' => $query->paginate(20)->appends(Input::except('page')),
            'status' => $status,
        ]);
    }

    public function create(Request $request)
    {
        $contract = \App\Contract::where('id', $request->contract_id)
            ->where('contract_type', 'hosting')
            ->firstOrFail();

        if (Contract::where('contract_id', $request->contract_id)->count()) {
            return redirect('/');
        }

        return view('hosting-contract.create', [
            'contract' => $contract
        ]);
    }

    public function store(StoreContractRequest $request)
    {
        \App\Contract::where('id', $request->contract_id)
            ->where('contract_type', 'hosting')
            ->firstOrFail();

        $contract = new Contract($request->except(['scanned_copy', 'files']));

        if ($request->hasFile('scanned_copy') && $request->file('scanned_copy')->isValid()) {
            $contract->scanned_copy = $request->scanned_copy->store('files', 'public');
            $contract->status = 1;
        } else {
            $contract->status = 3;
        }

        $contract->save();

        if ($request->has('files')) {
            foreach ($request->all()['files'] as $id => $file) {
                if (!isset($file['file'])) {
                    continue;
                }

                $contractFile = new ContractFile();
                $contractFile->filename = $file['file']->store('files', 'public');
                $contractFile->name = $file['name'];
                $contractFile->contract_id = $contract->id;
                $contractFile->contract_type = 'hosting';

                $contractFile->save();
            }
        }

        Session::flash('success', 'Запись успешно сохранена');

        return redirect()->route('contracts.hosting.edit', ['id' => $contract->id]);
    }

    public function edit(Contract $contract)
    {
        return view('hosting-contract.edit', [
            'contract' => $contract
        ]);
    }

    public function show($contract)
    {
        return view('hosting-contract.show', [
            'contract' =>  Contract::withTrashed()->findOrFail($contract)
        ]);
    }

    public function update(StoreContractRequest $request, $id)
    {
        $contract = Contract::findOrFail($id);
        $contract->fill($request->except('id', 'scanned_copy', 'files'));

        if ($request->hasFile('scanned_copy') && $request->file('scanned_copy')->isValid()) {
            if ($contract->scanned_copy) {
                Storage::delete('public/' . $contract->scanned_copy);
            }

            $contract->scanned_copy = $request->scanned_copy->store('files', 'public');
            $contract->status = 1;
        }

        $contract->save();

        if ($request->has('files')) {
            foreach ($request->all()['files'] as $id => $file) {
                if (!isset($file['file'])) {
                    continue;
                }

                if (strpos($id, 'id_') !== false) {
                    $contractFile = ContractFile::find(substr($id, 3));
                    Storage::delete('public/' . $contractFile->filename);

                    $contractFile->filename = $file['file']->store('files', 'public');
                    $contractFile->name = $file['name'];
                } else {
                    $contractFile = new ContractFile();
                    $contractFile->filename = $file['file']->store('files', 'public');
                    $contractFile->name = $file['name'];
                    $contractFile->contract_id = $contract->id;
                    $contractFile->contract_type = 'hosting';
                }

                $contractFile->save();
            }
        }

        Session::flash('success', 'Запись успешно изменена');

        return redirect()->route('contracts.hosting.edit', ['id' => $contract->id]);
    }

    public function destroy(Contract $contract)
    {
        $contract->delete();

        return redirect()->back();
    }

    public function forceDelete($contract)
    {
        $contract = Contract::withTrashed()->where('id', $contract)->first();
        $contract->forceDelete();

        return redirect()->back();
    }

    public function restore($contract)
    {
        $contract = Contract::withTrashed()->where('id', $contract)->first();
        $contract->restore();

        return redirect()->back();
    }

    public function download($contract)
    {
        $contract = Contract::withTrashed()->findOrFail($contract);

        return response()->download(storage_path('app/public/' . $contract->scanned_copy), $contract->contract->contract_number . '.' . File::extension(storage_path('app/public/' . $contract->scanned_copy)));
    }

    public function downloadFile(Contract $contract, ContractFile $file)
    {
        if ($contract->id !== $file->contract->id) {
            abort(404);
        }

        return response()->download(storage_path('app/public/' . $file->filename), $file->name . '.' . File::extension(storage_path('app/public/' . $file->filename)));
    }

    public function deleteFile(Contract $contract, ContractFile $file)
    {
        if ($contract->id !== $file->contract->id) {
            abort(404);
        }

        $file->delete();
        Storage::delete('public/' . $file->filename);

        return redirect()->back();
    }

    public function decline(CancelContractRequest $request, Contract $contract)
    {
        if ($request->hasFile('cancellation_letter') && $request->file('cancellation_letter')->isValid()) {
            if ($contract->cancellation_letter) {
                Storage::delete('public/' . $contract->cancellation_letter);
            }

            $contract->cancellation_letter = $request->cancellation_letter->store('files', 'public');
        }

        if ($request->hasFile('cancellation_agreement') && $request->file('cancellation_agreement')->isValid()) {
            if ($contract->cancellation_agreement) {
                Storage::delete('public/' . $contract->cancellation_agreement);
            }

            $contract->cancellation_agreement = $request->cancellation_agreement->store('files', 'public');
        }

        $contract->cancellation_date = $request->post('cancellation_date');
        $contract->status = 0;
        $contract->save();

        return redirect()->back();
    }

    public function activate(Contract $contract)
    {
        if ($contract->cancellation_letter) {
            Storage::delete('public/' . $contract->cancellation_letter);
        }

        if ($contract->cancellation_agreement) {
            Storage::delete('public/' . $contract->cancellation_agreement);
        }

        $contract->cancellation_date = null;
        $contract->cancellation_letter = null;
        $contract->cancellation_agreement = null;
        $contract->status = 1;
        $contract->save();

        return redirect()->back();
    }

    public function downloadCancellationLetter($contract)
    {
        $contract = Contract::withTrashed()->findOrFail($contract);

        if (!$contract->cancellation_letter || !File::exists(storage_path('app/public/' . $contract->cancellation_letter))) {
            abort(404);
        }

        return response()->download(storage_path('app/public/' . $contract->cancellation_letter), 'Письмо о рассторжении договора ' . $contract->contract->contract_number . '.' . File::extension(storage_path('app/public/' . $contract->cancellation_letter)));
    }

    public function downloadCancellationAgreement($contract)
    {
        $contract = Contract::withTrashed()->findOrFail($contract);

        if (!$contract->cancellation_agreement || !File::exists(storage_path('app/public/' . $contract->cancellation_agreement))) {
            abort(404);
        }

        return response()->download(storage_path('app/public/' . $contract->cancellation_agreement), 'Дополнительное соглашение о рассторжении договора ' . $contract->contract->contract_number . '.' . File::extension(storage_path('app/public/' . $contract->cancellation_agreement)));
    }
}