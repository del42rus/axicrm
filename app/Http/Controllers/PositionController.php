<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $positions = Position::orderBy('id', 'asc')->get();

        return view('position.index', [
            'positions' => $positions
        ]);
    }

    public function create()
    {
        return view('position.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $position = new Position($request->all());

        if ($position->save()) {
            Session::flash('success', __('Position has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('positions.edit', ['id' => $position->id]);
    }

    public function edit($id)
    {
        $position = Position::findOrFail($id);

        return view('position.edit', [
            'position' => $position
        ]);
    }

    public function update(Request $request, $id)
    {
        $position = Position::findOrFail($id);
        $position->fill($request->all());

        if ($position->save()) {
            Session::flash('success', __('Position has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('positions.edit', ['id' => $position->id]);
    }

    public function destroy(Request $request, $id)
    {
        $position = Position::findOrFail($id);
        $position->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
