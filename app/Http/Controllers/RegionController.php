<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RegionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $regions = Region::orderBy('id', 'asc')->get();

        return view('region.index', [
            'regions' => $regions
        ]);
    }

    public function create()
    {
        return view('region.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $region = new Region($request->all());

        if ($region->save()) {
            Session::flash('success', __('Region has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('regions.edit', ['id' => $region->id]);
    }

    public function edit($id)
    {
        $region = Region::findOrFail($id);

        return view('region.edit', [
            'region' => $region
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        
        $region = Region::findOrFail($id);
        $region->fill($request->all());

        if ($region->save()) {
            Session::flash('success', __('Region has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('regions.edit', ['id' => $region->id]);
    }

    public function destroy(Request $request, $id)
    {
        $region = Region::findOrFail($id);
        $region->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
