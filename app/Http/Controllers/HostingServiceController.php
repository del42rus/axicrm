<?php

namespace App\Http\Controllers;

use App\HostingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HostingServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $services = HostingService::orderBy('id', 'asc')->get();

        return view('hosting-service.index', [
            'services' => $services
        ]);
    }

    public function create()
    {
        return view('hosting-service.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $service = new HostingService($request->all());

        if ($service->save()) {
            Session::flash('success', __('Service has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('hosting-services.edit', ['id' => $service->id]);
    }

    public function edit($id)
    {
        $service = HostingService::findOrFail($id);

        return view('hosting-service.edit', [
            'service' => $service
        ]);
    }

    public function update(Request $request, $id)
    {
        $service = HostingService::findOrFail($id);
        $service->fill($request->all());

        if ($service->save()) {
            Session::flash('success', __('Service has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('hosting-services.edit', ['id' => $service->id]);
    }

    public function destroy(Request $request, $id)
    {
        $service = HostingService::findOrFail($id);
        $service->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
