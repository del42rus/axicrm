<?php

namespace App\Http\Controllers;

use App\Client;
use App\Contact;
use App\Http\Requests\StoreContact as StoreContactRequest;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-contacts', ['only' => ['clients', 'suppliers']]);
        $this->middleware('permission:create-contacts', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-contacts', ['only' => ['edit', 'update']]);
        $this->middleware('permission:soft-delete-contacts', ['only' => ['destroy']]);
        $this->middleware('permission:restore-contacts', ['only' => ['restore']]);
        $this->middleware('permission:delete-contacts', ['only' => ['forceDelete']]);
        $this->middleware('permission:export-contacts', ['only' => ['downloadClientsContacts', 'downloadSuppliersContacts']]);
    }
    
    public function index()
    {
        return redirect()->route('contacts.clients');
    }

    public function clients(Request $request)
    {
        if ($request->removed) {
            $query = Contact::onlyTrashed()->select('contacts.*');
        } else {
            $query = Contact::select('contacts.*');
        }

        $query->where('contactable_type', 'client');

        if ($request->name) {
            $query->where('contacts.name', 'like',  '%' . $request->name . '%');
        }

        if ($request->email) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->phone) {
            $query->where('phone', $request->phone);
        }

        if ($request->client_id) {
            $query->where('contactable_id', $request->client_id);
        }

        if ($request->sort == 'client' || $request->sort == '-client' || $request->activity_id || $request->brand) {
            $query->leftJoin('clients', 'clients.id', '=', 'contacts.contactable_id');

            if ($request->sort == 'client') {
                $query->orderBy('clients.legal_name', 'asc');
            }

            if ($request->sort == '-client') {
                $query->orderBy('clients.legal_name', 'desc');
            }

            if ($request->activity_id) {
                $query->where('clients.activity_id', $request->activity_id);
            }

            if ($request->brand) {
                $query->where('clients.brand', $request->brand);
            }
        }

        return view('contact.clients', [
            'contacts' => $query->paginate(20)->appends(Input::except('page'))
        ]);
    }

    public function suppliers(Request $request)
    {
        if ($request->removed) {
            $query = Contact::onlyTrashed()->select('contacts.*');
        } else {
            $query = Contact::select('contacts.*');
        }

        $query = $query->where('contactable_type', 'supplier');

        if ($request->name) {
            $query->where('contacts.name', 'like',  '%' . $request->name . '%');
        }

        if ($request->email) {
            $query->where('email', 'like', '%' . $request->email . '%');
        }

        if ($request->phone) {
            $query->where('phone', $request->phone);
        }

        if ($request->supplier_id) {
            $query->where('contactable_id', $request->supplier_id);
        }

        if ($request->sort == 'supplier' || $request->sort == '-supplier' || $request->activity_id || $request->brand) {
            $query->leftJoin('suppliers', 'suppliers.id', '=', 'contacts.contactable_id');

            if ($request->sort == 'supplier') {
                $query->orderBy('suppliers.legal_name', 'asc');
            }

            if ($request->sort == '-supplier') {
                $query->orderBy('suppliers.legal_name', 'desc');
            }

            if ($request->activity_id) {
                $query->where('suppliers.activity_id', $request->activity_id);
            }

            if ($request->brand) {
                $query->where('suppliers.brand', $request->brand);
            }
        }

        return view('contact.suppliers', [
            'contacts' => $query->paginate(20)->appends(Input::except('page'))
        ]);
    }

    public function create(Request $request)
    {
        if (!in_array($request->contactable, ['client', 'supplier']) &&
            !(($request->client_id && Client::find($request->client_id)) || ($request->supplier_id && Client::find($request->supplier_id)))
        ) {
            abort(404);
        }

        return view('contact.create');
    }

    public function store(StoreContactRequest $request)
    {
        $data = $request->all();

        if ($request->client_id) {
            $data['contactable_id'] = $request->client_id;
            $data['contactable_type'] = 'client';
        } elseif ($request->supplier_id) {
            $data['contactable_id'] = $request->supplier_id;
            $data['contactable_type'] = 'supplier';
        }

        $contact = new Contact($data);

        if ($contact->save()) {
            Session::flash('success', __('Contact has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('contacts.edit', ['id' => $contact->id]);
    }

    public function edit(Contact $contact)
    {
        return view('contact.edit', [
            'contact' => $contact
        ]);
    }

    public function update(StoreContactRequest $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->fill($request->all());

        if ($contact->save()) {
            Session::flash('success', __('Contact has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('contacts.edit', ['id' => $contact->id]);
    }

    public function destroy(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return redirect()->to($request->get('backUrl'));
    }

    public function forceDelete($id)
    {
        $contact = Contact::withTrashed()->where('id', $id)->first();
        $contact->forceDelete();

        return redirect()->back();
    }

    public function restore($id)
    {
        $contact = Contact::withTrashed()->where('id', $id)->first();
        $contact->restore();

        return redirect()->back();
    }

    public function downloadClientsContacts(Request $request)
    {
        return Excel::create(__('Clients') . ' - ' . __('Contacts'), function($excel) use ($request) {
            $excel->sheet('Excel sheet', function($sheet) use ($request) {
                $sheet->setCellValue('A1', __('Full Name'));
                $sheet->setCellValue('B1', __('Email'));
                $sheet->setCellValue('C1', __('Phone'));
                $sheet->setCellValue('D1', __('Position'));
                $sheet->setCellValue('E1', __('Client'));

                $query = Contact::select('contacts.*')->where('contactable_type', 'client');

                if ($request->name) {
                    $query->where('contacts.name', 'like',  '%' . $request->name . '%');
                }

                if ($request->email) {
                    $query->where('email', $request->email);
                }

                if ($request->phone) {
                    $query->where('phone', $request->phone);
                }

                if ($request->client_id) {
                    $query->where('contactable_id', $request->client_id);
                }

                if ($request->sort == 'client' || $request->sort == '-client') {
                    $query->leftJoin('clients', 'clients.id', '=', 'contacts.contactable_id');

                    if ($request->sort == 'client') {
                        $query->orderBy('clients.legal_name', 'asc');
                    }

                    if ($request->sort == '-client') {
                        $query->orderBy('clients.legal_name', 'desc');
                    }
                }

                $contacts = $query->get();

                $rowNumber = 2;

                foreach ($contacts as $contact) {
                    $sheet->setCellValue('A' . $rowNumber, $contact->name);
                    $sheet->setCellValue('B' . $rowNumber, $contact->email);
                    $sheet->setCellValue('C' . $rowNumber, $contact->phone);
                    $sheet->setCellValue('D' . $rowNumber, $contact->position ? $contact->position->name : '');
                    $sheet->setCellValue('E' . $rowNumber, $contact->contactable->legal_name . ', ' . $contact->contactable->ownershipType->name);

                    $rowNumber++;
                }

            });
        })->export('xlsx');
    }

    public function downloadSuppliersContacts(Request $request)
    {
        return Excel::create(__('Suppliers') . ' - ' . __('Contacts'), function($excel) use ($request) {
            $excel->sheet('Excel sheet', function($sheet) use ($request) {
                $sheet->setCellValue('A1', __('Full Name'));
                $sheet->setCellValue('B1', __('Email'));
                $sheet->setCellValue('C1', __('Phone'));
                $sheet->setCellValue('D1', __('Position'));
                $sheet->setCellValue('E1', __('Client'));

                $query = Contact::select('contacts.*')->where('contactable_type', 'supplier');

                if ($request->name) {
                    $query->where('contacts.name', 'like',  '%' . $request->name . '%');
                }

                if ($request->email) {
                    $query->where('email', $request->email);
                }

                if ($request->phone) {
                    $query->where('phone', $request->phone);
                }

                if ($request->supplier_id) {
                    $query->where('contactable_id', $request->supplier_id);
                }

                if ($request->sort == 'supplier' || $request->sort == '-supplier') {
                    $query->leftJoin('suppliers', 'suppliers.id', '=', 'contacts.contactable_id');

                    if ($request->sort == 'supplier') {
                        $query->orderBy('suppliers.legal_name', 'asc');
                    }

                    if ($request->sort == '-supplier') {
                        $query->orderBy('suppliers.legal_name', 'desc');
                    }
                }

                $contacts = $query->get();

                $rowNumber = 2;

                foreach ($contacts as $contact) {
                    $sheet->setCellValue('A' . $rowNumber, $contact->name);
                    $sheet->setCellValue('B' . $rowNumber, $contact->email);
                    $sheet->setCellValue('C' . $rowNumber, $contact->phone);
                    $sheet->setCellValue('D' . $rowNumber, $contact->position ? $contact->position->name : '');
                    $sheet->setCellValue('E' . $rowNumber, $contact->contactable->legal_name . ', ' . $contact->contactable->ownershipType->name);

                    $rowNumber++;
                }

            });
        })->export('xlsx');
    }
}
