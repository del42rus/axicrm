<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\Http\Requests\StoreRole as StoreRoleRequest;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-acl', ['only' => ['index']]);
        $this->middleware('permission:create-acl', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-acl', ['only' => ['edit', 'update']]);
    }

    public function index()
    {
        $roles = Role::all();

        return view('role.index', [
            'roles' => $roles
        ]);
    }

    public function create()
    {
        $permissions = Permission::all();

        $groupedPermissions = [];

        foreach ($permissions as $permission) {
            $groupedPermissions[$permission->group ?: __('Others')][] = $permission;
        }

        ksort($groupedPermissions);

        return view('role.create',[
            'groupedPermissions' => $groupedPermissions
        ]);
    }

    public function store(StoreRoleRequest $request)
    {
        $role = new Role();
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;

        if ($role->save()) {
            Session::flash('success', __('Role has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        if ($request->permissions) {
            $role->syncPermissions($request->permissions);
        }

        return redirect()->route('roles.edit', ['id' => $role->id]);
    }

    public function edit($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        $permissions = Permission::all();

        $groupedPermissions = [];

        foreach ($permissions as $permission) {
            $groupedPermissions[$permission->group ?: __('Others')][] = $permission;
        }

        ksort($groupedPermissions);

        return view('role.edit',[
            'role' => $role,
            'groupedPermissions' => $groupedPermissions
        ]);
    }

    public function update(StoreRoleRequest $request, $id)
    {
        $role = Role::findOrFail($id);

        $role->display_name = $request->display_name;
        $role->description = $request->description;

        if ($role->save()) {
            Session::flash('success', __('Role has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        if ($request->permissions) {
            $role->syncPermissions($request->permissions);
        }

        return redirect()->route('roles.edit', ['id' => $role->id]);
    }
}
