<?php

namespace App\Http\Controllers;

use App\BitrixCRMEdition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BitrixCRMEditionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $editions = BitrixCRMEdition::orderBy('id', 'asc')->get();

        return view('bitrix-crm-edition.index', [
            'editions' => $editions
        ]);
    }

    public function create()
    {
        return view('bitrix-crm-edition.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $edition = new BitrixCRMEdition($request->all());

        if ($edition->save()) {
            Session::flash('success', 'Редакция CRM успешно сохранена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('bitrix-crm-editions.edit', ['id' => $edition->id]);
    }

    public function edit($id)
    {
        $edition = BitrixCRMEdition::findOrFail($id);

        return view('bitrix-crm-edition.edit', [
            'edition' => $edition
        ]);
    }

    public function update(Request $request, $id)
    {
        $edition = BitrixCRMEdition::findOrFail($id);
        $edition->fill($request->all());

        if ($edition->save()) {
            Session::flash('success', 'Редакция CRM успешно изменена');
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('bitrix-crm-editions.edit', ['id' => $edition->id]);
    }

    public function destroy(Request $request, $id)
    {
        $edition = BitrixCRMEdition::findOrFail($id);
        $edition->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
