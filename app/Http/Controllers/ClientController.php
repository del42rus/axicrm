<?php

namespace App\Http\Controllers;

use App\ActualAddress;
use App\AdmContract;
use App\AppContract;
use App\CAContract;
use App\CIContract;
use App\Client;
use App\HostingContract;
use App\Http\Requests\StoreClient as StoreClientRequest;
use App\LegalAddress;
use App\SeoContract;
use App\SMMContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-clients', ['only' => ['index']]);
        $this->middleware('permission:create-clients', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-clients', ['only' => ['edit', 'update']]);
        $this->middleware('permission:soft-delete-clients', ['only' => ['destroy']]);
        $this->middleware('permission:restore-clients', ['only' => ['restore']]);
        $this->middleware('permission:delete-clients', ['only' => ['forceDelete']]);
        $this->middleware('permission:export-clients', ['only' => ['download']]);
    }

    public function index(Request $request)
    {
        if ($request->removed == 1) {
            $query = Client::onlyTrashed()
                ->select('clients.*');
        } else {
            $query = Client::select('clients.*');
        }

        if ($request->legal_name) {
            $query->where('legal_name', $request->legal_name);
        }

        if ($request->brand) {
            $query->where('brand', $request->brand);
        }

        if ($request->site) {
            $query->where('site', 'like', '%' . $request->site . '%');
        }

        if ($request->activity_id) {
            $query->where('activity_id', $request->activity_id);
        }

        if ($request->sort == 'client' || $request->sort == '-client') {
            if ($request->sort == 'client') {
                $query->orderBy('legal_name', 'asc');
            }

            if ($request->sort == '-client') {
                $query->orderBy('legal_name', 'desc');
            }
        }

        if ($request->marketer_id || $request->salesman_id || $request->contract_status != '') {
            $query->join('contracts', 'clients.id', '=', 'contracts.client_id');

            if ($request->marketer_id) {
                $query->where('contracts.marketer_id', $request->marketer_id);
            }

            if ($request->salesman_id) {
                $query->where('contracts.salesman_id', $request->salesman_id);
            }

            if ($request->contract_status != '') {
                $query->where('contracts.status', $request->contract_status);
            }
        }

        return view('client.index', [
            'clients' =>  $query->paginate(20)->appends(Input::except('page')),
        ]);
    }

    public function show(Client $client)
    {
        return view('client.show', [
            'client' => $client
        ]);
    }

    public function create()
    {
        return view('client.create');
    }

    public function store(StoreClientRequest $request)
    {
        $client = new Client($request->all());

        if ($client->save()) {
            $actualAddress = new ActualAddress();
            $actualAddress->city_id = $request->actual_address['city_id'];
            $actualAddress->full_address = $request->actual_address['full_address'];
            $actualAddress->agent_id = $client->id;
            $actualAddress->agent_type = 'client';
            $actualAddress->save();

            $legalAddress = new LegalAddress();
            $legalAddress->city_id = $request->legal_address['city_id'];
            $legalAddress->full_address = $request->legal_address['full_address'];
            $legalAddress->agent_id = $client->id;
            $legalAddress->agent_type = 'client';
            $legalAddress->save();

            Session::flash('success', __('Client has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('clients.edit', ['id' => $client->id]);
    }

    public function edit(Client $client)
    {
        return view('client.edit', [
            'client' => $client
        ]);
    }

    public function update(StoreClientRequest $request, $id)
    {
        $client = Client::findOrFail($id);
        $client->fill($request->all());

        if ($client->save()) {
            $actualAddress = $client->actualAddress ?: new ActualAddress();
            $actualAddress->city_id = $request->actual_address['city_id'];
            $actualAddress->full_address = $request->actual_address['full_address'];
            $actualAddress->agent_id = $client->id;
            $actualAddress->agent_type = 'client';
            $actualAddress->save();

            $legalAddress = $client->legalAddress ?: new LegalAddress();
            $legalAddress->city_id = $request->legal_address['city_id'];
            $legalAddress->full_address = $request->legal_address['full_address'];
            $legalAddress->agent_id = $client->id;
            $legalAddress->agent_type = 'client';
            $legalAddress->save();

            Session::flash('success', __('Client has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('clients.edit', ['id' => $client->id]);
    }

    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        $client->delete();

        return redirect()->route('clients.index');
    }

    public function forceDelete($id)
    {
        $client = Client::withTrashed()->where('id', $id)->first();
        $client->forceDelete();

        return redirect()->back();
    }

    public function restore($id)
    {
        $client = Client::withTrashed()->where('id', $id)->first();
        $client->restore();

        return redirect()->back();
    }

    public function contacts(Client $client)
    {
        return view('client.contacts', [
            'client' => $client,
            'contacts' => $client->contacts
        ]);
    }

    public function download()
    {
        return Excel::create(__('Clients'), function($excel) {
            $excel->sheet('Excel sheet', function($sheet) {
                $sheet->setCellValue('A1', 'Юридическое название');
                $sheet->setCellValue('B1', 'Бренд');
                $sheet->setCellValue('C1', 'Вид деятельности');
                $sheet->setCellValue('D1', 'Сайт');
                $sheet->setCellValue('E1', 'Договоры');

                $clients = Client::all();

                $rowNumber = 2;

                foreach ($clients as $client) {
                    $sheet->setCellValue('A' . $rowNumber, $client->legal_name . ', ' . $client->ownershipType->name);
                    $sheet->setCellValue('B' . $rowNumber, $client->brand);
                    $sheet->setCellValue('C' . $rowNumber, $client->activity ? $client->activity->name : '');
                    $sheet->setCellValue('D' . $rowNumber, $client->site);

                    $contracts = '';

                    if ($client->caContracts()->count()) {
                        $contracts = 'Контекстная реклама: ';

                        foreach ($client->caContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->caContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->seoContracts()->count()) {
                        $contracts .= 'SEO: ';

                        foreach ($client->seoContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->seoContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->smmContracts()->count()) {
                        $contracts .= 'SMM: ';

                        foreach ($client->smmContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->smmContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->appContracts()->count()) {
                        $contracts .= 'Сайты и Приложения: ';

                        foreach ($client->appContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->appContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->admContracts()->count()) {
                        $contracts .= 'Администрирование: ';

                        foreach ($client->admContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->admContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->ciContracts()->count()) {
                        $contracts .= 'Фирменный стиль: ';

                        foreach ($client->ciContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->ciContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->hostingContracts()->count()) {
                        $contracts .= 'Хостинг: ';

                        foreach ($client->hostingContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->hostingContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->bitrixContracts()->count()) {
                        $contracts .= 'CMS:1С-Битрикс: ';

                        foreach ($client->bitrixContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->bitrixContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->bitrixCrmContracts()->count()) {
                        $contracts .= 'CRM:Битрикс-24: ';

                        foreach ($client->bitrixCrmContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->bitrixCrmContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->amoCrmContracts()->count()) {
                        $contracts .= 'CRM:Amo.CRM: ';

                        foreach ($client->amoCrmContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->amoCrmContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->additionalContracts()->count()) {
                        $contracts .= 'Доп. работы ПО: ';

                        foreach ($client->additionalContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->additionalContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    if ($client->additionalDigitalContracts()->count()) {
                        $contracts .= 'Доп. работы ОАиП: ';

                        foreach ($client->additionalDigitalContracts() as $key => $contract) {
                            $contracts .= '№' . $contract->contract_number . ($contract->salesman ? '(' . $contract->salesman->name. ')' : '') . ($key == $client->additionalDigitalContracts()->count() - 1 ? PHP_EOL : ', ');
                        }
                    }

                    $sheet->setCellValue('E' . $rowNumber, $contracts);

                    $rowNumber++;
                }

            });
        })->export('xlsx');
    }
}