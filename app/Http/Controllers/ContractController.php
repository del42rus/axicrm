<?php

namespace App\Http\Controllers;

use App\AdditionalContract;
use App\AdditionalDigitalContract;
use App\AdmContract;
use App\AmoCRMContract;
use App\AppContract;
use App\BitrixContract;
use App\BitrixCRMContract;
use App\CAContract;
use App\CIContract;
use App\Contract;
use App\ContractType;
use App\HostingContract;
use App\SeoContract;
use App\SMMContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-contracts', ['only' => ['index']]);
        $this->middleware('permission:create-contracts', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-contracts', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-contracts', ['only' => ['destroy']]);
    }

    protected function getContractQuery(Request $request, $status = 'active')
    {
        $query = Contract::select('contracts.*');

        if ($request->marketer_id) {
            $query->where('marketer_id', $request->marketer_id);
        }

        if ($request->salesman_id) {
            $query->where('salesman_id', $request->salesman_id);
        }

        if ($request->responsible_person_id) {
            $query->where('responsible_person_id', $request->responsible_person_id);
        }

        switch ($status) {
            case 'new':
                $query->whereNull('status');
                break;
            case 'active' :
                $query->where('status', 1);
                break;
            case 'declined' :
                $query->where('status', 0);
                break;
            case 'completed' :
                $query->where('status', 2);
                break;
            case 'unsigned' :
                $query->where('status', 3);
                break;
        }

        $query->where(function($query) {
            $user = Auth::user();
            if ($user->can('read-app-contracts')) {
                $query->where('contracts.contract_type', 'app');
            }

            if ($user->can('read-adm-contracts')) {
                $query->orWhere('contracts.contract_type', 'adm');
            }

            if ($user->can('read-ca-contracts')) {
                $query->orWhere('contracts.contract_type', 'ca');
            }

            if ($user->can('read-ci-contracts')) {
                $query->orWhere('contracts.contract_type', 'ci');
            }

            if ($user->can('read-seo-contracts')) {
                $query->orWhere('contracts.contract_type', 'seo');
            }

            if ($user->can('read-smm-contracts')) {
                $query->orWhere('contracts.contract_type', 'smm');
            }

            if ($user->can('read-additional-contracts')) {
                $query->orWhere('contracts.contract_type', 'additional');
            }

            if ($user->can('read-additional-digital-contracts')) {
                $query->orWhere('contracts.contract_type', 'additional-digital');
            }

            if ($user->can('read-hosting-contracts')) {
                $query->orWhere('contracts.contract_type', 'hosting');
            }

            if ($user->can('read-bitrix-contracts')) {
                $query->orWhere('contracts.contract_type', 'bitrix');
            }

            if ($user->can('read-bitrix-crm-contracts')) {
                $query->orWhere('contracts.contract_type', 'bitrix-crm');
            }

            if ($user->can('read-amo-crm-contracts')) {
                $query->orWhere('contracts.contract_type', 'amo-crm');
            }
        });

        if ($request->brand) {
            $query->leftJoin('clients', 'clients.id', '=', 'contracts.client_id')
                ->where('clients.brand', $request->brand);
        }

        if ($request->client_id) {
            $query->where('contracts.client_id', $request->client_id);
        }

        if ($request->contract_number) {
            $query->where('contracts.contract_number', 'like', '%' . $request->contract_number . '%');
        }

        if ($request->contract_type) {
            $query->where('contracts.contract_type', $request->contract_type);
        }

        return $query;
    }

    public function index(Request $request, $status = 'active')
    {
        $query = $this->getContractQuery($request, $status);

        return view('contract.index', [
            'contracts' => $query->paginate(20)->appends(Input::except('page')),
            'status' => $status,
        ]);
    }

    public function create()
    {
        return view('contract.create');
    }

    public function store(Request $request)
    {
        if ($request->post('parent_id')) {
            $request->validate([
                'contract_type' => 'required',
                'number' => 'required|integer',
                'client_id' => 'required|exists:clients,id',
                'parent_id' => 'exists:contracts,id',
                'date' => 'required|date_format:d/m/Y'
            ]);
        } else {
            $request->validate([
                'contract_type' => 'required',
                'contract_number' => 'required',
                'client_id' => 'required|exists:clients,id',
                'parent_id' => 'nullable',
                'date' => 'required|date_format:d/m/Y'
            ]);
        }

        $contract = new Contract($request->all());

        if ($request->post('parent_id')) {
            $contract->parent_id = $request->post('parent_id');

            $parent = Contract::findOrFail($contract->parent_id);
            $contract->contract_number = $parent->contract_number . '-' . $request->post('number');
            $contract->additional_number = $request->post('number');
        }

        $contract->save();

        Session::flash('success', 'Запись успешно сохранена');

        return redirect()->route('contracts.edit', ['id' => $contract->id]);
    }

    public function edit($id)
    {
        $contract = Contract::findOrFail($id);

        return view('contract.edit', [
            'contract' => $contract
        ]);
    }

    public function update(Request $request, $id)
    {
        /** @var Contract $contract */
        $contract = Contract::findOrFail($id);

        if ($contract->isCorrectiveContract()) {
            $validator = Validator::make($request->all(), [
                'number' => 'required|integer',
                'date' => 'required|date_format:d/m/Y',
            ]);

            $validator->after(function ($validator) use($contract, $request) {
                $exists = Contract::where([
                    ['id', '<>', $contract->id],
                    ['parent_id', '=', $contract->parent->id],
                    ['additional_number', '=', $request->number],
                    ['contract_type', '<>', "additional"],
                    ['contract_type', '<>', "additional-digital"]]
                )->count();

                if ($exists) {
                    $validator->errors()->add('number', 'Доп. соглашение с таким номером уже существует.');
                }
            });

            $validator->validate();
        } else {
            if ($request->post('parent_id')) {
                $request->validate([
                    'contract_type' => 'required',
                    'number' => 'required|integer',
                    'client_id' => 'required|exists:clients,id',
                    'parent_id' => 'exists:contracts,id',
                    'date' => 'required|date_format:d/m/Y'
                ]);
            } else {
                $request->validate([
                    'contract_type' => 'required',
                    'contract_number' => 'required',
                    'client_id' => 'required|exists:clients,id',
                    'parent_id' => 'nullable',
                    'date' => 'required|date_format:d/m/Y'
                ]);
            }
        }

        if ($request->post('contract_type') && $request->post('contract_type') != $contract->contract_type) {
            $files = DB::table(str_replace('-', '_', $contract->contract_type) . '_contracts')
                ->where(['contract_id' => $contract->id])
                ->pluck('scanned_copy');

            foreach ($files as $file) {
                if (!$file) {
                    continue;
                }

                Storage::delete('public/' . $file);
            }

            DB::table(str_replace('-', '_', $contract->contract_type) . '_contracts')->where(['contract_id' => $contract->id])->delete();

            $contract->status = null;
            $contract->marketer_id = null;
            $contract->salesman_id = null;
            $contract->responsible_person_id = null;
        }

        $contract->fill($request->all());

        if ($request->post('parent_id')) {
            $contract->parent_id = $request->post('parent_id');

            $parent = Contract::findOrFail($contract->parent_id);
            $contract->contract_number = $parent->contract_number . '-' . $request->post('number');
            $contract->additional_number = $request->post('number');
        } elseif ($contract->isCorrectiveContract()) {
            $contract->contract_number = $contract->parent->contract_number . '-' . $request->post('number');
            $contract->additional_number = $request->post('number');
        } else {
            $contract->parent_id = null;
            $contract->additional_number = null;
        }

        $contract->save();

        Session::flash('success', 'Запись успешно изменена');

        return redirect()->route('contracts.edit', ['id' => $contract->id]);
    }

    public function destroy(Request $request, $id)
    {
        $contract = Contract::findOrFail($id);
        $contract->delete();

        return redirect()->to($request->get('backUrl'));
    }

    public function correct(Request $request, Contract $contract)
    {
        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'number' => 'required|integer',
                'date' => 'required|date_format:d/m/Y',
            ]);

            $validator->after(function ($validator) use($contract, $request) {
                if (Contract::where([['parent_id', '=', $contract->id], ['additional_number', '=', $request->number]])->count()) {
                    $validator->errors()->add('number', 'Доп. соглашение с таким номером уже существует.');
                }
            });

            if ($validator->fails()) {
                return redirect(url()->previous())
                    ->withErrors($validator)
                    ->withInput();
            }

            $correctiveContract = new Contract();
            $correctiveContract->parent_id = $contract->id;
            $correctiveContract->client_id = $contract->client_id;
            $correctiveContract->contract_type = $contract->contract_type;
            $correctiveContract->contract_number = $contract->contract_number . '-' . $request->number;
            $correctiveContract->additional_number = $request->number;
            $correctiveContract->date = $request->post('date');

            $correctiveContract->save();

            Session::flash('success', 'Запись успешно сохранена');

            return redirect()->route('contracts.index', ['status' => 'new']);
        }

        return view('contract.correct');
    }

    public function getClientContracts(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            abort(404);
        }

        $clientId = $request->post('client_id');

        if (!$clientId) {
            abort(404);
        }

        $query = Contract::where('client_id', $clientId);

        if ($request->post('id')) {
            $query->where('id', '<>', $request->post('id'));
        }

        $contracts = $query->whereNull('parent_id')
            ->get();

        $result = [];

        foreach ($contracts as $contract) {
            $result[$contract->id] = $contract->contract_number . ' (' . Contract::getTypeByAlias($contract->contract_type) . ')';
        }

        return response()->json($result);
    }

    public function download(Request $request, $status = 'active')
    {
        $query = $this->getContractQuery($request, $status);

        $contracts = $query->get();

        switch ($status) {
            case 'declined': $name = 'Отклоненные договора'; break;
            case 'completed': $name = 'Выполненные договора'; break;
            case 'new': $name = 'Неподписанные договора'; break;
            default: $name = 'Текущие договора';
        }

        return Excel::create($name, function($excel) use ($contracts, $status) {
            $excel->sheet('Excel sheet', function($sheet) use ($contracts, $status) {
                $sheet->setCellValue('A1', 'Номер договора');
                $sheet->setCellValue('B1', 'Дата');
                $sheet->setCellValue('C1', 'Номер папки');
                $sheet->setCellValue('D1', 'Тип договора');
                $sheet->setCellValue('E1', 'Юридическое название');
                $sheet->setCellValue('F1', 'Бренд');
                $sheet->setCellValue('G1', 'Интернет маркетолог');
                $sheet->setCellValue('H1', 'Продажник');
                $sheet->setCellValue('I1', 'Способ оплаты');

                if ($status == 'declined') {
                    $sheet->setCellValue('J1', 'Дата отказа');
                }

                $rowNumber = 2;

                foreach ($contracts as $contract) {
                    $sheet->setCellValue('A' . $rowNumber, $contract->contract_number);
                    $sheet->setCellValue('B' . $rowNumber, $contract->date);
                    $sheet->setCellValue('C' . $rowNumber, $contract->folder_number);
                    $sheet->setCellValue('D' . $rowNumber, Contract::getTypeByAlias($contract->contract_type));
                    $sheet->setCellValue('E' . $rowNumber, $contract->client->legal_name . ', ' . $contract->client->ownershipType->name);
                    $sheet->setCellValue('F' . $rowNumber, $contract->client->brand);
                    $sheet->setCellValue('G' . $rowNumber, $contract->contract && $contract->contract->marketer ? $contract->contract->marketer->name : '');
                    $sheet->setCellValue('H' . $rowNumber, $contract->contract && $contract->contract->salesman ? $contract->contract->salesman->name : '');
                    $sheet->setCellValue('I' . $rowNumber, $contract->contract && $contract->contract->paymentMethod ? $contract->contract->paymentMethod->name : '');

                    if ($contract->contract && $contract->contract->isDeclined()) {
                        $sheet->setCellValue('J' . $rowNumber, $contract->contract->cancellation_date );
                    }

                    $rowNumber++;
                }

            });
        })->export('xlsx');
    }
}
