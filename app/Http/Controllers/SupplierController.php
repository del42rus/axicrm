<?php

namespace App\Http\Controllers;

use App\ActualAddress;
use App\LegalAddress;
use App\Supplier;
use App\Http\Requests\StoreSupplier as StoreSupplierRequest;
use App\SupplierContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-suppliers', ['only' => ['index']]);
        $this->middleware('permission:create-suppliers', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-suppliers', ['only' => ['edit', 'update']]);
        $this->middleware('permission:soft-delete-suppliers', ['only' => ['destroy']]);
        $this->middleware('permission:restore-suppliers', ['only' => ['restore']]);
        $this->middleware('permission:delete-suppliers', ['only' => ['forceDelete']]);
        $this->middleware('permission:export-suppliers', ['only' => ['download']]);
    }

    public function index(Request $request)
    {
        if ($request->removed == 1) {
            $query = Supplier::onlyTrashed();
        } else {
            $query = Supplier::select();
        }

        if ($request->legal_name) {
            $query->where('legal_name', $request->legal_name);
        }

        if ($request->brand) {
            $query->where('brand', $request->brand);
        }

        if ($request->site) {
            $query->where('site', 'like', '%' . $request->site . '%');
        }

        if ($request->activity_id) {
            $query->where('activity_id', $request->activity_id);
        }
        
        return view('supplier.index', [
            'suppliers' =>  $query->paginate(20)->appends(Input::except('page')),
        ]);
    }

    public function show(Supplier $supplier)
    {
        return view('supplier.show', [
            'supplier' => $supplier
        ]);
    }

    public function create()
    {
        return view('supplier.create');
    }

    public function store(StoreSupplierRequest $request)
    {
        $supplier = new Supplier($request->all());

        if ($supplier->save()) {
            if ($request->hasFile('contract_scan') && $request->file('contract_scan')->isValid()) {
                $contract = new SupplierContract();
                $contract->contract_scan = $request->contract_scan->store('files', 'public');
                $contract->number = $request->contract_number;

                $contract->supplier()->associate($supplier);
                $contract->save();
            }

            $actualAddress = $supplier->actualAddress ?: new ActualAddress();
            $actualAddress->city_id = $request->actual_address['city_id'];
            $actualAddress->full_address = $request->actual_address['full_address'];
            $actualAddress->agent_id = $supplier->id;
            $actualAddress->agent_type = 'supplier';
            $actualAddress->save();

            $legalAddress = $supplier->legalAddress ?: new LegalAddress();
            $legalAddress->city_id = $request->legal_address['city_id'];
            $legalAddress->full_address = $request->legal_address['full_address'];
            $legalAddress->agent_id = $supplier->id;
            $legalAddress->agent_type = 'supplier';
            $legalAddress->save();

            Session::flash('success', __('Supplier has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('suppliers.edit', ['id' => $supplier->id]);
    }

    public function edit(Supplier $supplier)
    {
        return view('supplier.edit', [
            'supplier' => $supplier
        ]);
    }

    public function update(StoreSupplierRequest $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->fill($request->all());

        if ($supplier->save()) {
            if ($request->hasFile('contract_scan') && $request->file('contract_scan')->isValid()) {
                if ($supplier->contract->contract_scan) {
                    Storage::disk('public')->delete($supplier->contract->contract_scan);
                }

                $supplier->contract->contract_scan = $request->contract_scan->store('files', 'public');
                $supplier->contract->number = $request->contract_number;

                $supplier->contract->save();
            }

            $actualAddress = $supplier->actualAddress ?: new ActualAddress();
            $actualAddress->city_id = $request->actual_address['city_id'];
            $actualAddress->full_address = $request->actual_address['full_address'];
            $actualAddress->agent_id = $supplier->id;
            $actualAddress->agent_type = 'supplier';
            $actualAddress->save();

            $legalAddress = $supplier->legalAddress ?: new LegalAddress();
            $legalAddress->city_id = $request->legal_address['city_id'];
            $legalAddress->full_address = $request->legal_address['full_address'];
            $legalAddress->agent_id = $supplier->id;
            $legalAddress->agent_type = 'supplier';
            $legalAddress->save();

            Session::flash('success', __('Supplier has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('suppliers.edit', ['id' => $supplier->id]);
    }

    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();

        return redirect()->route('suppliers.index');
    }

    public function forceDelete($id)
    {
        $supplier = Supplier::withTrashed()->where('id', $id)->first();
        $supplier->forceDelete();

        return redirect()->back();
    }

    public function restore($id)
    {
        $supplier = Supplier::withTrashed()->where('id', $id)->first();
        $supplier->restore();

        return redirect()->back();
    }

    public function contacts(Supplier $supplier)
    {
        return view('supplier.contacts', [
            'supplier' => $supplier,
            'contacts' => $supplier->contacts
        ]);
    }

    public function download()
    {
        return Excel::create(__('Suppliers'), function($excel) {
            $excel->sheet('Excel sheet', function($sheet) {
                $sheet->setCellValue('A1', __('Legal Name'));
                $sheet->setCellValue('B1', __('Brand'));
                $sheet->setCellValue('C1', __('Kind of Activity'));
                $sheet->setCellValue('D1', __('Site'));

                $suppliers = Supplier::all();

                $rowNumber = 2;

                foreach ($suppliers as $supplier) {
                    $sheet->setCellValue('A' . $rowNumber, $supplier->legal_name . ', ' . $supplier->ownershipType->name);
                    $sheet->setCellValue('B' . $rowNumber, $supplier->brand);
                    $sheet->setCellValue('C' . $rowNumber, $supplier->activity ? $supplier->activity->name : '');
                    $sheet->setCellValue('D' . $rowNumber, $supplier->site);

                    $rowNumber++;
                }

            });
        })->export('xlsx');
    }

    public function downloadContract(Supplier $supplier)
    {
        return response()->download(storage_path('app/public/' . $supplier->contract->contract_scan));
    }
}