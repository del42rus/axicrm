<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfile as UpdateProfileRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:read-profile', ['only' => ['index']]);
    }

    public function index()
    {
        return view('profile.index', [
            'user' => Auth::user()
        ]);
    }

    public function update(UpdateProfileRequest $request)
    {
        $user = Auth::user();
        $user->fill($request->except(['id', 'password']));

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($user->save()) {
            Session::flash('success', __('Profile has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('profile.update');
    }
}
