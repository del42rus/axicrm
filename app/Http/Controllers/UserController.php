<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Http\Requests\StoreUser as StoreUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-users', ['only' => ['index']]);
        $this->middleware('permission:create-users', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-users', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-users', ['only' => ['destroy']]);
    }
    
    public function index()
    {
        $users = User::paginate(20);

        return view('user.index', [
            'users' => $users
        ]);
    }

    public function create()
    {
        $roles = Role::all();

        return view('user.create', [
            'roles' => $roles
        ]);
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User($request->except('password'));

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($user->save()) {
            Session::flash('success', __('User has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        if ($request->roles) {
            $user->syncRoles($request->roles);
        }

        return redirect()->route('users.edit', ['id' => $user->id]);
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->with('roles')->first();
        $roles = Role::all();

        return view('user.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(StoreUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->except(['id', 'password']));

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($user->save()) {
            Session::flash('success', __('User has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        if ($request->roles) {
            $user->syncRoles($request->roles);
        }

        return redirect()->route('users.edit', ['id' => $user->id]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index');
    }
}
