<?php

namespace App\Http\Controllers;

use App\OwnershipType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OwnershipTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $types = OwnershipType::orderBy('id', 'asc')->get();

        return view('ownership-type.index', [
            'types' => $types
        ]);
    }

    public function create()
    {
        return view('ownership-type.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $type = new OwnershipType($request->all());

        if ($type->save()) {
            Session::flash('success', __('Ownership type has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('ownership-types.edit', ['id' => $type->id]);
    }

    public function edit($id)
    {
        $type = OwnershipType::findOrFail($id);

        return view('ownership-type.edit', [
            'type' => $type
        ]);
    }

    public function update(Request $request, $id)
    {
        $type = OwnershipType::findOrFail($id);
        $type->fill($request->all());

        if ($type->save()) {
            Session::flash('success', __('Ownership type has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('ownership-types.edit', ['id' => $type->id]);
    }

    public function destroy(Request $request, $id)
    {
        $type = OwnershipType::findOrFail($id);
        $type->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
