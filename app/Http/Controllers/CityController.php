<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        if ($request->region_id) {
            $cities = City::where('region_id', $request->region_id)->orderBy('id', 'asc')->get();
        } else {
            $cities = City::orderBy('id', 'asc')->get();
        }

        return view('city.index', [
            'cities' => $cities
        ]);
    }

    public function create()
    {
        return view('city.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'region_id' => 'required|exists:regions,id'
        ]);

        $city = new City($request->all());

        if ($city->save()) {
            Session::flash('success', __('City has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('cities.edit', ['id' => $city->id]);
    }

    public function edit($id)
    {
        $city = City::findOrFail($id);

        return view('city.edit', [
            'city' => $city
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'region_id' => 'required|exists:regions,id'
        ]);

        $city = City::findOrFail($id);
        $city->fill($request->all());

        if ($city->save()) {
            Session::flash('success', __('City has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('cities.edit', ['id' => $city->id]);
    }

    public function destroy(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
