<?php

namespace App\Http\Controllers;

use App\AppContractStage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AppContractStageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $stages = AppContractStage::all();

        return view('app-contract-stage.index', [
            'stages' => $stages
        ]);
    }

    public function create()
    {
        return view('app-contract-stage.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $stage = new AppContractStage($request->all());

        if ($stage->save()) {
            Session::flash('success', __('Stage has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('app-contract-stages.edit', ['id' => $stage->id]);
    }

    public function edit($id)
    {
        $stage = AppContractStage::findOrFail($id);

        return view('app-contract-stage.edit', [
            'stage' => $stage
        ]);
    }

    public function update(Request $request, $id)
    {
        $stage = AppContractStage::findOrFail($id);
        $stage->fill($request->all());

        if ($stage->save()) {
            Session::flash('success', __('Stage has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('app-contract-stages.edit', ['id' => $stage->id]);
    }

    public function destroy(Request $request, $id)
    {
        $stage = AppContractStage::findOrFail($id);
        $stage->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
