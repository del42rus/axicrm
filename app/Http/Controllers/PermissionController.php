<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-acl', ['only' => ['index']]);
        $this->middleware('permission:create-acl', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-acl', ['only' => ['edit', 'update']]);
    }

    public function index()
    {
        $permissions = Permission::all();

        $groupedPermissions = [];

        foreach ($permissions as $permission) {
            $groupedPermissions[$permission->group ?: __('Others')][] = $permission;
        }

        ksort($groupedPermissions);

        return view('permission.index', [
            'groupedPermissions' => $groupedPermissions
        ]);
    }

    public function create()
    {
        return view('permission.create');
    }

    public function store(Request $request)
    {
        if ($request->permission_type == 'basic') {
            $this->validate($request, [
                'display_name' => 'required|max:255',
                'name' => 'required|max:255|alphadash|unique:permissions,name',
                'description' => 'max:255'
            ]);

            $permission = new Permission();
            $permission->name = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description = $request->description;
            $permission->group = $request->group;
            $permission->save();

            return redirect()->route('permissions.index');
        } elseif ($request->permission_type == 'crud') {
            $this->validate($request, [
                'resource' => 'required|min:3|max:100|alpha'
            ]);

            if (count($request->operation) > 0) {
                foreach ($request->operation as $operation) {
                    $slug = strtolower($operation) . '-' . strtolower($request->resource);
                    $display_name = ucwords($operation . " " . $request->resource);
                    $description = "Allows a user to " . strtoupper($operation) . ' a ' . ucwords($request->resource);

                    $permission = new Permission();
                    $permission->name = $slug;
                    $permission->display_name = $display_name;
                    $permission->description = $description;
                    $permission->group = $request->group_crud;
                    $permission->save();
                }

                return redirect()->route('permissions.index');
            }
        } else {
            return redirect()->route('permissions.create')->withInput();
        }
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        return view('permission.edit', [
            'permission' => $permission
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'display_name' => 'required|max:255',
            'description' => 'max:255'
        ]);

        $permission = Permission::findOrFail($id);
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->group = $request->group;

        if ($permission->save()) {
            Session::flash('success', __('Permission has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('permissions.edit', ['id' => $permission->id]);
    }
}
