<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read-dictionaries', ['only' => ['index']]);
        $this->middleware('permission:create-dictionaries', ['only' => ['create', 'store']]);
        $this->middleware('permission:update-dictionaries', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-dictionaries', ['only' => ['destroy']]);
    }

    public function index()
    {
        $activities = Activity::orderBy('id', 'asc')->get();

        return view('activity.index', [
            'activities' => $activities
        ]);
    }

    public function create()
    {
        return view('activity.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $activity = new Activity($request->all());

        if ($activity->save()) {
            Session::flash('success', __('Activity has been saved successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('activities.edit', ['id' => $activity->id]);
    }

    public function edit($id)
    {
        $activity = Activity::findOrFail($id);

        return view('activity.edit', [
            'activity' => $activity
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $activity = Activity::findOrFail($id);
        $activity->fill($request->all());

        if ($activity->save()) {
            Session::flash('success', __('Activity has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('activities.edit', ['id' => $type->id]);
    }

    public function destroy(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);
        $activity->delete();

        return redirect()->to($request->get('backUrl'));
    }
}
