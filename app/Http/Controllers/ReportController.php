<?php

namespace App\Http\Controllers;

use App\Report;
use App\Service\ReportGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreReport as StoreReportRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    protected $reportGenerator;

    public function __construct(ReportGenerator $reportGenerator)
    {
        $this->reportGenerator = $reportGenerator;

        $this->middleware('permission:read-reports', ['only' => ['index']]);
        $this->middleware('permission:update-reports', ['only' => ['edit', 'update']]);
        $this->middleware('permission:update-reports-limited', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-reports', ['only' => ['destroy']]);
    }

    public function index(Request $request, $type)
    {
        $query = Report::select('reports.*')->where('reports.contract_type', $type);

        if ($request->marketer_id) {
            $query->where('reports.marketer_id', $request->marketer_id);
        }

        if ($request->responsible_person_id) {
            $query->where('reports.responsible_person_id', $request->responsible_person_id);
        }

        if ($request->year) {
            $query->where(DB::raw('YEAR(reports.created_at)'), $request->year);
        }

        if ($request->month) {
            $query->where(DB::raw('MONTH(reports.created_at)'), $request->month);
        }

        if ($request->contract_number || $request->client_id || $request->brand ||
            $request->sort == 'brand' || $request->sort == '-brand'
        ) {
            $query->leftJoin('contracts', function ($join) use ($type) {
                $join->on('contracts.contract_id', '=', 'reports.contract_id')
                    ->where('contracts.contract_type', '=', $type);
            });

            if ($request->contract_number) {
                $query->where('contracts.contract_number', 'like', '%' . $request->contract_number . '%');
            }

            if ($request->client_id) {
                $query->where('contracts.client_id', $request->client_id);
            }

            if ($request->brand || $request->sort == 'brand' || $request->sort == '-brand') {
                $query->leftJoin('clients', 'clients.id', '=', 'contracts.client_id');

                if ($request->brand) {
                    $query->where('clients.brand', $request->brand);
                }

                if ($request->sort == 'brand' || $request->sort == '-brand') {
                    if ($request->sort == 'brand') {
                        $query->orderBy('clients.brand', 'asc');
                    }

                    if ($request->sort == '-brand') {
                        $query->orderBy('clients.brand', 'desc');
                    }
                }
            }
        }

        if ($request->sort == 'year' || $request->sort == '-year') {
            if ($request->sort == 'year') {
                $query->orderBy(DB::raw('YEAR(reports.created_at)'), 'asc');
            }

            if ($request->sort == '-year') {
                $query->orderBy(DB::raw('YEAR(reports.created_at)'), 'desc');
            }
        }

        return view('report.index', ['reports' => $query->paginate(20)->appends(Input::except('page')), 'contractType' => $type]);
    }

    public function edit(Report $report)
    {
        return view('report.edit', [
            'report' => $report
        ]);
    }

    public function update(StoreReportRequest $request, $id)
    {
        $report = Report::findOrFail($id);
        if (!Auth::user()->hasPermission('update-reports') &&
            !($report->getYear() == date('Y') && $report->getMonth() == date('m') && $report->getDay() <= 15)
        ) {
            abort(403);
        }

        $report->fill($request->except('id', 'report_file', 'act_file'));

        if ($request->hasFile('report_file') && $request->file('report_file')->isValid()) {
            if ($report->report_file) {
                Storage::delete('public/' . $report->report_file);
            }
            $report->report_file = $request->report_file->store('files', 'public');
        }

        if ($request->hasFile('act_file') && $request->file('act_file')->isValid()) {
            if ($report->act_file) {
                Storage::delete('public/' . $report->act_file);
            }
            $report->act_file = $request->act_file->store('files', 'public');
        }

        if ($report->save()) {
            Session::flash('success', __('Report has been updated successfully'));
        } else {
            Session::flash('danger', __('An error occurred'));
        }

        return redirect()->route('reports.edit', ['id' => $report->id]);
    }

    public function destroy(Report $report)
    {
        $report->delete();

        return redirect()->back();
    }

    public function downloadReport(Report $report)
    {
        if (!$report->report_file) {
            abort(404);
        }

        return response()->download(storage_path('app/public/' . $report->report_file));
    }

    public function downloadAct(Report $report)
    {
        if (!$report->act_file) {
            abort(404);
        }

        return response()->download(storage_path('app/public/' . $report->act_file));
    }

    public function generate()
    {
        $this->reportGenerator->generateSeoContractReports();
        $this->reportGenerator->generateAdmContractReports();
        $this->reportGenerator->generateCaContractReports();
        $this->reportGenerator->generateSmmContractReports();
        return response('OK');
    }
}
