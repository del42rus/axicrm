<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'contactable_id', 'contactable_type', 'name', 'email', 'phone', 'position_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function contactable()
    {
        return $this->morphTo();
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }
}
