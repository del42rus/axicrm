let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.sass('resources/assets/sass/bootstrap.scss', 'public/css')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .copy('resources/assets/js/app.js', 'public/js')
    .copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js')
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js')
    .copy('node_modules/popper.js/dist/umd/popper.min.js', 'public/js/popper.min.js')
    .copy('node_modules/flatpickr/dist/flatpickr.min.js', 'public/js/flatpickr.min.js')
    .copy('node_modules/flatpickr/dist/l10n/ru.js', 'public/js/flatpickr/l10n/ru.js')
    .copy('node_modules/flatpickr/dist/flatpickr.min.css', 'public/css/flatpickr.min.css')
    .copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/css/font-awesome.min.css')
    .copy('node_modules/font-awesome/fonts', 'public/fonts');
