<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@index');

Route::middleware(['auth'])->group(function () {
    Route::resource('/users', 'UserController', ['except' => ['show']]);

    Route::resource('/contacts', 'ContactController', ['except' => ['show']]);
    Route::get('/contacts/clients', 'ContactController@clients')->name('contacts.clients');
    Route::get('/contacts/suppliers', 'ContactController@suppliers')->name('contacts.suppliers');
    Route::get('/contacts/clients/download', 'ContactController@downloadClientsContacts')->name('contacts.clients.download');
    Route::get('/contacts/suppliers/download', 'ContactController@downloadSuppliersContacts')->name('contacts.suppliers.download');
    Route::delete('/contacts/{contact}/force-delete', 'ContactController@forceDelete')->name('contacts.force-delete');
    Route::get('/contacts/{contact}/restore', 'ContactController@restore')->name('contacts.restore');


    Route::get('/clients/{client}/contacts', 'ClientController@contacts')->name('clients.contacts');
    Route::get('/clients/download', 'ClientController@download')->name('clients.download');
    Route::delete('/clients/{client}/force-delete', 'ClientController@forceDelete')->name('clients.force-delete');
    Route::get('/clients/{client}/restore', 'ClientController@restore')->name('clients.restore');
    Route::resource('/clients', 'ClientController');

    Route::get('/suppliers/{supplier}/contacts', 'SupplierController@contacts')->name('suppliers.contacts');
    Route::get('/suppliers/download', 'SupplierController@download')->name('suppliers.download');
    Route::get('/suppliers/{supplier}/download-contract', 'SupplierController@downloadContract')->name('suppliers.contract.download');
    Route::delete('/suppliers/{supplier}/force-delete', 'SupplierController@forceDelete')->name('suppliers.force-delete');
    Route::get('/suppliers/{supplier}/restore', 'SupplierController@restore')->name('suppliers.restore');
    Route::resource('/suppliers', 'SupplierController');

    Route::get('/contracts/ca/{status?}', 'CAContractController@index')->where('status', 'active|declined|deleted')->name('contracts.ca.index');
    Route::get('/contracts/ca/create', 'CAContractController@create')->name('contracts.ca.create');
    Route::post('/contracts/ca/store', 'CAContractController@store')->name('contracts.ca.store');
    Route::get('/contracts/ca/{contract}/edit', 'CAContractController@edit')->name('contracts.ca.edit');
    Route::put('/contracts/ca/{contract}', 'CAContractController@update')->name('contracts.ca.update');
    Route::get('/contracts/ca/{contract}', 'CAContractController@show')->name('contracts.ca.show');
    Route::delete('/contracts/ca/{contract}', 'CAContractController@destroy')->name('contracts.ca.destroy');
    Route::delete('/contracts/ca/{contract}/force-delete', 'CAContractController@forceDelete')->name('contracts.ca.force-delete');
    Route::get('/contracts/ca/{contract}/restore', 'CAContractController@restore')->name('contracts.ca.restore');
    Route::get('/contracts/ca/{contract}/download', 'CAContractController@download')->name('contracts.ca.download');
    Route::post('/contracts/ca/{contract}/decline', 'CAContractController@decline')->name('contracts.ca.decline');
    Route::get('/contracts/ca/{contract}/activate', 'CAContractController@activate')->name('contracts.ca.activate');
    Route::get('/contracts/ca/{contract}/download-file/{file}', 'CAContractController@downloadFile')->name('contracts.ca.download-file');
    Route::get('/contracts/ca/{contract}/delete-file/{file}', 'CAContractController@deleteFile')->name('contracts.ca.delete-file');
    Route::get('/contracts/ca/{contract}/download-cancellation-letter', 'CAContractController@downloadCancellationLetter')->name('contracts.ca.download-cancellation-letter');
    Route::get('/contracts/ca/{contract}/download-cancellation-agreement', 'CAContractController@downloadCancellationAgreement')->name('contracts.ca.download-cancellation-agreement');

    Route::get('/contracts/smm/{status?}', 'SMMContractController@index')->where('status', 'active|declined')->name('contracts.smm.index');
    Route::get('/contracts/smm/create', 'SMMContractController@create')->name('contracts.smm.create');
    Route::post('/contracts/smm/store', 'SMMContractController@store')->name('contracts.smm.store');
    Route::get('/contracts/smm/{contract}/edit', 'SMMContractController@edit')->name('contracts.smm.edit');
    Route::get('/contracts/smm/{contract}', 'SMMContractController@show')->name('contracts.smm.show');
    Route::put('/contracts/smm/{contract}', 'SMMContractController@update')->name('contracts.smm.update');
    Route::delete('/contracts/smm/{contract}', 'SMMContractController@destroy')->name('contracts.smm.destroy');
    Route::delete('/contracts/smm/{contract}/force-delete', 'SMMContractController@forceDelete')->name('contracts.smm.force-delete');
    Route::get('/contracts/smm/{contract}/restore', 'SMMContractController@restore')->name('contracts.smm.restore');
    Route::get('/contracts/smm/{contract}/download', 'SMMContractController@download')->name('contracts.smm.download');
    Route::post('/contracts/smm/{contract}/decline', 'SMMContractController@decline')->name('contracts.smm.decline');
    Route::get('/contracts/smm/{contract}/activate', 'SMMContractController@activate')->name('contracts.smm.activate');
    Route::get('/contracts/smm/{contract}/download-file/{file}', 'SMMContractController@downloadFile')->name('contracts.smm.download-file');
    Route::get('/contracts/smm/{contract}/delete-file/{file}', 'SMMContractController@deleteFile')->name('contracts.smm.delete-file');
    Route::get('/contracts/smm/{contract}/download-cancellation-letter', 'SMMContractController@downloadCancellationLetter')->name('contracts.smm.download-cancellation-letter');
    Route::get('/contracts/smm/{contract}/download-cancellation-agreement', 'SMMContractController@downloadCancellationAgreement')->name('contracts.smm.download-cancellation-agreement');

    Route::get('/contracts/adm/{status?}', 'AdmContractController@index')->where('status', 'active|declined|deleted')->name('contracts.adm.index');
    Route::get('/contracts/adm/create', 'AdmContractController@create')->name('contracts.adm.create');
    Route::post('/contracts/adm/store', 'AdmContractController@store')->name('contracts.adm.store');
    Route::get('/contracts/adm/{contract}/edit', 'AdmContractController@edit')->name('contracts.adm.edit');
    Route::put('/contracts/adm/{contract}', 'AdmContractController@update')->name('contracts.adm.update');
    Route::get('/contracts/adm/{contract}', 'AdmContractController@show')->name('contracts.adm.show');
    Route::delete('/contracts/adm/{contract}', 'AdmContractController@destroy')->name('contracts.adm.destroy');
    Route::delete('/contracts/adm/{contract}/force-delete', 'AdmContractController@forceDelete')->name('contracts.adm.force-delete');
    Route::get('/contracts/adm/{contract}/restore', 'AdmContractController@restore')->name('contracts.adm.restore');
    Route::get('/contracts/adm/{contract}/download', 'AdmContractController@download')->name('contracts.adm.download');
    Route::post('/contracts/adm/{contract}/decline', 'AdmContractController@decline')->name('contracts.adm.decline');
    Route::get('/contracts/adm/{contract}/activate', 'AdmContractController@activate')->name('contracts.adm.activate');
    Route::get('/contracts/adm/{contract}/download-file/{file}', 'AdmContractController@downloadFile')->name('contracts.adm.download-file');
    Route::get('/contracts/adm/{contract}/delete-file/{file}', 'AdmContractController@deleteFile')->name('contracts.adm.delete-file');
    Route::get('/contracts/adm/{contract}/download-cancellation-letter', 'AdmContractController@downloadCancellationLetter')->name('contracts.adm.download-cancellation-letter');
    Route::get('/contracts/adm/{contract}/download-cancellation-agreement', 'AdmContractController@downloadCancellationAgreement')->name('contracts.adm.download-cancellation-agreement');
    Route::get('/contracts/adm/download/{status?}', 'AdmContractController@export')->where('status', 'active|declined|deleted')->name('contracts.adm.export');

    Route::get('/contracts/seo/{status?}', 'SeoContractController@index')->where('status', 'active|declined|deleted')->name('contracts.seo.index');
    Route::get('/contracts/seo/create', 'SeoContractController@create')->name('contracts.seo.create');
    Route::post('/contracts/seo/store', 'SeoContractController@store')->name('contracts.seo.store');
    Route::get('/contracts/seo/{contract}/edit', 'SeoContractController@edit')->name('contracts.seo.edit');
    Route::put('/contracts/seo/{contract}', 'SeoContractController@update')->name('contracts.seo.update');
    Route::get('/contracts/seo/{contract}', 'SeoContractController@show')->name('contracts.seo.show');
    Route::delete('/contracts/seo/{contract}', 'SeoContractController@destroy')->name('contracts.seo.destroy');
    Route::delete('/contracts/seo/{contract}/force-delete', 'SeoContractController@forceDelete')->name('contracts.seo.force-delete');
    Route::get('/contracts/seo/{contract}/restore', 'SeoContractController@restore')->name('contracts.seo.restore');
    Route::get('/contracts/seo/{contract}/download', 'SeoContractController@download')->name('contracts.seo.download');
    Route::post('/contracts/seo/{contract}/decline', 'SeoContractController@decline')->name('contracts.seo.decline');
    Route::get('/contracts/seo/{contract}/activate', 'SeoContractController@activate')->name('contracts.seo.activate');
    Route::get('/contracts/seo/{contract}/download-file/{file}', 'SeoContractController@downloadFile')->name('contracts.seo.download-file');
    Route::get('/contracts/seo/{contract}/delete-file/{file}', 'SeoContractController@deleteFile')->name('contracts.seo.delete-file');
    Route::get('/contracts/seo/{contract}/download-cancellation-letter', 'SeoContractController@downloadCancellationLetter')->name('contracts.seo.download-cancellation-letter');
    Route::get('/contracts/seo/{contract}/download-cancellation-agreement', 'SeoContractController@downloadCancellationAgreement')->name('contracts.seo.download-cancellation-agreement');

    Route::get('/contracts/app/{status?}', 'AppContractController@index')->where('status', 'active|declined|completed|deleted')->name('contracts.app.index');
    Route::get('/contracts/app/create', 'AppContractController@create')->name('contracts.app.create');
    Route::post('/contracts/app/store', 'AppContractController@store')->name('contracts.app.store');
    Route::get('/contracts/app/{contract}/edit', 'AppContractController@edit')->name('contracts.app.edit');
    Route::put('/contracts/app/{contract}', 'AppContractController@update')->name('contracts.app.update');
    Route::get('/contracts/app/{contract}', 'AppContractController@show')->name('contracts.app.show');
    Route::delete('/contracts/app/{contract}', 'AppContractController@destroy')->name('contracts.app.destroy');
    Route::delete('/contracts/app/{contract}/force-delete', 'AppContractController@forceDelete')->name('contracts.app.force-delete');
    Route::get('/contracts/app/{contract}/restore', 'AppContractController@restore')->name('contracts.app.restore');
    Route::get('/contracts/app/{contract}/download', 'AppContractController@download')->name('contracts.app.download');
    Route::post('/contracts/app/{contract}/decline', 'AppContractController@decline')->name('contracts.app.decline');
    Route::get('/contracts/app/{contract}/activate', 'AppContractController@activate')->name('contracts.app.activate');
    Route::post('/contracts/app/{contract}/complete', 'AppContractController@complete')->name('contracts.app.complete');
    Route::get('/contracts/app/{contract}/download-file/{file}', 'AppContractController@downloadFile')->name('contracts.app.download-file');
    Route::get('/contracts/app/{contract}/delete-file/{file}', 'AppContractController@deleteFile')->name('contracts.app.delete-file');
    Route::get('/contracts/app/{contract}/download-cancellation-letter', 'AppContractController@downloadCancellationLetter')->name('contracts.app.download-cancellation-letter');
    Route::get('/contracts/app/{contract}/download-cancellation-agreement', 'AppContractController@downloadCancellationAgreement')->name('contracts.app.download-cancellation-agreement');

    Route::get('/contracts/ci/{status?}', 'CIContractController@index')->where('status', 'active|declined|completed|deleted')->name('contracts.ci.index');
    Route::get('/contracts/ci/create', 'CIContractController@create')->name('contracts.ci.create');
    Route::post('/contracts/ci/store', 'CIContractController@store')->name('contracts.ci.store');
    Route::get('/contracts/ci/{contract}/edit', 'CIContractController@edit')->name('contracts.ci.edit');
    Route::put('/contracts/ci/{contract}', 'CIContractController@update')->name('contracts.ci.update');
    Route::get('/contracts/ci/{contract}', 'CIContractController@show')->name('contracts.ci.show');
    Route::delete('/contracts/ci/{contract}', 'CIContractController@destroy')->name('contracts.ci.destroy');
    Route::delete('/contracts/ci/{contract}/force-delete', 'CIContractController@forceDelete')->name('contracts.ci.force-delete');
    Route::get('/contracts/ci/{contract}/restore', 'CIContractController@restore')->name('contracts.ci.restore');
    Route::get('/contracts/ci/{contract}/download', 'CIContractController@download')->name('contracts.ci.download');
    Route::post('/contracts/ci/{contract}/decline', 'CIContractController@decline')->name('contracts.ci.decline');
    Route::get('/contracts/ci/{contract}/activate', 'CIContractController@activate')->name('contracts.ci.activate');
    Route::post('/contracts/ci/{contract}/complete', 'CIContractController@complete')->name('contracts.ci.complete');
    Route::get('/contracts/ci/{contract}/download-file/{file}', 'CIContractController@downloadFile')->name('contracts.ci.download-file');
    Route::get('/contracts/ci/{contract}/delete-file/{file}', 'CIContractController@deleteFile')->name('contracts.ci.delete-file');
    Route::get('/contracts/ci/{contract}/download-cancellation-letter', 'CIContractController@downloadCancellationLetter')->name('contracts.ci.download-cancellation-letter');
    Route::get('/contracts/ci/{contract}/download-cancellation-agreement', 'CIContractController@downloadCancellationAgreement')->name('contracts.ci.download-cancellation-agreement');

    Route::get('/contracts/hosting/{status?}', 'HostingContractController@index')->where('status', 'active|declined|deleted')->name('contracts.hosting.index');
    Route::get('/contracts/hosting/create', 'HostingContractController@create')->name('contracts.hosting.create');
    Route::post('/contracts/hosting/store', 'HostingContractController@store')->name('contracts.hosting.store');
    Route::get('/contracts/hosting/{contract}/edit', 'HostingContractController@edit')->name('contracts.hosting.edit');
    Route::put('/contracts/hosting/{contract}', 'HostingContractController@update')->name('contracts.hosting.update');
    Route::get('/contracts/hosting/{contract}', 'HostingContractController@show')->name('contracts.hosting.show');
    Route::delete('/contracts/hosting/{contract}', 'HostingContractController@destroy')->name('contracts.hosting.destroy');
    Route::delete('/contracts/hosting/{contract}/force-delete', 'HostingContractController@forceDelete')->name('contracts.hosting.force-delete');
    Route::get('/contracts/hosting/{contract}/restore', 'HostingContractController@restore')->name('contracts.hosting.restore');
    Route::get('/contracts/hosting/{contract}/download', 'HostingContractController@download')->name('contracts.hosting.download');
    Route::post('/contracts/hosting/{contract}/decline', 'HostingContractController@decline')->name('contracts.hosting.decline');
    Route::get('/contracts/hosting/{contract}/activate', 'HostingContractController@activate')->name('contracts.hosting.activate');
    Route::get('/contracts/hosting/{contract}/download-file/{file}', 'HostingContractController@downloadFile')->name('contracts.hosting.download-file');
    Route::get('/contracts/hosting/{contract}/delete-file/{file}', 'HostingContractController@deleteFile')->name('contracts.hosting.delete-file');
    Route::get('/contracts/hosting/{contract}/download-cancellation-letter', 'HostingContractController@downloadCancellationLetter')->name('contracts.hosting.download-cancellation-letter');
    Route::get('/contracts/hosting/{contract}/download-cancellation-agreement', 'HostingContractController@downloadCancellationAgreement')->name('contracts.hosting.download-cancellation-agreement');

    Route::get('/contracts/additional/{status?}', 'AdditionalContractController@index')->where('status', 'active|declined|completed|deleted')->name('contracts.additional.index');
    Route::get('/contracts/additional/create', 'AdditionalContractController@create')->name('contracts.additional.create');
    Route::post('/contracts/additional/store', 'AdditionalContractController@store')->name('contracts.additional.store');
    Route::get('/contracts/additional/{contract}/edit', 'AdditionalContractController@edit')->name('contracts.additional.edit');
    Route::put('/contracts/additional/{contract}', 'AdditionalContractController@update')->name('contracts.additional.update');
    Route::get('/contracts/additional/{contract}', 'AdditionalContractController@show')->name('contracts.additional.show');
    Route::delete('/contracts/additional/{contract}', 'AdditionalContractController@destroy')->name('contracts.additional.destroy');
    Route::delete('/contracts/additional/{contract}/force-delete', 'AdditionalContractController@forceDelete')->name('contracts.additional.force-delete');
    Route::get('/contracts/additional/{contract}/restore', 'AdditionalContractController@restore')->name('contracts.additional.restore');
    Route::get('/contracts/additional/{contract}/download', 'AdditionalContractController@download')->name('contracts.additional.download');
    Route::post('/contracts/additional/{contract}/decline', 'AdditionalContractController@decline')->name('contracts.additional.decline');
    Route::get('/contracts/additional/{contract}/activate', 'AdditionalContractController@activate')->name('contracts.additional.activate');
    Route::get('/contracts/additional/{contract}/download-file/{file}', 'AdditionalContractController@downloadFile')->name('contracts.additional.download-file');
    Route::get('/contracts/additional/{contract}/delete-file/{file}', 'AdditionalContractController@deleteFile')->name('contracts.additional.delete-file');
    Route::get('/contracts/additional/{contract}/download-cancellation-letter', 'AdditionalContractController@downloadCancellationLetter')->name('contracts.additional.download-cancellation-letter');
    Route::get('/contracts/additional/{contract}/download-cancellation-agreement', 'AdditionalContractController@downloadCancellationAgreement')->name('contracts.additional.download-cancellation-agreement');
    Route::post('/contracts/additional/{contract}/complete', 'AdditionalContractController@complete')->name('contracts.additional.complete');

    Route::get('/contracts/additional-digital/{status?}', 'AdditionalDigitalContractController@index')->where('status', 'active|declined|completed|deleted')->name('contracts.additional-digital.index');
    Route::get('/contracts/additional-digital/create', 'AdditionalDigitalContractController@create')->name('contracts.additional-digital.create');
    Route::post('/contracts/additional-digital/store', 'AdditionalDigitalContractController@store')->name('contracts.additional-digital.store');
    Route::get('/contracts/additional-digital/{contract}/edit', 'AdditionalDigitalContractController@edit')->name('contracts.additional-digital.edit');
    Route::put('/contracts/additional-digital/{contract}', 'AdditionalDigitalContractController@update')->name('contracts.additional-digital.update');
    Route::get('/contracts/additional-digital/{contract}', 'AdditionalDigitalContractController@show')->name('contracts.additional-digital.show');
    Route::delete('/contracts/additional-digital/{contract}', 'AdditionalDigitalContractController@destroy')->name('contracts.additional-digital.destroy');
    Route::delete('/contracts/additional-digital/{contract}/force-delete', 'AdditionalDigitalContractController@forceDelete')->name('contracts.additional-digital.force-delete');
    Route::get('/contracts/additional-digital/{contract}/restore', 'AdditionalDigitalContractController@restore')->name('contracts.additional-digital.restore');
    Route::get('/contracts/additional-digital/{contract}/download', 'AdditionalDigitalContractController@download')->name('contracts.additional-digital.download');
    Route::post('/contracts/additional-digital/{contract}/decline', 'AdditionalDigitalContractController@decline')->name('contracts.additional-digital.decline');
    Route::get('/contracts/additional-digital/{contract}/activate', 'AdditionalDigitalContractController@activate')->name('contracts.additional-digital.activate');
    Route::get('/contracts/additional-digital/{contract}/download-file/{file}', 'AdditionalDigitalContractController@downloadFile')->name('contracts.additional-digital.download-file');
    Route::get('/contracts/additional-digital/{contract}/delete-file/{file}', 'AdditionalDigitalContractController@deleteFile')->name('contracts.additional-digital.delete-file');
    Route::get('/contracts/additional-digital/{contract}/download-cancellation-letter', 'AdditionalDigitalContractController@downloadCancellationLetter')->name('contracts.additional-digital.download-cancellation-letter');
    Route::get('/contracts/additional-digital/{contract}/download-cancellation-agreement', 'AdditionalDigitalContractController@downloadCancellationAgreement')->name('contracts.additional-digital.download-cancellation-agreement');
    Route::post('/contracts/additional-digital/{contract}/complete', 'AdditionalDigitalContractController@complete')->name('contracts.additional-digital.complete');

    Route::get('/contracts/bitrix/{status?}', 'BitrixContractController@index')->where('status', 'active|declined|deleted')->name('contracts.bitrix.index');
    Route::get('/contracts/bitrix/create', 'BitrixContractController@create')->name('contracts.bitrix.create');
    Route::post('/contracts/bitrix/store', 'BitrixContractController@store')->name('contracts.bitrix.store');
    Route::get('/contracts/bitrix/{contract}/edit', 'BitrixContractController@edit')->name('contracts.bitrix.edit');
    Route::put('/contracts/bitrix/{contract}', 'BitrixContractController@update')->name('contracts.bitrix.update');
    Route::get('/contracts/bitrix/{contract}', 'BitrixContractController@show')->name('contracts.bitrix.show');
    Route::delete('/contracts/bitrix/{contract}', 'BitrixContractController@destroy')->name('contracts.bitrix.destroy');
    Route::delete('/contracts/bitrix/{contract}/force-delete', 'BitrixContractController@forceDelete')->name('contracts.bitrix.force-delete');
    Route::get('/contracts/bitrix/{contract}/restore', 'BitrixContractController@restore')->name('contracts.bitrix.restore');
    Route::get('/contracts/bitrix/{contract}/download', 'BitrixContractController@download')->name('contracts.bitrix.download');
    Route::post('/contracts/bitrix/{contract}/decline', 'BitrixContractController@decline')->name('contracts.bitrix.decline');
    Route::get('/contracts/bitrix/{contract}/activate', 'BitrixContractController@activate')->name('contracts.bitrix.activate');
    Route::post('/contracts/bitrix/{contract}/renew', 'BitrixContractController@renew')->name('contracts.bitrix.renew');
    Route::get('/contracts/bitrix/{contract}/download-file/{file}', 'BitrixContractController@downloadFile')->name('contracts.bitrix.download-file');
    Route::get('/contracts/bitrix/{contract}/delete-file/{file}', 'BitrixContractController@deleteFile')->name('contracts.bitrix.delete-file');
    Route::get('/contracts/bitrix/{contract}/download-cancellation-letter', 'BitrixContractController@downloadCancellationLetter')->name('contracts.bitrix.download-cancellation-letter');
    Route::get('/contracts/bitrix/{contract}/download-cancellation-agreement', 'BitrixContractController@downloadCancellationAgreement')->name('contracts.bitrix.download-cancellation-agreement');

    Route::get('/contracts/bitrix-crm/{status?}', 'BitrixCRMContractController@index')->where('status', 'active|declined|deleted')->name('contracts.bitrix-crm.index');
    Route::get('/contracts/bitrix-crm/create', 'BitrixCRMContractController@create')->name('contracts.bitrix-crm.create');
    Route::post('/contracts/bitrix-crm/store', 'BitrixCRMContractController@store')->name('contracts.bitrix-crm.store');
    Route::get('/contracts/bitrix-crm/{contract}/edit', 'BitrixCRMContractController@edit')->name('contracts.bitrix-crm.edit');
    Route::put('/contracts/bitrix-crm/{contract}', 'BitrixCRMContractController@update')->name('contracts.bitrix-crm.update');
    Route::get('/contracts/bitrix-crm/{contract}', 'BitrixCRMContractController@show')->name('contracts.bitrix-crm.show');
    Route::delete('/contracts/bitrix-crm/{contract}', 'BitrixCRMContractController@destroy')->name('contracts.bitrix-crm.destroy');
    Route::delete('/contracts/bitrix-crm/{contract}/force-delete', 'BitrixCRMContractController@forceDelete')->name('contracts.bitrix-crm.force-delete');
    Route::get('/contracts/bitrix-crm/{contract}/restore', 'BitrixCRMContractController@restore')->name('contracts.bitrix-crm.restore');
    Route::get('/contracts/bitrix-crm/{contract}/download', 'BitrixCRMContractController@download')->name('contracts.bitrix-crm.download');
    Route::post('/contracts/bitrix-crm/{contract}/decline', 'BitrixCRMContractController@decline')->name('contracts.bitrix-crm.decline');
    Route::get('/contracts/bitrix-crm/{contract}/activate', 'BitrixCRMContractController@activate')->name('contracts.bitrix-crm.activate');
    Route::post('/contracts/bitrix-crm/{contract}/renew', 'BitrixCRMContractController@renew')->name('contracts.bitrix-crm.renew');
    Route::get('/contracts/bitrix-crm/{contract}/download-file/{file}', 'BitrixCRMContractController@downloadFile')->name('contracts.bitrix-crm.download-file');
    Route::get('/contracts/bitrix-crm/{contract}/delete-file/{file}', 'BitrixCRMContractController@deleteFile')->name('contracts.bitrix-crm.delete-file');
    Route::get('/contracts/bitrix-crm/{contract}/download-cancellation-letter', 'BitrixCRMContractController@downloadCancellationLetter')->name('contracts.bitrix-crm.download-cancellation-letter');
    Route::get('/contracts/bitrix-crm/{contract}/download-cancellation-agreement', 'BitrixCRMContractController@downloadCancellationAgreement')->name('contracts.bitrix-crm.download-cancellation-agreement');

    Route::get('/contracts/amo-crm/{status?}', 'AmoCRMContractController@index')->where('status', 'active|declined|deleted')->name('contracts.amo-crm.index');
    Route::get('/contracts/amo-crm/create', 'AmoCRMContractController@create')->name('contracts.amo-crm.create');
    Route::post('/contracts/amo-crm/store', 'AmoCRMContractController@store')->name('contracts.amo-crm.store');
    Route::get('/contracts/amo-crm/{contract}/edit', 'AmoCRMContractController@edit')->name('contracts.amo-crm.edit');
    Route::put('/contracts/amo-crm/{contract}', 'AmoCRMContractController@update')->name('contracts.amo-crm.update');
    Route::get('/contracts/amo-crm/{contract}', 'AmoCRMContractController@show')->name('contracts.amo-crm.show');
    Route::delete('/contracts/amo-crm/{contract}', 'AmoCRMContractController@destroy')->name('contracts.amo-crm.destroy');
    Route::delete('/contracts/amo-crm/{contract}/force-delete', 'AmoCRMContractController@forceDelete')->name('contracts.amo-crm.force-delete');
    Route::get('/contracts/amo-crm/{contract}/restore', 'AmoCRMContractController@restore')->name('contracts.amo-crm.restore');
    Route::get('/contracts/amo-crm/{contract}/download', 'AmoCRMContractController@download')->name('contracts.amo-crm.download');
    Route::post('/contracts/amo-crm/{contract}/decline', 'AmoCRMContractController@decline')->name('contracts.amo-crm.decline');
    Route::get('/contracts/amo-crm/{contract}/activate', 'AmoCRMContractController@activate')->name('contracts.amo-crm.activate');
    Route::post('/contracts/amo-crm/{contract}/renew', 'AmoCRMContractController@renew')->name('contracts.amo-crm.renew');
    Route::get('/contracts/amo-crm/{contract}/download-file/{file}', 'AmoCRMContractController@downloadFile')->name('contracts.amo-crm.download-file');
    Route::get('/contracts/amo-crm/{contract}/delete-file/{file}', 'AmoCRMContractController@deleteFile')->name('contracts.amo-crm.delete-file');
    Route::get('/contracts/amo-crm/{contract}/download-cancellation-letter', 'AmoCRMContractController@downloadCancellationLetter')->name('contracts.amo-crm.download-cancellation-letter');
    Route::get('/contracts/amo-crm/{contract}/download-cancellation-agreement', 'AmoCRMContractController@downloadCancellationAgreement')->name('contracts.amo-crm.download-cancellation-agreement');
    
    Route::resource('/permissions', 'PermissionController', ['except' => ['destroy', 'show']]);
    Route::resource('/roles', 'RoleController', ['except' => ['destroy', 'show']]);

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::put('/profile', 'ProfileController@update')->name('profile.update');

    Route::prefix('dictionaries')->group(function () {
        Route::resource('/app-contract-stages', 'AppContractStageController', ['except' => ['show']]);
        Route::resource('/ownership-types', 'OwnershipTypeController', ['except' => ['show']]);
        Route::resource('/positions', 'PositionController', ['except' => ['show']]);
        Route::resource('/activities', 'ActivityController', ['except' => ['show']]);
        Route::resource('/hosting-services', 'HostingServiceController', ['except' => ['show']]);
        Route::resource('/regions', 'RegionController', ['except' => ['show']]);
        Route::resource('/cities', 'CityController', ['except' => ['show']]);
        Route::resource('/payment-methods', 'PaymentMethodController', ['except' => ['show']]);
        Route::resource('/bitrix-editions', 'BitrixEditionController', ['except' => ['show']]);
        Route::resource('/bitrix-crm-editions', 'BitrixCRMEditionController', ['except' => ['show']]);
        Route::resource('/amo-crm-editions', 'AmoCRMEditionController', ['except' => ['show']]);
    });

//    Route::get('/reports/{type}', 'ReportController@index')->where('type', 'smm|seo|ca|adm')->name('reports.index');
//    Route::get('/reports/{report}/edit', 'ReportController@edit')->name('reports.edit');
//    Route::put('/reports/{report}/edit', 'ReportController@update')->name('reports.update');
//    Route::get('/reports/{report}/download-report', 'ReportController@downloadReport')->name('reports.download-report');
//    Route::get('/reports/{report}/download-act', 'ReportController@downloadAct')->name('reports.download-act');
//    Route::delete('/reports/{report}', 'ReportController@destroy')->name('reports.destroy');

    Route::get('/reports/generate', 'ReportController@generate')->name('reports.generate');

    Route::get('/contracts/{status?}', 'ContractController@index')->where('status', 'new|active|declined|completed|unsigned')->name('contracts.index');
    Route::get('/contracts/download/{status?}', 'ContractController@download')->where('status', 'new|active|declined|completed')->name('contracts.download');
    Route::match(['get', 'post'],'/contracts/correct/{contract}', 'ContractController@correct')
        ->name('contracts.correct');

    Route::post('/contracts/get-client-contracts', 'ContractController@getClientContracts')
        ->name('contracts.get-client-contracts');

    Route::resource('/contracts', 'ContractController', [
        'except' => ['show', 'index'],
        'names' => [
            'create' => 'contracts.create',
            'store' => 'contracts.store',
            'edit' => 'contracts.edit',
            'update' => 'contracts.update',
            'destroy' => 'contracts.destroy',
        ]
    ]);

});