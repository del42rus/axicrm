$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#confirm-delete, #confirm-decline-complete, #confirm-force-delete, #confirm-cancellation, #confirm-renewal').on('show.bs.modal', function(e) {
        $(this).find('form').attr('action', $(e.relatedTarget).data('href'));
    });

    $('form[name="PermissionCreate"]').find(':radio').on('change', function () {
        $('#basic').toggleClass('d-none', $(this).val() == 'crud');
        $('#crud').toggleClass('d-none', $(this).val() == 'basic')
    });

    $(".datepicker").flatpickr({locale: 'ru', dateFormat: 'd/m/Y'});
    $('select').selectize();

    $('input[name="phone"]').inputmask({mask: "+7 (999[9]) 99[9]-99-99", greedy: false});
});
